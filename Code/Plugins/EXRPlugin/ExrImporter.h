#pragma once

#include "ImfRgbaFile.h"

using namespace System::Runtime::InteropServices;
using namespace System;
using namespace Assets;

public ref class ExrImporter abstract sealed
{
public:
	static Image^ Load(String^ filePath)
	{
		Imf::RgbaInputFile* exrFile = new Imf::RgbaInputFile(static_cast<char*>(Marshal::StringToHGlobalAnsi(filePath).ToPointer()));

		Imath::Box2i dataBox = exrFile->dataWindow();

		Image^ image = gcnew Image(dataBox.max.x - dataBox.min.x + 1, dataBox.max.y - dataBox.min.y + 1, ImageFormat::RGBA16);
		GCHandle gcHandle = GCHandle::Alloc(image->Buffer, GCHandleType::Pinned);
		exrFile->setFrameBuffer(((Imf::Rgba*)gcHandle.AddrOfPinnedObject().ToPointer()) - dataBox.min.x - dataBox.min.y, 1, image->Width);
		exrFile->readPixels(dataBox.min.y, dataBox.max.y);
		gcHandle.Free();

		return image;
	}
};

