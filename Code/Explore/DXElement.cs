﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Runtime.CompilerServices;
using Explore.UI.ContextMenus;
using Graphics;

namespace Explore
{

    /// <summary>
    /// A <see cref="UIElement"/> displaying DirectX scene. 
    /// Takes care of resizing and refreshing a <see cref="DXImageSource"/>.
    /// It does no Direct3D work, which is delegated to
    /// the <see cref="Scene"/> <see cref="Renderer"/> object.
    /// </summary>
    public class DXElement : FrameworkElement, INotifyPropertyChanged
    {
        #region Init
        private static object _padLock = new object();
        /// <summary>
        /// 
        /// </summary>
        public DXElement()
        {
            base.SnapsToDevicePixels = true;

            m_renderTimer = new Stopwatch();
            m_renderTimer.Start();
            m_surface = new DXImageSource();
            //m_surface.IsFrontBufferAvailableChanged += (s, e) =>
            //{
            //    UpdateReallyLoopRendering();
            //    if (/*!m_isReallyLoopRendering && */m_surface.IsFrontBufferAvailable)
            //        Render();
            //};
            IsVisibleChanged += (s, e) => UpdateReallyLoopRendering();
            CompositionTarget.Rendering += OnLoopRendering;
        }

        #endregion

        #region Dependency Property

        /// <summary>
        /// The D3D device that will handle the drawing
        /// </summary>
        public Scene Renderer
        {
            get { return (Scene)GetValue(RendererProperty); }
            set { SetValue(RendererProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty RendererProperty =
            DependencyProperty.Register(
                "Renderer",
                typeof(Scene),
                typeof(DXElement),
                new PropertyMetadata((d, e) => ((DXElement)d).OnRendererChanged((Scene)e.OldValue, (Scene)e.NewValue)));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnRendererChanged(Scene oldValue, Scene newValue)
        {
            UpdateSize();
            UpdateReallyLoopRendering();
            Renderer.DrawFrame += UpdateFrame;
            //Focusable = newValue is IInteractiveDirect3D;
        }

        private void UpdateFrame(object sender, EventArgs e)
        {
            CompositionTarget.Rendering += OnLoopRendering;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The image source where the DirectX scene (from the <see cref="Renderer"/>) will be rendered.
        /// </summary>
        public DXImageSource Surface => m_surface;

        public FormattedText FPSText { get; private set; }

        /// <summary>
        /// Wether or not the DirectX scene will be redrawn continuously
        /// </summary>
        public bool IsLoopRendering
        {
            get => m_isLoopRendering;
            set
            {
                if (value == m_isLoopRendering)
                    return;
                m_isLoopRendering = value;
                UpdateReallyLoopRendering();
                RaiseThisPropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the control is in design mode
        /// (running in Blend or Visual Studio).
        /// </summary>
        public bool IsInDesignMode => DesignerProperties.GetIsInDesignMode(this);

        #endregion

        #region Protected Size Overrides

        /// <summary>
        /// 
        /// </summary>        
        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            if (IsInDesignMode)
                return;
            UpdateReallyLoopRendering();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override System.Windows.Size ArrangeOverride(System.Windows.Size finalSize)
        {
            base.ArrangeOverride(finalSize);
            UpdateSize();
            return finalSize;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override System.Windows.Size MeasureOverride(System.Windows.Size availableSize)
        {
            int w = (int)Math.Ceiling(availableSize.Width);
            int h = (int)Math.Ceiling(availableSize.Height);
            return new System.Windows.Size(w, h);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override Visual GetVisualChild(int index)
        {
            throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// 
        /// </summary>        
        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawImage(Surface, new Rect(RenderSize));
            dc.DrawText(FPSText, new Point(5,5));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override int VisualChildrenCount => 0;

        #endregion

        #region Private: ..LoopRendering.., UpdateSize

        /// <summary>
        /// 
        /// </summary>
        private void UpdateReallyLoopRendering()
        {
            //var newValue =
            //    !IsInDesignMode
            //    && IsLoopRendering
            //    && Renderer != null
            //    && Surface.IsFrontBufferAvailable
            //    && VisualParent != null
            //    && IsVisible
            //    ;

            //if (newValue != m_isReallyLoopRendering)
            //{
            //    m_isReallyLoopRendering = newValue;
            //    if (m_isReallyLoopRendering)
            //    {
            //        m_renderTimer.Start();
            //        CompositionTarget.Rendering += OnLoopRendering;
            //    }
            //    else
            //    {
            //        CompositionTarget.Rendering -= OnLoopRendering;
            //        m_renderTimer.Stop();
            //    }
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnLoopRendering(object sender, EventArgs e)
        {
            lock (_padLock)
            {
                CompositionTarget.Rendering -= OnLoopRendering;
                //if (!m_isReallyLoopRendering)
                //    return;
                Render();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateSize()
        {
            if (Renderer == null)
                return;
            Renderer.Reset(GetDrawEventArgs());
        }

        #endregion

        #region Render

        /// <summary>
        /// Will redraw the underlying surface once.
        /// </summary>
        public void Render()
        {
            if (Renderer == null || IsInDesignMode)
                return;

            DrawEventArgs e = GetDrawEventArgs();
            double fps = 1.0d / e.DeltaTime.TotalSeconds;
            FPSText = new FormattedText(fps.ToString().Split('.')[0], System.Globalization.CultureInfo.InvariantCulture, FlowDirection.LeftToRight, Fonts.GetTypefaces(@"C:\Windows\Fonts").First(), 12, new SolidColorBrush(Color.FromRgb(255, 0, 0)), 1.25);
            Renderer.Render(e);
            Surface.Invalidate();
            InvalidateVisual();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DrawEventArgs GetDrawEventArgs()
        {
            var eargs = new DrawEventArgs
            {
                TotalTime = m_renderTimer.Elapsed,
                DeltaTime = m_lastDrawEventArgs != null ? m_renderTimer.Elapsed - m_lastDrawEventArgs.TotalTime : TimeSpan.Zero,
                RenderSize = DesiredSize,
                Target = Surface,
            };
            m_lastDrawEventArgs = eargs;
            return eargs;
        }

        public void Shutdown()
        {
            Renderer.Dispose();
        }

        #endregion

        #region Override Mouse and Key Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            //Renderer.DrawPicker.DoPicking(RenderSize, e.GetPosition(this));
            if (Renderer.SelectedObject == null)
            {
                ContextMenu = new ViewportContextMenu1();
            }
            else
            {
                ContextMenu = new ViewportContextMenu2();
            }
 
            base.OnMouseRightButtonDown(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnMouseDown(this, e);
        }

        System.Windows.Point _dragStart;
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            e.Handled = true;
            _dragStart = e.GetPosition(this);
            Renderer.DrawPicker.DoPicking(RenderSize, e.GetPosition(this));
            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed)
            {
                _dragStart = e.GetPosition(this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            System.Windows.Point now = e.GetPosition(this);
            Assets.Vector delta = new Assets.Vector((float)now.X, (float)now.Y, 0) - new Assets.Vector((float)_dragStart.X, (float)_dragStart.Y, 0);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Renderer.Manipulate(new Assets.Vector(delta.X,-delta.Y, -delta.Z));
            }
            else if(e.MiddleButton == MouseButtonState.Pressed)
            {
                Renderer.HandleCameraRotate(delta);
            }

            _dragStart = now;

            base.OnMouseMove(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnMouseMove(this, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnMouseUp(this, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnMouseWheel(this, e);
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            e.Handled = true;
            Renderer.HandleCameraZoom(e.Delta);        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnKeyDown(this, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            //if (Renderer is IInteractiveDirect3D)
            //    ((IInteractiveDirect3D)Renderer).OnKeyUp(this, e);
        }

        #endregion

        #region INotifyPropertyChanged Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        private void RaiseThisPropertyChanged([CallerMemberName] string name="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Fields
        private bool m_isLoopRendering = true;
        private bool m_isReallyLoopRendering;
        private DrawEventArgs m_lastDrawEventArgs;
        private DXImageSource m_surface;
        private Stopwatch m_renderTimer;
        #endregion
    }
}
