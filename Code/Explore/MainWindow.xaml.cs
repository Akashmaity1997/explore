﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics;
using Assets;
using Vector = Assets.Vector;
using Explore.UI.ViewModels;
using Explore.UI;

namespace Explore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainTabControl TabControl => MainTabControl;

        public MainWindow()
        {
            InitializeComponent();
            MainWindowViewModel mainWindowViewModel = new MainWindowViewModel();
            DataContext = mainWindowViewModel;
            View1.Renderer = new Scene(mainWindowViewModel, TabControl, View1);
            Closing += (s, e) =>
            {
                View1.Shutdown();
            };
        }
    }
}
