﻿using System;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using DataModel;
using Assets;
using Assets.Interfaces;
using System.Windows;
using Vector = Assets.Vector;
using Point = Assets.Point;
using Explore.UI.ViewModels;
using Graphics.ShaderInterfaces;
using Explore.UI;
using System.Windows.Controls;
using System.Windows.Input;
using Explore.UI.Commands;
using Assets.Materials;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using SharpDX.D3DCompiler;
using SharpDX.DXGI;
using System.Linq;
using Assets.Utilities;
using System.Collections.Generic;
using System.IO;
using Graphics;
using Manipulate;

namespace Explore
{
    public class Scene : IDisposable
    {
        private readonly IDataProvider _dataProvider;
        private readonly IDrawingManager _drawingManager;
        private readonly MainTabControl _mainTabControl;
        private readonly DXElement _mainViewport;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IWorldDm _worldDm;
        private readonly PassGenerator _passGenerator;
        private readonly ManipulatorManager _manipulatorManager;

        public event EventHandler DrawFrame;

        public IDrawPicker DrawPicker { get; private set; }

        public Scene(MainWindowViewModel mainViewModel, MainTabControl mainTabControl, DXElement mainViewport)
        {
            Renderer = new D3D11();
            _dataProvider = new DataProvider(mainViewport);
            _backgroundJobManager = new BackgroundJobManager();
            _passGenerator = new PassGenerator(_dataProvider, Renderer);
            _drawingManager = new DrawingManager(Renderer, _passGenerator);
            _worldDm = new WorldDm();
            _dataProvider.CurrentWorldDm = _worldDm;
            _manipulatorManager = new ManipulatorManager(_dataProvider);
            _passGenerator.RegisterExtension(_manipulatorManager);

            IViewModelDataProvider objectCreator = new ViewModelDataProvider(_dataProvider, mainViewModel);

            _mainTabControl = mainTabControl;
            _mainViewport = mainViewport;

            World world = new World();
            _dataProvider.CurrentWorld = world;
            ICameraDm cameraDm = new CameraDm(new Vector(3f, 3f, -3f), new Vector(), 1, 90.0f, 1.0f, 50.0f);
            _dataProvider.CurrentCameraDm = cameraDm;

            _dataProvider.CurrentEnvironmentDm = new EnvironmentDm
            {
                Texture = new TextureDm
                {
                    Asset = TextureLoader.LoadImage($@"{AppDomain.CurrentDomain.BaseDirectory}Environment Maps\stone_alley_03.exr"),
                },
                Position = MatrixHelpers.Create(new Vector(2f, 0, 0), new Vector(0, 2, 0f), new Vector(0, 0f, 2), new Point())
        };

            SetupCommandBindings();

            Axis worldAxis = new Axis(world.Position, _dataProvider, 1.0f, 0.005f);
            DrawPicker = new DrawPicker(_dataProvider, _drawingManager, _passGenerator);

            Attach();
            _dataProvider.CrucialPropertyChanged += UpdateScene;

            AssetManager assetManager = new AssetManager();
        }

        public IDrawable SelectedObject => _dataProvider.SelectedObject;

        public void AddCube()
        {
            CreateModel(ProceduralMeshType.Cube);
        }

        public void AddCylinder()
        {
            CreateModel(ProceduralMeshType.Cylinder);
        }

        public void AddSphere()
        {
            CreateModel(ProceduralMeshType.Sphere);
        }

        public void AddTorus()
        {
            CreateModel(ProceduralMeshType.Torus);
        }

        public void AddCone()
        {
            CreateModel(ProceduralMeshType.Cone);
        }

        private void CreateModel(ImportData importData)
        {
            ModelDm model = new ModelDm() { Name = importData.Name };
            ShapeDm root = new ShapeDm(null) { Name = $"{importData.Name} Group" };
            root.Parent = model;
            model.Shapes.Add(root);
            Mesh mesh = importData.Import();
            ShapeDm shape = new ShapeDm(mesh) { Name = $"{importData.Name} Part"};
            shape.Parent = root;
            model.Shapes.Add(shape);
            _worldDm.Models.Add(model);
        }

        private void CreateModel(ProceduralMeshType type)
        {
            ModelDm model = new ModelDm() { Name = type.ToString() };
            ShapeDm root = new ShapeDm(null) { Name = model.Name + " Group" };
            root.Parent = model;
            model.Shapes.Add(root);
            Mesh mesh = ProceduralMeshGenerator.Create(type);
            ShapeDm shape = new ShapeDm(mesh) { Name = model.Name + " Part" };
            shape.Parent = root;
            model.Shapes.Add(shape);
            _worldDm.Models.Add(model);
        }

        public void AddLight()
        {
            Light l = new Light(MatrixHelpers.Identity, new Vector(),3.0f, new SharpDX.Mathematics.Interop.RawVector3(1.0f,1.0f,1.0f), _dataProvider);
        }

        public void AddDecal()
        {
            SharpDX.Direct3D11.Device device = Renderer.Device;
            DeviceContext context = device.ImmediateContext;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == false)
                return;

            string filepath = openFileDialog.FileName;
            Texture2D texture = TextureLoader.LoadTexture2D(device, filepath);
            WriteableBitmap image = texture.GetBitmap();

            Decal d = new Decal(filepath, texture, image, _dataProvider);
        }

        public void DeleteObject()
        {
            _dataProvider.UnRegisterObject();
        }

        public void Manipulate(Vector delta)
        {
            _dataProvider.Manipulate(delta);
        }

        public void HandleCameraZoom(float delta)
        {
            float zoom = delta / 100f;
            if (_dataProvider.CurrentCameraDm.FOV + zoom >= 10 && _dataProvider.CurrentCameraDm.FOV + zoom <= 180)
                _dataProvider.CurrentCameraDm.FOV += zoom;
        }

        public void HandleCameraRotate(Vector delta)
        {
            _dataProvider.CurrentCameraDm.CameraMouseRotation(delta);
        }

        private D3D11 _renderer;
        public D3D11 Renderer
        {
            get => _renderer;
            private set
            {
                if (Renderer != null)
                {
                    Renderer.Rendering -= ContextRendering;
                    Renderer.Resetted -= Renderer_Resetted;
                    //Detach();
                }
                _renderer = value;
                if (Renderer != null)
                {
                    Renderer.Rendering += ContextRendering;
                    Renderer.Resetted += Renderer_Resetted;
                    //Attach();
                }
            }
        }

        private void Renderer_Resetted(object sender, DrawEventArgs e)
        {
            _dataProvider.CurrentCameraDm.AspectRatio = (float)(e.RenderSize.Width / e.RenderSize.Height);
        }

        private void ContextRendering(object sender, DrawEventArgs e)
        {
            RenderScene(e);
        }

        private void Attach()
        {
            if (Renderer == null)
                return;
            var device = Renderer.Device;
            if (device == null)
                return;

            var context = device.ImmediateContext;
            context.InputAssembler.PrimitiveTopology = (PrimitiveTopology.TriangleList);
            //context.OutputMerger.SetTargets(Renderer.RenderView);
        }

        private void UpdateScene(object sender, EventArgs e)
        {
           Attach();
           DrawFrame?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            Renderer.Rendering -= ContextRendering;
            DisposeAssets();
            Renderer.Dispose();
        }

        private void DisposeAssets()
        {
            _dataProvider.DisposeAssets();
        }

        public void RenderScene(DrawEventArgs args)
        {
            //_drawingManager.UpdateDrawTarget(Renderer.RenderTargetView, Renderer.DepthStencilView);
            //_drawingManager.DrawMainPipeline();
            _drawingManager.Draw();
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="args"></param>
		public void Reset(DrawEventArgs args)
        {
            if (Renderer != null)
                Renderer.Reset(args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public void Render(DrawEventArgs args)
        {
            if (Renderer != null)
                Renderer.Render(args);
        }

        private void SetupCommandBindings()
        {
            TabItem geomteryTab = _mainTabControl.GeometryViewTab;
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.CreateCubeCommand, ExecuteCreateCube));
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.CreateCylinderCommand, ExecuteCreateCylinder));
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.CreateSphereCommand, ExecuteCreateSphere));
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.CreateTorusCommand, ExecuteCreateTorus));
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.CreateConeCommand, ExecuteCreateCone));
            geomteryTab.CommandBindings.Add(new CommandBinding(GeometryCommands.ImportModelCommand, ExecuteImportModel));

            TabItem materialTab = _mainTabControl.MaterialViewTab;
            materialTab.CommandBindings.Add(new CommandBinding(MaterialCommands.CreateDecalCommand, ExecuteCreateDecal));

            TabItem environmentTab = _mainTabControl.EnvironmentViewTab;
            environmentTab.CommandBindings.Add(new CommandBinding(EnvironmentCommands.CreateLightCommand, ExecuteCreateLight));

            TabItem cameraTab = _mainTabControl.CameraViewTab;
            cameraTab.CommandBindings.Add(new CommandBinding(CameraCommands.CreateCameraCommand, ExecuteCreateCamera));

            Window mainWindow = Application.Current.MainWindow;
            mainWindow.CommandBindings.Add(new CommandBinding(CommonCommands.DeleteObjectCommand, ExecuteDeleteObject));
        }

        private void ExecuteCreateCube(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddCube();
        }
        private void ExecuteCreateCylinder(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddCylinder();
        }
        private void ExecuteCreateSphere(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddSphere();
        }
        private void ExecuteCreateTorus(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddTorus();
        }

        private void ExecuteCreateCone(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddCone();
        }

        private readonly List<string> importExtensions = new List<string>()
        {
            ".stl"
        };

        private void ExecuteImportModel(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == false)
                return;

            string filepath = openFileDialog.FileName;
            string extn = Path.GetExtension(filepath).ToLowerInvariant();
            if (importExtensions.Contains(extn))
            {

                ImportData data = new ImportData(filepath, _dataProvider, _backgroundJobManager);
                CreateModel(data);
            }
            else
                MessageBox.Show("File not supported");
        }

        private void ExecuteCreateLight(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            AddLight();
        }

        private void ExecuteDeleteObject(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            DeleteObject();
        }

        private void ExecuteCreateCamera(object sender, ExecutedRoutedEventArgs e)
        {
            void MouseLeftButtonUp(object s, MouseButtonEventArgs w)
            {
                _mainViewport.MouseLeftButtonUp -= MouseLeftButtonUp;
                DrawPicker.DoPicking(_mainViewport.RenderSize, Mouse.GetPosition(_mainViewport));
                if (_dataProvider.SelectedObject != null)
                {
                    SharpDX.Mathematics.Interop.RawMatrix pos = _dataProvider.SelectedObject.Position;
                    Vector refPosition = new Vector(pos.M41, pos.M42, pos.M43);
                    Vector position = refPosition + new Vector(1.5f, 1.5f, 6f);
                    CameraDm camera = new CameraDm(position, refPosition, 1, 45.0f, 1.0f, 50.0f);
                    _dataProvider.CurrentCameraDm = camera;
                }
            }

            _mainViewport.MouseLeftButtonUp += MouseLeftButtonUp;
        }

        private void ExecuteCreateDecal(object sender, ExecutedRoutedEventArgs e)
        {
            AddDecal();
        }
    }
}
