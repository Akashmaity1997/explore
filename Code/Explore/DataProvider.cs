﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using System.ComponentModel;
using System.Windows;
using System.Reflection;
using DataModel;
using SharpDX.Mathematics.Interop;
using System.Collections.ObjectModel;
using Graphics.ShaderInterfaces;
using System.Collections.Specialized;
using Assets.Materials;
using System.Windows.Input;
using SharpDX.Direct3D11;
using SharpDX;

namespace Explore
{
    public class DataProvider : PropertyAware, IDataProvider
    {
        private readonly DXElement _currentViewport;

        public ObservableCollection<IDrawable> Objects { get; set; } = new ObservableCollection<IDrawable>();
        public ObservableCollection<Light> Lights { get; set; } = new ObservableCollection<Light>();
        public ObservableCollection<Camera> Cameras { get; set; } = new ObservableCollection<Camera>();

        public ObservableCollection<Decal> Decals { get; set; } = new ObservableCollection<Decal>();

        public Dictionary<Assets.Point, IDrawable> PickableColourToObjectMapping { get; set; } = new Dictionary<Assets.Point, IDrawable>();

        public DataProvider(DXElement currentViewport)
        {
            _currentViewport = currentViewport;
            Lights.CollectionChanged += LightsCollectionChanged;
        }

        public event EventHandler CrucialPropertyChanged;

        private World _currentWorld;
        public World CurrentWorld
        {
            get => _currentWorld;
            set
            {
                _currentWorld = value;
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private IWorldDm _currentWorldDm;
        public IWorldDm CurrentWorldDm 
        { 
            get => _currentWorldDm;
            set
            {
                if(_currentWorldDm == value)
                {
                    return;
                }

                _currentWorldDm = value;
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private ICameraDm _currentCameraDm;
        public ICameraDm CurrentCameraDm
        {
            get => _currentCameraDm;
            set
            {
                if (_currentCameraDm != null)
                {
                    _currentCameraDm.PropertyChanged -= CameraPropertyChanged;
                }

                if(_currentCameraDm == value)
                {
                    return;
                }

                _currentCameraDm = value;
                _currentCameraDm.PropertyChanged += CameraPropertyChanged;

                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private IEnvironmentDm _currentEnvironmentDm;
        public IEnvironmentDm CurrentEnvironmentDm
        {
            get => _currentEnvironmentDm;
            set
            {
                if(_currentEnvironmentDm != null)
                {
                    _currentEnvironmentDm.PropertyChanged -= EnvironmentPropertyChanged;
                }

                _currentEnvironmentDm = value;

                if(_currentEnvironmentDm != null)
                {
                    _currentEnvironmentDm.PropertyChanged += EnvironmentPropertyChanged;
                }

                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private Camera _currentCamera;
        public Camera CurrentCamera
        {
            get => _currentCamera;
            set
            {
                if(_currentCamera != null)
                {
                    _currentCamera.PropertyChanged -= CameraPropertyChanged;
                }

                _currentCamera = value;
                _currentCamera.PropertyChanged += CameraPropertyChanged;

                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public int VertexCount { get; set; }

        private Axis _manipulator;

        private IBaseDm _selectedDm;
        public IBaseDm SelectedDm
        {
            get => _selectedDm;
            set
            {
                //lock (_padLock)
                //{
                if(_selectedDm == value)
                {
                    return;
                }

                _selectedDm = value;
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
                RaiseThisPropertyChanged();
                //}
            }
        }

        private IDrawable _selectedObject;
        public IDrawable SelectedObject
        {
            get => _selectedObject;
            set
            {
                //lock (_padLock)
                //{
                    if (_selectedObject == value)
                        return;

                    if (value != null && value.IsManipulator)
                        return;

                    _selectedObject = value;
                    DrawObjectManipulator();
                    CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
                RaiseThisPropertyChanged();
                //}
            }
        }

        private void DrawObjectManipulator()
        {
            if (_selectedObject != null)
            {
                if(_manipulator != null)
                {
                    _manipulator.UnRegister();
                    _manipulator = null;
                }

                _manipulator = new Axis(_selectedObject.Position, this, 2.0f, 0.01f, true, _selectedObject.ScaleMode);
            }
            else
            {

                _manipulator.UnRegister();
                _manipulator = null;
            }          
        }

        private void CameraPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((PropertyChangedEventHandler)CameraPropertyChanged, sender, e);
                return;
            }

            if(e.PropertyName.Equals(nameof(CurrentCameraDm.ViewMatrix)) || 
                e.PropertyName.Equals(nameof(CurrentCameraDm.ProjectionMatrix)) ||
                e.PropertyName.Equals(nameof(CurrentCameraDm.Position)))
            {
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void EnvironmentPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((PropertyChangedEventHandler)EnvironmentPropertyChanged, sender, e);
                return;
            }

            if (e.PropertyName.Equals(nameof(CurrentEnvironmentDm.Texture)))
            {
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public IBaseDm FindByGuid(Guid guid)
        {
            foreach(IModelDm model in _currentWorldDm.Models)
            {
                if(model.Guid == guid)
                {
                    return model;
                }

                foreach(IShapeDm shape in model.Shapes)
                {
                    if(shape.Guid == guid)
                    {
                        return shape;
                    }
                }
            }

            return null;
        }

        public void RegisterObject(APresentable obj)
        {
            if (obj is Light)
            {
                Lights.Add((Light)obj);
                return;
            }

            VertexCount += obj.VertexCount;
            obj.PropertyChanged += GeometryPropertyChanged;
            CurrentWorld.RegisterObject(obj);
            CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);

            Objects.Add(obj);
        }

        public void RegisterObject(Camera camera)
        {
            Cameras.Add(camera);
        }

        public void RegisterObject(Decal decal)
        {
            Decals.Add(decal);
        }

        public void UnRegisterObject()
        {
            if (Lights.FirstOrDefault(l => l.Representor == _selectedObject) is Light light)
            {
                Lights.Remove(light);
                light.Unregister();
                _selectedObject = null;
                return;
            }
            else if(Cameras.FirstOrDefault(c => c.Representor == _selectedObject) is Camera camera)
            {
                Cameras.Remove(camera);
                camera.Unregister();
                _selectedObject = null;
                return;
            }

            UnRegisterObject((APresentable)_selectedObject);
            SelectedObject = null;
        }

        public void UnRegisterObject(APresentable obj)
        {
            VertexCount -= obj.VertexCount;
            obj.PropertyChanged -= GeometryPropertyChanged;
            CurrentWorld.UnregisterObject(obj);
            CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            Objects.Remove(obj);
            PickableColourToObjectMapping.Remove(obj.Pickable.PickColour);
        }

        public void Manipulate(Assets.Vector delta)
        {
            if (_manipulator == null)
                return;

            if (_manipulator.IsSelected)
            {
                _manipulator.Manipulate(delta);
                //((IAnimatable)_selectedObject).Translate();
            }
        }

        public void GeometryPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((PropertyChangedEventHandler)GeometryPropertyChanged, sender, e);
                return;
            }

            if(e.PropertyName.Equals(nameof(APresentable.ScaleMode)))
            {
                DrawObjectManipulator();
            }

            CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
        }

        private void LightsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            
            if(e.NewItems != null)
            {
                foreach (Light light in e.NewItems)
                    light.PropertyChanged += LightPropertyChanged;
            }

            if(e.OldItems != null)
            {
                foreach (Light light in e.OldItems)
                    light.PropertyChanged -= LightPropertyChanged;
            }

            CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
        }

        private void LightPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Light light = sender as Light;

            if (e.PropertyName.Equals(nameof(light.Position))||
                e.PropertyName.Equals(nameof(light.Colour)) ||
                e.PropertyName.Equals(nameof(light.Type)))
            {
                CrucialPropertyChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public void UnRegisterObject(Decal decal)
        {
            Decals.Remove(decal);
        }

        public void DisposeAssets()
        {
            foreach (Decal decal in Decals)
                decal.TextureAsset.Dispose();
        }

        public void PickCameraTarget(bool follow)
        {
            void MouseLeftButtonUp(object s, MouseButtonEventArgs w)
            {
                _currentViewport.MouseLeftButtonUp -= MouseLeftButtonUp;
                //DrawPicker.DoPicking(_currentViewport.RenderSize, Mouse.GetPosition(_currentViewport));
                if (SelectedObject != null)
                {
                    SharpDX.Mathematics.Interop.RawMatrix pos = SelectedObject.Position;
                    CurrentCamera.LookAt = new Assets.Vector(pos.M41, pos.M42, pos.M43);
                    if (follow)
                        CurrentCamera.FollowingObject = (APresentable)SelectedObject;
                }
            }

            _currentViewport.MouseLeftButtonUp += MouseLeftButtonUp;
        }
    }
}
