﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using System.IO;

namespace Graphics
{
    public class D3D11 : IDisposable, INotifyPropertyChanged
    {
        /// <summary>
        /// 
        /// </summary>
        public D3D11()
            : this((SharpDX.Direct3D11.Device)null)
        {
            Reset(1, 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minLevel"></param>
        public D3D11(FeatureLevel minLevel)
        {
            m_device = DXHelper.Create11(DeviceCreationFlags.BgraSupport, minLevel);
            if (m_device == null)
                throw new NotSupportedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dev"></param>
        public D3D11(SharpDX.Direct3D11.Device dev)
        {
            // REMARK: SharpDX.Direct3D.DriverType.Warp works without graphics card!
            if (dev != null)
            {
                //dev.AddReference();
                m_device = dev;
            }
            else
            {
                m_device = DXHelper.Create11(DeviceCreationFlags.BgraSupport);
                if (m_device == null)
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        public D3D11(Adapter a)
        {
            if (a == null)
            {
                m_device = DXHelper.Create11(DeviceCreationFlags.BgraSupport, FeatureLevel.Level_11_0);
                if (m_device == null)
                    throw new NotSupportedException();
            }
            m_device = new SharpDX.Direct3D11.Device(a);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose()
        {
            RenderTarget.Dispose();
            DepthTarget.Dispose();
            Disposer.Dispose(ref m_device);
        }

        /// <summary>
        /// 
        /// </summary>
        public SharpDX.Direct3D11.Device Device => m_device;

        /// <summary>
        /// 
        /// </summary>
        public bool IsDisposed => m_device == null;

        public RenderTarget RenderTarget { get; private set; }

        public RenderTarget DepthTarget { get; private set; }

        //public BaseCamera Camera
        //{
        //    get { return m_camera; }
        //    set { if (value == m_camera) return; m_camera = value; RaiseThisPropertyChanged(); }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dximage"></param>
        public void SetBackBuffer(DXImageSource dximage) { dximage.SetBackBuffer(RenderTarget.GetResource()); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public bool Reset(int w, int h)
        {
            //m_device.GetOrThrow();

            if (w < 1)
                throw new ArgumentOutOfRangeException("w");
            if (h < 1)
                throw new ArgumentOutOfRangeException("h");

            if(RenderTarget != null && RenderTarget.Width == w && RenderTarget.Height == h)
            {
                return false;
            }

            RenderTarget?.Dispose();
            RenderTarget = new RenderTarget(w, h, Format.B8G8R8A8_UNorm, RenderTargetUsage.RenderTarget, ResourceOptionFlags.Shared);
            RenderTarget.Initialize(Device);
            RenderTarget.InitAntialiazedRenderTarget(Device);
            DepthTarget?.Dispose();
            DepthTarget = new RenderTarget(w, h, Format.D24_UNorm_S8_UInt, RenderTargetUsage.DepthTarget);
            DepthTarget.Initialize(Device);
            DepthTarget.InitAntialiazedDepthTarget(Device);
            return true;
        }

        public void Reset(DrawEventArgs args)
        {
            int w = (int)Math.Ceiling(args.RenderSize.Width);
            int h = (int)Math.Ceiling(args.RenderSize.Height);
            if (w < 1 || h < 1)
                return;

            SharpDX.Mathematics.Interop.RawVector2 RenderSize = new SharpDX.Mathematics.Interop.RawVector2(w, h);
            //if (Camera != null)
            //    Camera.AspectRatio = (float)(args.RenderSize.Width / args.RenderSize.Height);

            if (Reset(w, h))
            {
                Resetted?.Invoke(this, args);
            }

            Render(args);

            if (args.Target != null)
                SetBackBuffer(args.Target);
        }

        public void Render(DrawEventArgs args)
        {
            TimeSpan RenderTime = args.TotalTime;
            //if (Camera != null)
            //    Camera.FrameMove(args.DeltaTime);

            //BeginRender(args);
            RenderScene(args);
            //EndRender(args);
        }

        public virtual void RenderScene(DrawEventArgs e)
        {
            Rendering?.Invoke(this, e);
        }

        public System.Windows.Media.Imaging.WriteableBitmap ToImage() { return RenderTarget.GetBitmap(); }

        public BitmapImage ToBitmapImage(WriteableBitmap wbm)
        {
            BitmapImage bmImage = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbm));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }

        private void RaiseThisPropertyChanged([CallerMemberName] string PropertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        #region Fields

        /// <summary>
        /// must be Shared to be displayed in a D3DImage
        /// but, it seams, the Shared flag makes the WARP device to throw the OutOfMem exception...
        /// </summary>
        protected SharpDX.Direct3D11.Device m_device;

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler<DrawEventArgs> Rendering, Resetted;

        #endregion
        }
}