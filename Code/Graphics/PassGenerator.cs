﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Direct3D11;
using Assets;
using Assets.Materials;
using SharpDX.Mathematics.Interop;
using DataModel;

namespace Graphics
{
    public class PassGenerator
    {
        private readonly IDataProvider _dataProvider;
        private readonly D3D11 _renderer;
        private readonly EnvironmentMaps EnvironmentMaps;

        public PassGenerator(IDataProvider dataProvider, D3D11 renderer)
        {
            _dataProvider = dataProvider;
            _renderer = renderer;
            EnvironmentMaps = new EnvironmentMaps(renderer.Device);
        }

        public List<IDrawingExtension> DrawingExtensions { get; } = new List<IDrawingExtension>();

        public IEnumerable<Pass> ConstructWorldPasses()
        {
            IEnumerable<Pass> environmentPasses = GenerateEnvironmentPasses();
            IEnumerable<Pass> worldPasses = GenerateWorldPasses(_renderer.RenderTarget, _renderer.DepthTarget, false, false, true);

            return environmentPasses.Concat(worldPasses);
        }

        private IEnumerable<Pass> GenerateEnvironmentPasses()
        {
            Pass pass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = _renderer.RenderTarget,
                    DepthTarget = _renderer.DepthTarget,
                    UseProvidedAntialiazedRenderTarget = true
                },
                State = new DrawState()
            };

            TextureUsage usage = new TextureUsage
            {
                Usage = TextureMapping.Diffuse,
                Texture = _dataProvider.CurrentEnvironmentDm.Texture
            };

            Drawable environmentSphere = new Drawable
            {
                MeshData = StandardMeshes.UnitSphere,
                World = _dataProvider.CurrentWorldDm,
                Camera = _dataProvider.CurrentCameraDm,
                ObjectGuid = _dataProvider.CurrentEnvironmentDm.Guid,
                Position = _dataProvider.CurrentEnvironmentDm.GetWorldPosition(),
                Material = new BasicMaterial(),
                TextureUsages = new List<TextureUsage> { usage }
            };
            pass.Drawables.Add(environmentSphere);

            IEnumerable<Pass> reflectionPasses = GenerateReflectionPasses();
            IEnumerable<Pass> brdfPasses = GenerateBRDFLUTPasses();

            return new List<Pass> { pass }.Concat(reflectionPasses).Concat(brdfPasses);
        }

        private IEnumerable<Pass> GenerateReflectionPasses()
        {
            if (EnvironmentMaps[EnvironmentMap.Reflection].Image != _dataProvider.CurrentEnvironmentDm.Texture.Asset)
            {
                EnvironmentMaps[EnvironmentMap.Reflection].Invalidate = true;
            }

            if(!EnvironmentMaps[EnvironmentMap.Reflection].Invalidate)
            {
                return new List<Pass>();
            }
            EnvironmentMaps[EnvironmentMap.Reflection].Invalidate = false;
            EnvironmentMaps[EnvironmentMap.Reflection].Image = _dataProvider.CurrentEnvironmentDm.Texture.Asset;

            List<Pass> reflectionPasses = new List<Pass>();
            foreach(CubeFace face in Enum.GetValues(typeof(CubeFace)))
            {
                Pass pass = new Pass
                {
                    Target = new CubeDrawTarget
                    {
                        RenderTarget = EnvironmentMaps[EnvironmentMap.Reflection].RenderTarget,
                        DepthTarget = EnvironmentMaps[EnvironmentMap.Reflection].DepthTarget,
                        RenderTargetView = EnvironmentMaps[EnvironmentMap.Reflection].RenderTarget.GetCubeRenderTargetView(face),
                        DepthStencilView = EnvironmentMaps[EnvironmentMap.Reflection].DepthTarget.GetCubeDepthStencilView(face),
                    },
                    State = new DrawState(),
                    // generate mips after last face is drawn
                    PostDrawGenerateMips = face == CubeFace.NegZ
                };

                TextureUsage usage = new TextureUsage
                {
                    Usage = TextureMapping.Diffuse,
                    Texture = _dataProvider.CurrentEnvironmentDm.Texture
                };

                Drawable environmentSphere = new Drawable
                {
                    MeshData = StandardMeshes.UnitSphere,
                    World = _dataProvider.CurrentWorldDm,
                    Camera = GenerateCubeCamera(face),
                    Position = _dataProvider.CurrentEnvironmentDm.GetWorldPosition(),
                    Material = new BasicMaterial(),
                    TextureUsages = new List<TextureUsage> { usage }
                };
                pass.Drawables.Add(environmentSphere);
                reflectionPasses.Add(pass);
            }

            return reflectionPasses.Concat(GenerateIrradiancePasses()).Concat(GeneratePrefilterSpecularPasses());
        }

        private IEnumerable<Pass> GenerateIrradiancePasses()
        {
            List<Pass> irradiancePasses = new List<Pass>();
            foreach (CubeFace face in Enum.GetValues(typeof(CubeFace)))
            {
                Pass pass = new Pass
                {
                    Target = new CubeDrawTarget
                    {
                        RenderTarget = EnvironmentMaps[EnvironmentMap.PRT].RenderTarget,
                        DepthTarget = EnvironmentMaps[EnvironmentMap.PRT].DepthTarget,
                        RenderTargetView = EnvironmentMaps[EnvironmentMap.PRT].RenderTarget.GetCubeRenderTargetView(face),
                        DepthStencilView = EnvironmentMaps[EnvironmentMap.PRT].DepthTarget.GetCubeDepthStencilView(face),
                    },
                    State = new DrawState()
                };

                Drawable environmentSphere = new Drawable
                {
                    MeshData = StandardMeshes.UnitSphere,
                    World = _dataProvider.CurrentWorldDm,
                    Camera = GenerateCubeCamera(face),
                    Position = _dataProvider.CurrentEnvironmentDm.GetWorldPosition(),
                    Material = new BasicMaterial(),
                    ReflectionMap = EnvironmentMaps[EnvironmentMap.Reflection].RenderTarget,
                    SpecialOutputMode = OutputColorMode.Irradiance
                };
                pass.Drawables.Add(environmentSphere);
                irradiancePasses.Add(pass);
            }


            return irradiancePasses;
        }

        private IEnumerable<Pass> GeneratePrefilterSpecularPasses()
        {
            List<Pass> preFilterPasses = new List<Pass>();
            int mipLevels = EnvironmentMaps[EnvironmentMap.SpecularIBL].RenderTarget.GetResource().Description.MipLevels;
            if(mipLevels == 0)
            {
                mipLevels = 1;
            }
            foreach (CubeFace face in Enum.GetValues(typeof(CubeFace)))
            {
                for (int i = 0; i < mipLevels; ++i)
                {
                    Pass pass = new Pass
                    {
                        Target = new CubeDrawTarget
                        {
                            RenderTarget = EnvironmentMaps[EnvironmentMap.SpecularIBL].RenderTarget,
                            DepthTarget = EnvironmentMaps[EnvironmentMap.SpecularIBL].DepthTarget,
                            RenderTargetView = EnvironmentMaps[EnvironmentMap.SpecularIBL].RenderTarget.GetCubeRenderTargetView(face, i),
                            DepthStencilView = EnvironmentMaps[EnvironmentMap.SpecularIBL].DepthTarget.GetCubeDepthStencilView(face, i),
                        },
                        State = new DrawState()
                    };

                    float roughness = mipLevels == 1 ? 0f : (float)i / (float)(mipLevels - 1);
                    Drawable environmentSphere = new Drawable
                    {
                        MeshData = StandardMeshes.UnitSphere,
                        World = _dataProvider.CurrentWorldDm,
                        Camera = GenerateCubeCamera(face),
                        Position = _dataProvider.CurrentEnvironmentDm.GetWorldPosition(),
                        Material = new BasicMaterial { Roughness = roughness },
                        ReflectionMap = EnvironmentMaps[EnvironmentMap.Reflection].RenderTarget,
                        SpecialOutputMode = OutputColorMode.SpecularIBL
                    };
                    pass.Drawables.Add(environmentSphere);
                    preFilterPasses.Add(pass);
                }
            }


            return preFilterPasses;
        }

        private ICameraDm GenerateCubeCamera(CubeFace face)
        {
            Vector target = new Vector();
            Vector up = new Vector();
            switch(face)
            {
                case CubeFace.PosX:
                    target = Vector.XAxis;
                    up = Vector.YAxis;
                    break;
                case CubeFace.NegX:
                    target = Vector.NegXAxis;
                    up = Vector.YAxis;
                    break;
                case CubeFace.PosY:
                    target = Vector.YAxis;
                    up = Vector.NegZAxis;
                    break;
                case CubeFace.NegY:
                    target = Vector.NegYAxis;
                    up = Vector.ZAxis;
                    break;
                case CubeFace.PosZ:
                    target = Vector.ZAxis;
                    up = Vector.YAxis;
                    break;
                case CubeFace.NegZ:
                    target = Vector.NegZAxis;
                    up = Vector.YAxis;
                    break;
            }
            ICameraDm cameraDm = new CameraDm(new Vector(), target, 1, 90, 1, 10000f)
            {
                // we have to correct the camera positions manually for now
                Position = MatrixHelpers.Create(up.Cross(target).Normalize(), up, target, new Point()),
                Up = up,
                LookAt = target
            };
            return cameraDm;
        }

        public IEnumerable<Pass> GenerateBRDFLUTPasses()
        {
            if(!EnvironmentMaps[EnvironmentMap.BRDF].Invalidate)
            {
                return new List<Pass>();
            }

            EnvironmentMaps[EnvironmentMap.BRDF].Invalidate = false;
            List<Pass> brdfPasses = new List<Pass>();
            Pass pass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = EnvironmentMaps[EnvironmentMap.BRDF].RenderTarget,
                    DepthTarget = EnvironmentMaps[EnvironmentMap.BRDF].DepthTarget
                },
                State = new DrawState()
            };
            Drawable plane = new Drawable
            {
                MeshData = StandardMeshes.UnitPlane,
                Material = new BasicMaterial(),
                World = _dataProvider.CurrentWorldDm,
                Camera = GenerateCubeCamera(CubeFace.PosZ),
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 0, 1)),
                SpecialOutputMode = OutputColorMode.BRDFLUT
            };
            pass.Drawables.Add(plane);
            brdfPasses.Add(pass);
            return brdfPasses;
        }

        public IEnumerable<Pass> ConstructPickingPasses(RenderTarget renderTarget, RenderTarget depthTarget)
        {
            return GenerateWorldPasses(renderTarget, depthTarget, true, true, false).Concat(GenerateExtensionPasses(renderTarget, depthTarget, false));
        }

        private IEnumerable<Pass> GenerateWorldPasses(RenderTarget renderTarget, RenderTarget depthTarget, bool clearDepth, bool clearColor, bool useAntialiazedTarget)
        {
            Pass pass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = renderTarget,
                    DepthTarget = depthTarget,
                    UseProvidedAntialiazedRenderTarget = useAntialiazedTarget
                },
                State = new DrawState
                {
                    ClearDepth = clearDepth,
                    ClearTarget = clearColor
                }
            };

            foreach (IModelDm model in _dataProvider.CurrentWorldDm.Models)
            {
                foreach (IShapeDm shape in model.Shapes.Where(s => s.Mesh != null))
                {
                    Drawable drawable = new Drawable
                    {
                        MeshData = shape.Mesh,
                        World = _dataProvider.CurrentWorldDm,
                        Camera = _dataProvider.CurrentCameraDm,
                        ObjectGuid = shape.Guid,
                        Position = shape.GetWorldPosition(),
                        Material = new PlasticMaterial { Diffuse = new RawColor3(0, 0.5f, 0.5f) },
                        ReflectionMap = EnvironmentMaps[EnvironmentMap.Reflection].RenderTarget,
                        IrradianceMap = EnvironmentMaps[EnvironmentMap.PRT].RenderTarget,
                        PrefilterMap = EnvironmentMaps[EnvironmentMap.SpecularIBL].RenderTarget,
                        BRDFLUTMap = EnvironmentMaps[EnvironmentMap.BRDF].RenderTarget
                    };
                    pass.Drawables.Add(drawable);
                }
            }

            return new List<Pass> { pass };
        }

        public IEnumerable<Pass> ConstructSelectionPasses(RenderTarget renderTarget, RenderTarget depthTarget)
        {
            if(_dataProvider.SelectedDm == null)
            {
                return new List<Pass>();
            }

            RenderTarget target = new RenderTarget(_renderer.RenderTarget.Width, _renderer.RenderTarget.Height, SharpDX.DXGI.Format.B8G8R8A8_UNorm, RenderTargetUsage.RenderTarget);
            target.Initialize(_renderer.Device);
            PostProcessMaterial m1 = new PostProcessMaterial
            {
                PostProcessMap = target
            };

            Pass selectionPass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = target,
                    DepthTarget = depthTarget
                },
                State = new DrawState()
            };

            int i = 0;
            if (_dataProvider.SelectedDm is IModelDm model)
            {
                foreach (IShapeDm shape in model.Shapes.Where(s => s.Mesh != null))
                {
                    Drawable drawable = new Drawable
                    {
                        MeshData = shape.Mesh,
                        World = _dataProvider.CurrentWorldDm,
                        Camera = _dataProvider.CurrentCameraDm,
                        IsPicking = true,
                        Position = shape.GetWorldPosition(),
                        Material = new PlasticMaterial { Diffuse = VectorExtensions.PickingColor(i) }
                    };
                    ++i;
                    selectionPass.Drawables.Add(drawable);
                }
            }
            else if (_dataProvider.SelectedDm is IShapeDm sh)
            {
                IEnumerable<IShapeDm> shapes = sh.GetSelfAndDescendants();
                foreach (IShapeDm shape in shapes.Where(s => s.Mesh != null))
                {
                    Drawable drawable = new Drawable
                    {
                        MeshData = shape.Mesh,
                        World = _dataProvider.CurrentWorldDm,
                        Camera = _dataProvider.CurrentCameraDm,
                        IsPicking = true,
                        Position = shape.GetWorldPosition(),
                        Material = new PlasticMaterial { Diffuse = VectorExtensions.PickingColor(i) }
                    };
                    ++i;
                    selectionPass.Drawables.Add(drawable);
                }
            }

            Pass edgeDetectionPass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = renderTarget,
                    DepthTarget = depthTarget
                },
                State = new DrawState()
            };
            Drawable plane = new Drawable
            {
                MeshData = StandardMeshes.UnitPlane,
                Material = m1,
                World = _dataProvider.CurrentWorldDm,
                Camera = GenerateCubeCamera(CubeFace.PosZ),
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 0, 1)),
                SpecialOutputMode = OutputColorMode.SelectionEdge
            };
            edgeDetectionPass.Drawables.Add(plane);

            Pass mergePass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = _renderer.RenderTarget,
                    DepthTarget = _renderer.DepthTarget,
                    UseProvidedAntialiazedRenderTarget = true
                },
                State = new DrawState()
            };
            Drawable plane2 = new Drawable
            {
                MeshData = StandardMeshes.UnitPlane,
                Material = new PostProcessMaterial 
                { 
                    PostProcessMap = _renderer.RenderTarget.CopyAntialiazed(_renderer.Device),
                    MergeMap = renderTarget // edge detection buffer
                },
                World = _dataProvider.CurrentWorldDm,
                Camera = GenerateCubeCamera(CubeFace.PosZ),
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 0, 1)),
                SpecialOutputMode = OutputColorMode.Merge
            };
            mergePass.Drawables.Add(plane2);

            return new List<Pass> { selectionPass, edgeDetectionPass, mergePass };
        }

        public IEnumerable<Pass> ConstructPostProcessPasses()
        {
            List<Pass> postProcessPasses = new List<Pass>();
            Pass pass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = _renderer.RenderTarget,
                    DepthTarget = _renderer.DepthTarget,
                    UseProvidedAntialiazedRenderTarget = true
                },
                State = new DrawState()
            };
            Drawable plane = new Drawable
            {
                MeshData = StandardMeshes.UnitPlane,
                Material = new PostProcessMaterial 
                { 
                    PostProcessMap = _renderer.RenderTarget.CopyAntialiazed(_renderer.Device),
                    OutputGamma = 2.2f
                },
                World = _dataProvider.CurrentWorldDm,
                Camera = GenerateCubeCamera(CubeFace.PosZ),
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 0, 1))
            };
            pass.Drawables.Add(plane);
            postProcessPasses.Add(pass);
            return postProcessPasses;
        }

        public IEnumerable<Pass> ConstructVisualizerPasses()
        {
            Pass pass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = _renderer.RenderTarget,
                    DepthTarget = _renderer.DepthTarget,
                    UseProvidedAntialiazedRenderTarget = true
                },
                State = new DrawState
                {
                    ClearDepth = false,
                    ClearTarget = false
                }
            };

            Drawable xAxis = new Drawable
            {
                MeshData = StandardMeshes.AxisCylinder,
                World = _dataProvider.CurrentWorldDm,
                Camera = _dataProvider.CurrentCameraDm,
                Position = MatrixHelpers.Create(Vector.NegYAxis, Vector.XAxis, Vector.ZAxis, new Point(1.25f, 0, 0)),
                Material = new BasicMaterial { Diffuse = new RawColor3(1, 0, 0)}
            };
            pass.Drawables.Add(xAxis);

            Drawable yAxis = new Drawable
            {
                MeshData = StandardMeshes.AxisCylinder,
                World = _dataProvider.CurrentWorldDm,
                Camera = _dataProvider.CurrentCameraDm,
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 1.25f, 0)),
                Material = new BasicMaterial { Diffuse = new RawColor3(0, 1, 0) }
            };
            pass.Drawables.Add(yAxis);

            Drawable zAxis = new Drawable
            {
                MeshData = StandardMeshes.AxisCylinder,
                World = _dataProvider.CurrentWorldDm,
                Camera = _dataProvider.CurrentCameraDm,
                Position = MatrixHelpers.Create(Vector.XAxis, Vector.NegZAxis, Vector.YAxis, new Point(0, 0, -1.25f)),
                Material = new BasicMaterial { Diffuse = new RawColor3(0, 0, 1) }
            };

            pass.Drawables.Add(zAxis);

            return new List<Pass> { pass }.Concat(GenerateExtensionPasses());
        }

        private IEnumerable<Pass> GenerateExtensionPasses()
        {
            return GenerateExtensionPasses(_renderer.RenderTarget, _renderer.DepthTarget, true);
        }

        private IEnumerable<Pass> GenerateExtensionPasses(RenderTarget renderTarget, RenderTarget depthTarget, bool useAntialiazedTarget)
        {
            Pass extensionPass = new Pass
            {
                Target = new DrawTarget
                {
                    RenderTarget = renderTarget,
                    DepthTarget = depthTarget,
                    UseProvidedAntialiazedRenderTarget = useAntialiazedTarget
                },
                State = new DrawState
                {
                    ClearDepth = true,
                    ClearTarget = false
                }
            };

            foreach (IDrawingExtension extension in DrawingExtensions)
            {
                extension.ConstructPass(extensionPass);
            }

            return new List<Pass> { extensionPass };
        }

        public void RegisterExtension(IDrawingExtension extension)
        {
            DrawingExtensions.Add(extension);
        }

        public void UnregisterExtension(IDrawingExtension extension)
        {
            DrawingExtensions.Remove(extension);
        }
    }
}
