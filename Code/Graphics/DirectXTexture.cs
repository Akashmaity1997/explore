﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Device = SharpDX.Direct3D11.Device;
using SharpDX;

namespace Graphics
{
    public class DirectXTexture
    {
        private Dictionary<Image, TextureWrapper> _imageToTextures = new Dictionary<Image, TextureWrapper>();

        public TextureWrapper Draw(Image image, Device device)
        {
            TextureWrapper texture;
            if (_imageToTextures.TryGetValue(image, out texture))
            {
                return texture;
            }
            else
            {
                texture = new TextureWrapper();
                _imageToTextures.Add(image, texture);
            }

            texture.Init(device, image);
            return texture;
        }
    }
}
