﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    public class Pass
    {
        public List<Drawable> Drawables = new List<Drawable>();

        public DrawTarget Target { get; set; }

        public DrawState State { get; set; }

        public bool PostDrawGenerateMips { get; set; }
    }
}
