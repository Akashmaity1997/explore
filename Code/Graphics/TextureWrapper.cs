﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Assets;
using Assets.Interfaces;
using SharpDX;
using SharpDX.Mathematics.Interop;
using Device = SharpDX.Direct3D11.Device;

namespace Graphics
{
    public class TextureWrapper : ITextureResource
    {
        private Texture2D _texture;
        private ShaderResourceView _resourceView;
        private SamplerState _samplerState;

        public int Width => _texture.Description.Width;

        public int Height => _texture.Description.Height;

        public void Init(Device device, Image image)
        {
            Format format = Format.B8G8R8A8_UNorm;
            int bpp = 4;
            if (image.Format == ImageFormat.RGBA8)
            {
                format = Format.R8G8B8A8_UNorm;
            }
            else if (image.Format == ImageFormat.RGBE8)
            {
                format = Format.R9G9B9E5_Sharedexp;
            }
            else if (image.Format == ImageFormat.RGBA16)
            {
                format = Format.R16G16B16A16_Float;
                bpp = 8;
            }
            else if (image.Format == ImageFormat.RGBA32)
            {
                format = Format.R32G32B32A32_Float;
                bpp = 16;
            }

            Texture2DDescription desc = new Texture2DDescription
            {
                Width = image.Width,
                Height = image.Height,
                ArraySize = 1,
                BindFlags = SharpDX.Direct3D11.BindFlags.ShaderResource,
                Usage = SharpDX.Direct3D11.ResourceUsage.Default,
                CpuAccessFlags = SharpDX.Direct3D11.CpuAccessFlags.None,
                Format = format,
                MipLevels = 1,
                OptionFlags = SharpDX.Direct3D11.ResourceOptionFlags.None,
                SampleDescription = new SharpDX.DXGI.SampleDescription(1, 0),
            };

            if (device.CheckFormatSupport(format).HasFlag(FormatSupport.RenderTarget))
            {
                desc.BindFlags |= BindFlags.RenderTarget;
            }

            using (DataStream dataStream = new DataStream(desc.Width * desc.Height * bpp, false, true))
            {
                if (format == Format.R9G9B9E5_Sharedexp)
                {
                    for (int i = 0; i < image.Buffer.Length / 4; ++i)
                    {
                        int e = image.Buffer[i * 4 + 3];
                        e -= 128 - 16;
                        if (e < 1)
                        {
                            e = 1;
                        }
                        if (e > 31)
                        {
                            e = 31;
                        }

                        int byt = image.Buffer[i * 4] << 0 |
                            image.Buffer[i * 4 + 1] << 9 |
                            image.Buffer[i * 4 + 2] << 18 |
                            (e & 0x1f) << 27;

                        dataStream.Write(byt);
                    }
                }
                else
                {
                    dataStream.Write(image.Buffer, 0, image.Buffer.Length);
                }

                dataStream.Seek(0, System.IO.SeekOrigin.Begin);
                _texture = new Texture2D(device, desc, new DataRectangle(dataStream.DataPointer, desc.Width * bpp));
            }
        }

        public ShaderResourceView GetShaderResourceView(Device device)
        {
            if(_resourceView != null)
            {
                return _resourceView;
            }

            _resourceView = new ShaderResourceView(device, _texture);
            return _resourceView;
        }

        public SamplerState GetSamplerState(Device device)
        {
            if(_samplerState != null)
            {
                return _samplerState;
            }

            _samplerState = new SamplerState(device, new SamplerStateDescription
            {
                Filter = Filter.MinMagMipLinear,
                MaximumAnisotropy = 16,
                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp,
                AddressW = TextureAddressMode.Wrap,
                ComparisonFunction = Comparison.Always,
                BorderColor = new RawColor4(1, 1, 1, 0),
                MaximumLod = float.MaxValue,
                MinimumLod = 0,
                MipLodBias = -1
            });

            return _samplerState;
        }

        public void Dispose()
        {
            _texture.Dispose();
            _resourceView.Dispose();
            _samplerState.Dispose();
        }
    }
}
