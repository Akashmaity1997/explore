﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using Device = SharpDX.Direct3D11.Device;
using SharpDX.Mathematics.Interop;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using Assets.Interfaces;
using System.IO;

namespace Graphics
{
    public enum RenderTargetUsage
    {
        RenderTarget,
        DepthTarget,
        CubeTarget,
        CubeDepthTarget
    }

    public enum CubeFace
    {
        PosX,
        NegX,
        PosY,
        NegY,
        PosZ,
        NegZ,
    }

    public class RenderTarget : ITextureResource
    {
        private const int _maxMipLevels = 5;

        private readonly int _width, _height;
        private readonly Format _format;
        private readonly RenderTargetUsage _usage;
        private readonly ResourceOptionFlags _flags;
        private readonly bool _createMips;
        private readonly bool _createFullMipChain;

        private Texture2D _texture;
        private Texture2D _antialiazedTexture;
        private RenderTargetView _targetView;
        private RenderTargetView _antialiazedTargetView;
        private DepthStencilView _targetDepthView;
        private DepthStencilView _antialiazedTargetDepthView;
        private RenderTargetView[,] _cubeTargetViews = new RenderTargetView[6, _maxMipLevels];
        private DepthStencilView[,] _cubeDepthViews = new DepthStencilView[6, _maxMipLevels];

        private ShaderResourceView _resourceView;
        private SamplerState _samplerState;

        public RenderTarget(int width, int height, Format format, RenderTargetUsage usage, ResourceOptionFlags options = ResourceOptionFlags.None, bool createMips = false, bool createFullMipChain = false)
        {
            _width = width;
            _height = height;
            _format = format;
            _usage = usage;
            _flags = options;
            _createMips = createMips;
            _createFullMipChain = createFullMipChain;
        }

        public int Width => _width;

        public int Height => _height;

        public Format Format => _format;

        public RenderTargetUsage Usage => _usage;

        public bool IsMipRenderTarget => _createMips;

        public void Initialize(Device device)
        {
            int mipLevels = _createFullMipChain ? 0 : _maxMipLevels;
            switch(_usage)
            {
                case RenderTargetUsage.RenderTarget:
                    Texture2DDescription renderDesc = new Texture2DDescription
                    {
                        Width = _width,
                        Height = _height,
                        Format = _format,
                        BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                        CpuAccessFlags = CpuAccessFlags.None,
                        Usage = ResourceUsage.Default,
                        MipLevels = _createMips ? mipLevels : 1,
                        OptionFlags = ResourceOptionFlags.None | _flags,
                        ArraySize = 1,
                        SampleDescription = new SampleDescription(1, 0)
                    };

                    _texture = new Texture2D(device, renderDesc);
                    _targetView = new RenderTargetView(device, _texture);
                    break;
                case RenderTargetUsage.DepthTarget:
                    Texture2DDescription depthDesc = new Texture2DDescription
                    {
                        Width = _width,
                        Height = _height,
                        Format = _format,
                        BindFlags = BindFlags.DepthStencil,
                        CpuAccessFlags = CpuAccessFlags.None,
                        Usage = ResourceUsage.Default,
                        MipLevels = _createMips ? mipLevels : 1,
                        OptionFlags = ResourceOptionFlags.None | _flags,
                        ArraySize = 1,
                        SampleDescription = new SampleDescription(1, 0)
                    };

                    _texture = new Texture2D(device, depthDesc);
                    _targetDepthView = new DepthStencilView(device, _texture);
                    break;
                case RenderTargetUsage.CubeTarget:
                    Texture2DDescription desc = new Texture2DDescription
                    {
                        Width = _width,
                        Height = _height,
                        Format = _format,
                        BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                        CpuAccessFlags = CpuAccessFlags.None,
                        Usage = ResourceUsage.Default,
                        MipLevels = _createMips ? mipLevels : 1,
                        OptionFlags = ResourceOptionFlags.TextureCube | _flags,
                        ArraySize = 6,
                        SampleDescription = new SampleDescription(1, 0) 
                    };

                    _texture = new Texture2D(device, desc);
                    foreach (CubeFace face in Enum.GetValues(typeof(CubeFace)))
                    {
                        for (int i = 0; i < (_createMips ? _maxMipLevels : 1); ++i)
                        {
                            _cubeTargetViews[(int)face, i] = GenerateCubeRenderTargetView(device, face, _texture, i);
                        }
                    }

                    break;
                case RenderTargetUsage.CubeDepthTarget:
                    Texture2DDescription desc1 = new Texture2DDescription
                    {
                        Width = _width,
                        Height = _height,
                        Format = _format,
                        BindFlags = BindFlags.DepthStencil,
                        CpuAccessFlags = CpuAccessFlags.None,
                        Usage = ResourceUsage.Default,
                        MipLevels = _createMips ? mipLevels : 1,
                        OptionFlags = ResourceOptionFlags.TextureCube | _flags,
                        ArraySize = 6,
                        SampleDescription = new SampleDescription(1, 0)
                    };

                    _texture = new Texture2D(device, desc1);

                    foreach(CubeFace face in Enum.GetValues(typeof(CubeFace)))
                    {
                        for (int i = 0; i < (_createMips ? _maxMipLevels : 1); ++i)
                        {
                            _cubeDepthViews[(int)face, i] = GenerateCubeDepthTargetView(device, face, _texture, i);
                        }
                    }

                    break;
            }
        }

        public Texture2D GetResource()
        {
            return _texture;
        }

        public Texture2D GetAntialiazedResource()
        {
            return _antialiazedTexture;
        }

        public void InitAntialiazedRenderTarget(Device device)
        {
            Texture2DDescription renderDesc = new Texture2DDescription
            {
                Width = _width,
                Height = _height,
                Format = _format,
                BindFlags = BindFlags.RenderTarget,
                CpuAccessFlags = CpuAccessFlags.None,
                Usage = ResourceUsage.Default,
                MipLevels = 1,
                OptionFlags = ResourceOptionFlags.None,
                ArraySize = 1,
                SampleDescription = new SampleDescription(8, 0)
            };

            _antialiazedTexture = new Texture2D(device, renderDesc);
            _antialiazedTargetView = new RenderTargetView(device, _antialiazedTexture);
        }

        public void InitAntialiazedDepthTarget(Device device)
        {
            Texture2DDescription depthDesc = new Texture2DDescription
            {
                Width = _width,
                Height = _height,
                Format = _format,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None,
                Usage = ResourceUsage.Default,
                MipLevels = 1,
                OptionFlags = ResourceOptionFlags.None,
                ArraySize = 1,
                SampleDescription = new SampleDescription(8, 0)
            };

            _antialiazedTexture = new Texture2D(device, depthDesc);
            _antialiazedTargetDepthView = new DepthStencilView(device, _antialiazedTexture);
        }

        private RenderTargetView GenerateCubeRenderTargetView(Device device, CubeFace face, Texture2D resource, int mipLevel)
        {
            RenderTargetViewDescription desc = new RenderTargetViewDescription
            {
                Format = resource.Description.Format,
                Texture2DArray = new RenderTargetViewDescription.Texture2DArrayResource
                {
                    ArraySize = 1,
                    FirstArraySlice = (int)face,
                    MipSlice = mipLevel
                },
                Dimension = RenderTargetViewDimension.Texture2DArray,
                //Texture3D = new RenderTargetViewDescription.Texture3DResource
                //{
                //    MipSlice = 0,
                //    DepthSliceCount = 1,
                //    FirstDepthSlice = 0
                //}
            };

            return new RenderTargetView(device, resource, desc);
        }

        private DepthStencilView GenerateCubeDepthTargetView(Device device, CubeFace face, Texture2D resource, int mipLevel)
        {
            DepthStencilViewDescription desc = new DepthStencilViewDescription
            {
                Format = resource.Description.Format,
                Texture2DArray = new DepthStencilViewDescription.Texture2DArrayResource
                {
                    ArraySize = 1,
                    FirstArraySlice = (int)face,
                    MipSlice = mipLevel
                },
                Dimension = DepthStencilViewDimension.Texture2DArray
            };

            return new DepthStencilView(device, resource, desc);
        }

        public RenderTargetView GetCubeRenderTargetView(CubeFace face, int mipLevel = 0)
        {
            return _cubeTargetViews[(int)face, mipLevel];
        }

        public DepthStencilView GetCubeDepthStencilView(CubeFace face, int mipLevel = 0)
        {
            return _cubeDepthViews[(int)face, mipLevel];
        }

        public RenderTargetView GetRenderTargetView()
        {
            return _targetView;
        }

        public RenderTargetView GetAntialiazedRenderTargetView()
        {
            return _antialiazedTargetView;
        }

        public DepthStencilView GetAntialiazedDepthStencilView()
        {
            return _antialiazedTargetDepthView;
        }

        public DepthStencilView GetDepthStencilView()
        {
            return _targetDepthView;
        }

        public ShaderResourceView GetShaderResourceView(Device device)
        {
            if (_resourceView != null)
            {
                return _resourceView;
            }

            _resourceView = new ShaderResourceView(device, _texture);
            
            return _resourceView;
        }

        public SamplerState GetSamplerState(Device device)
        {
            if (_samplerState != null)
            {
                return _samplerState;
            }

            _samplerState = new SamplerState(device, new SamplerStateDescription
            {
                Filter = Filter.MinLinearMagPointMipLinear,
                MaximumAnisotropy = 16,
                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp,
                AddressW = TextureAddressMode.Clamp,
                ComparisonFunction = Comparison.Always,
                BorderColor = new RawColor4(1, 1, 1, 0),
                MaximumLod = float.MaxValue,
                MinimumLod = 0,
                MipLodBias = -1
            });

            return _samplerState;
        }

        public WriteableBitmap GetBitmap()
        {
            return _texture.GetBitmap();
        }

        public RenderTarget Copy(Device device)
        {
            RenderTarget target = new RenderTarget(_width, _height, _format, _usage, _flags, _createMips, _createFullMipChain);
            target.Initialize(device);
            device.ImmediateContext.CopyResource(_texture, target._texture);
            return target;
        }

        public RenderTarget CopyAntialiazed(Device device)
        {
            RenderTarget target = new RenderTarget(_width, _height, _format, _usage, _flags, _createMips, _createFullMipChain);
            target.Initialize(device);
            device.ImmediateContext.ResolveSubresource(_antialiazedTexture, 0, target._texture, 0, _format);
            return target;
        }

        public void Dispose()
        {
            _targetView?.Dispose();
            _antialiazedTargetView?.Dispose();
            _antialiazedTargetDepthView?.Dispose();
            _targetDepthView?.Dispose();
            foreach(DepthStencilView cubeDepthView in _cubeDepthViews)
            {
                cubeDepthView?.Dispose();
            }
            foreach (RenderTargetView cubeTargetView in _cubeTargetViews)
            {
                cubeTargetView?.Dispose();
            }
            _samplerState?.Dispose();
            _resourceView?.Dispose();
            _texture?.Dispose();
            _antialiazedTexture?.Dispose();
        }
    }
}
