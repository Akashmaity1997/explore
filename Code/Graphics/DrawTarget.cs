﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D11;

namespace Graphics
{
    public class DrawTarget
    {
        public RenderTarget RenderTarget { get; set; }

        public RenderTarget DepthTarget { get; set; }

        public bool UseProvidedAntialiazedRenderTarget { get; set; }
    }

    public class CubeDrawTarget : DrawTarget
    {
        public RenderTargetView RenderTargetView { get; set; }

        public DepthStencilView DepthStencilView { get; set; }
    }
}
