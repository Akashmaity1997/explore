cbuffer VertexConstants : register(b0)
{
	float4x4 gWorld;
	float4x4 gView;
	float4x4 gProj;
	float4 eyePos;
};

cbuffer PerObjectConstants : register(b1)
{
	float4x4 position;
	float4x4 scale;
	float4x4 scaleinvtranspose; // inverse transpose of scale matrix
};

cbuffer PerObjectDecalCount : register(b4)
{
	int decalCount;
	int padding3;
	float2 padding4;
};

struct Light
{
	float4 lPosition;
	float brightness;
	float3 lcolour;
	float4 direction;
	float coneangle;
	float3 padding1;
	int isPoint;
	int isDirectional;
	int isSpot;
	int isCircular;
};

cbuffer LightConstants : register(b2)
{
	Light lights[10];
};

cbuffer LightCount : register(b3)
{
	int lightCount;
	int padding1;
	float2 padding2;
};

Texture2D text[10] : register(t0); // per object texture collection

SamplerState samplerState // make this buffer
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;
	AddressU = WRAP;
	AddressV = WRAP;
};


struct VS_IN
{
	float4 pos : POSITION;
	float4 col : COLOR;
	float4 tex : TEXCOORDS;
	float4 tangent : TANGENT;
	float4 binormal : BINORMAL;
	float4 normal : NORMAL;
};

struct PS_IN
{
	float4 pos : SV_POSITION;
	float4 col : COLOR;
	float4 tex : TEXCOORDS;
	float4 lightVector[10] : LIGHTVECTOR;
	float4 brightness[10] : BRIGHTNESS;
	float4 eyeVector : EYEVECTOR;
	float4 lightVectorNonTS : LIGHTVECTORUNTRANSFORMED; // not transformed to tangent space
};

PS_IN VS_fn( VS_IN input )
{
	PS_IN output = (PS_IN)0;

	// transform tangent space to world space
	/*float4 tangent = normalize(mul(input.tangent, scaleinvtranspose));
	float4 binormal = normalize(mul(input.binormal, scaleinvtranspose));
	float4 normal = normalize(mul(input.normal, scaleinvtranspose));*/
	float4 tangentp = normalize(mul(input.tangent, position));
	float4 binormalp = normalize(mul(input.binormal, position));
	float4 normalp = normalize(mul(input.normal, position));
	float4 tangentw = normalize(mul(tangentp, gWorld));
	float4 binormalw = normalize(mul(binormalp, gWorld));
	float4 normalw = normalize(mul(normalp, gWorld));

	output.tex = input.tex;

	//some hardcoded white ambient light for now
	float4 ambient = float4(0.2, 0.2, 0.2, 0);
	output.col = input.col + ambient;

	float4x4 worldViewProj = mul(mul(gWorld, gView), gProj);
	float4 pos = mul(input.pos, position);
	//pos = mul(pos, position);
	float4 posw = mul(pos, gWorld); // vertex in world space

	float4x4 ts = transpose(float4x4(tangentw.x, tangentw.y, tangentw.z, 0,
		binormalw.x, binormalw.y, binormalw.z, 0,
		normalw.x, normalw.y, normalw.z, 0,
		posw.x, posw.y, posw.z, 1));

	output.pos = mul(posw, mul(gView, gProj)); // perspective divide is done by GPU

	[unroll(10)]
	for (int i = 0; i < lightCount; ++i)
	{
		float4 lpos = mul(lights[i].lPosition, gWorld); // light in world space
		float4 lvect = lpos - posw; // light vector in world space
		output.brightness[i] = lights[i].brightness / (length(lvect) * length(lvect));
		float4 lightVectts;
		// calculate light vector and transform to tangent space and pass to pixel shader
		if (lights[i].isPoint == 1 || lights[i].isSpot == 1)
		{
			float4 lightVect = normalize(lvect);
			output.lightVectorNonTS = lightVect;
			lightVectts = mul(lightVect, ts); // light vector in tangent space
		}
		else
		{
			lightVectts = normalize(mul(lights[i].direction, ts)); // directional light
		}

		output.lightVector[i] = normalize(lightVectts); // normalized light vector in tangent space
	}

	// calculate eye vector and transform to tangent space and pass to pixel shader
	float4 eyePosW = mul(eyePos, gWorld); // eye in world space
	float4 eyeVect = normalize(eyePosW - posw);
	float4 eyeVectts = mul(eyeVect, ts); // eye vector in tangent space
	output.eyeVector = normalize(eyeVectts); // normalized eye vector in tangent space

	return output;
}

float4 PS_fn(PS_IN input) : SV_Target
{
	for (int i = 0; i < lightCount; ++i)
	{
		// diffuse lighting
		float diffuseConst = max(0, dot(normalize(input.lightVector[i]), float4(0,0,1,0)));
		float lumen = diffuseConst * input.brightness[i];
		float4 diffuse = float4(lumen * lights[i].lcolour.x, lumen * lights[i].lcolour.y, lumen * lights[i].lcolour.z, 0);
		float4 specular;

		//specular lighting
		if (diffuseConst = 0)
		{
			specular = float4(0, 0, 0, 0);
		}
		else
		{
			float specularPower = 8.0f; // hardcoded specular power
			float specularConst = max(0, dot(input.eyeVector, reflect(normalize(-input.lightVector[i]), float4(0,0,1,0))));
			float specularlumen = pow(specularConst, specularPower) * input.brightness[i];
			specular = float4(specularlumen * lights[i].lcolour.x, specularlumen * lights[i].lcolour.y, specularlumen * lights[i].lcolour.z, 0);
		}
		if (lights[i].isSpot == 1 && lights[i].isCircular == 1)
		{
			input.col += ((diffuse + specular) * max(0, pow(dot(normalize(-input.lightVectorNonTS), -lights[i].direction), 8.0f))); // relate power to coneangle
		}
		else
		{
			input.col += diffuse + specular;
		}
	}

	// texturing
	float2 t = float2(input.tex.x, input.tex.y);
	float4 texColour = float4(1, 1, 1, 1);
	for (int i = 0; i < decalCount; ++i)
	{
		texColour = text[i].Sample(samplerState, t);
		input.col = texColour * input.col;
	}

	return input.col;
}

PS_IN VSPickable_fn(VS_IN input)
{
	PS_IN output = (PS_IN)0;

	float4x4 worldViewProj = mul(mul(gWorld, gView), gProj);
	//float4 scaled = mul(input.pos, scale); // scale vertex
	float4 pos = mul(input.pos, position); // transform vertex in local space 
	float4 posw = mul(pos, gWorld); // vertex in world space

	output.pos = mul(posw, mul(gView, gProj)); // perspective divide is done by GPU

	output.col = input.col;

	return output;
}

float4 PSPickable_fn(PS_IN input) : SV_Target
{
	return input.col;
}

void doSaturation(float mod, inout float4 col : r_doWrite, out float3 colDerived : r_doWriteDerived)
{

}

//RasterizerState WireframeRS
//{
//	FillMode = Wireframe;
//	CullMode = Back;
//	FrontCounterClockwise = false;
//	// Default values used for any properties we do not set.
//};

technique11 PickTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VSPickable_fn()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PSPickable_fn()));
		//SetRasterizerState(WireframeRS);
	}
};

technique11 MainTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS_fn()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS_fn()));
		//SetRasterizerState(WireframeRS);
	}
};

