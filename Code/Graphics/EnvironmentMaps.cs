﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using SharpDX.Mathematics.Interop;
using Device = SharpDX.Direct3D11.Device;

namespace Graphics
{
    public enum EnvironmentMap
    {
        // this holds the environment texture as a cube map
        Reflection,
        // this holds the radiance of the environment texture,
        // as if it acts as a diffuse IBL
        PRT,
        // this holds the specular radiance of IBL (prefiltered BRDF)
        SpecularIBL,
        // BRDF LUT
        BRDF
    }

    public class EnvironmentMaps : IDisposable
    {
        private static readonly RawInt3 EnvMapResolution = new RawInt3(256, 256, 0);
        private static readonly RawInt3 PRTMapResolution = new RawInt3(1024, 1024, 0);
        private static readonly RawInt3 SpecularIBLMapResolution = new RawInt3(256, 256, 0);
        private static readonly RawInt3 BRDFLUTResolution = new RawInt3(256, 256, 0);
        private readonly List<EnvironmentEntry> _entries = new List<EnvironmentEntry>();
        
        public EnvironmentMaps(Device device)
        {
            foreach(EnvironmentMap map in Enum.GetValues(typeof(EnvironmentMap)))
            {
                RawInt3 res = new RawInt3();
                bool cubeMap = true;
                bool createMips = false;
                bool createFullMips = false;
                if(map == EnvironmentMap.Reflection)
                {
                    res = EnvMapResolution;
                    createMips = true;
                    createFullMips = true;
                }
                else if(map == EnvironmentMap.PRT)
                {
                    res = PRTMapResolution;
                }
                else if (map == EnvironmentMap.SpecularIBL)
                {
                    res = SpecularIBLMapResolution;
                    createMips = true;
                }
                else if (map == EnvironmentMap.BRDF)
                {
                    res = BRDFLUTResolution;
                    cubeMap = false;
                }

                EnvironmentEntry e = new EnvironmentEntry
                {
                    MapType = map,
                    RenderTarget = new RenderTarget(res.X, res.Y, SharpDX.DXGI.Format.R16G16B16A16_Float, cubeMap ? RenderTargetUsage.CubeTarget : RenderTargetUsage.RenderTarget, SharpDX.Direct3D11.ResourceOptionFlags.None, createMips, createFullMips),
                    DepthTarget = new RenderTarget(res.X, res.Y, SharpDX.DXGI.Format.D24_UNorm_S8_UInt, cubeMap ? RenderTargetUsage.CubeDepthTarget : RenderTargetUsage.DepthTarget, SharpDX.Direct3D11.ResourceOptionFlags.None, createMips, createFullMips),
                    Invalidate = true
                };
                e.RenderTarget.Initialize(device);
                e.DepthTarget.Initialize(device);
                _entries.Add(e);
            }
        }

        public EnvironmentEntry this[EnvironmentMap map]
        {
            get
            {
                return _entries.First(e => e.MapType == map);
            }
        }

        public void Dispose()
        {
            foreach(EnvironmentEntry e in _entries)
            {
                e.RenderTarget.Dispose();
                e.DepthTarget.Dispose();
            }
        }
    }

    public class EnvironmentEntry
    {
        public EnvironmentMap MapType { get; set; }
        public Image Image { get; set; }
        public RenderTarget RenderTarget { get; set; }
        public RenderTarget DepthTarget { get; set; }
        public bool Invalidate { get; set; }
    }
}
