﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using Graphics.ShaderInterfaces;
using Assets.Interfaces;
using DataModel;

namespace Graphics
{
    public interface IDrawingManager
    {
        void Draw();
        void Draw(Pass pass);
        D3D11 Renderer { get; }
    }

    public class DrawingManager : IDrawingManager
    {
        private readonly ShaderManager _shaderManager;
        private readonly DirectXMesh _directXMesh;
        private readonly PassGenerator _passGenerator;
        
        public DrawingManager(D3D11 renderer, PassGenerator passGenerator)
        {
            Renderer = renderer;
            _shaderManager = new ShaderManager(renderer);
            _directXMesh = new DirectXMesh();
            _passGenerator = passGenerator;
        }

        public D3D11 Renderer { get; }

        public void Draw()
        {
            IEnumerable<Pass> worldPasses = _passGenerator.ConstructWorldPasses();
            foreach(Pass pass in worldPasses)
            {
                Draw(pass);
            }
            IEnumerable<Pass> visualizerPasses = _passGenerator.ConstructVisualizerPasses();
            foreach (Pass pass in visualizerPasses)
            {
                Draw(pass);
            }
            Renderer.Device.ImmediateContext.Flush();
            RenderTarget tempRenderTarget = new RenderTarget(Renderer.RenderTarget.Width, Renderer.RenderTarget.Height, SharpDX.DXGI.Format.B8G8R8A8_UNorm, RenderTargetUsage.RenderTarget);
            tempRenderTarget.Initialize(Renderer.Device);
            RenderTarget tempDepthTarget = new RenderTarget(Renderer.RenderTarget.Width, Renderer.RenderTarget.Height, SharpDX.DXGI.Format.D24_UNorm_S8_UInt, RenderTargetUsage.DepthTarget);
            tempDepthTarget.Initialize(Renderer.Device);
            IEnumerable<Pass> selectionPasses = _passGenerator.ConstructSelectionPasses(tempRenderTarget, tempDepthTarget);
            foreach (Pass pass in selectionPasses)
            {
                Draw(pass);
                Renderer.Device.ImmediateContext.Flush();
            }
            selectionPasses.SelectMany(pass => pass.Drawables).ToList().ForEach(d => d.Material.Dispose());
            tempRenderTarget.Dispose();
            tempDepthTarget.Dispose();
            IEnumerable<Pass> postProcessPasses = _passGenerator.ConstructPostProcessPasses();
            foreach (Pass pass in postProcessPasses)
            {
                Draw(pass);
            }
            Renderer.Device.ImmediateContext.Flush();
            Renderer.Device.ImmediateContext.ResolveSubresource(Renderer.RenderTarget.GetAntialiazedResource(), 0, Renderer.RenderTarget.GetResource(), 0, Renderer.RenderTarget.Format);
            postProcessPasses.SelectMany(pass => pass.Drawables).ToList().ForEach(d => d.Material.Dispose());
        }

        public void Draw(Pass pass)
        {
            RenderTargetView renderTargetView = pass.Target.RenderTarget.GetRenderTargetView();
            DepthStencilView depthTargetView = pass.Target.DepthTarget.GetDepthStencilView();
            if (pass.Target is CubeDrawTarget cubeTarget)
            {
                renderTargetView = cubeTarget.RenderTargetView;
                depthTargetView = cubeTarget.DepthStencilView;
            }
            else if(pass.Target.UseProvidedAntialiazedRenderTarget)
            {
                renderTargetView = pass.Target.RenderTarget.GetAntialiazedRenderTargetView();
                depthTargetView = pass.Target.DepthTarget.GetAntialiazedDepthStencilView();
            }

            Renderer.Device.ImmediateContext.OutputMerger.SetTargets(depthTargetView, renderTargetView);

            float width = pass.Target.RenderTarget.Width;
            float height = pass.Target.RenderTarget.Height;
            if(renderTargetView.Description.Dimension == RenderTargetViewDimension.Texture2D)
            {
                width = (float)(width * Math.Pow(0.5, renderTargetView.Description.Texture2D.MipSlice));
                height = (float)(height * Math.Pow(0.5, renderTargetView.Description.Texture2D.MipSlice));
            }
            else if (renderTargetView.Description.Dimension == RenderTargetViewDimension.Texture2DArray)
            {
                width = (float)(width * Math.Pow(0.5, renderTargetView.Description.Texture2DArray.MipSlice));
                height = (float)(height * Math.Pow(0.5, renderTargetView.Description.Texture2DArray.MipSlice));
            }
            Renderer.Device.ImmediateContext.Rasterizer.SetViewport(new SharpDX.Mathematics.Interop.RawViewportF { X = 0, Y = 0, Width = width, Height = height, MinDepth = 0.0f, MaxDepth = 1.0f });
            
            if (pass.State.ClearDepth)
            {
                Renderer.Device.ImmediateContext.ClearDepthStencilView(depthTargetView, DepthStencilClearFlags.Depth, 1f, 0);
            }
            if (pass.State.ClearTarget)
            {
                Renderer.Device.ImmediateContext.ClearRenderTargetView(renderTargetView, new SharpDX.Mathematics.Interop.RawColor4());
            }
            //Renderer.Device.ImmediateContext.Flush();
            foreach (Drawable drawable in pass.Drawables)
            {
                GeneratedShader shader = _shaderManager.GetShaderForDrawable(drawable);
                Renderer.Device.ImmediateContext.VertexShader.Set(shader.VertexShader);
                Renderer.Device.ImmediateContext.PixelShader.Set(shader.PixelShader);
                _shaderManager.UploadConstants(drawable, shader);
                _directXMesh.Draw(Renderer.Device, drawable.MeshData, shader.VertexShaderByteCode);
                //Renderer.Device.ImmediateContext.Flush();
            }

            if(pass.PostDrawGenerateMips)
            {
                Renderer.Device.ImmediateContext.GenerateMips(pass.Target.RenderTarget.GetShaderResourceView(Renderer.Device));
            }
        }
    }
}
