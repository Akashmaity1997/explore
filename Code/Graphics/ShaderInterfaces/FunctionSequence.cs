﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class FunctionSequence
    {
        public List<ShaderFragment> VertexShaderFragments = new List<ShaderFragment>();
        public List<ShaderFragment> PixelShaderFragments = new List<ShaderFragment>();
        public List<ShaderFragment> ComputeShaderFragments = new List<ShaderFragment>();

        public override bool Equals(object obj)
        {
            if(obj is FunctionSequence other)
            {
                return VertexShaderFragments.SequenceEqual(other.VertexShaderFragments) &&
                    PixelShaderFragments.SequenceEqual(other.PixelShaderFragments) &&
                    ComputeShaderFragments.SequenceEqual(other.ComputeShaderFragments);
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            VertexShaderFragments.ForEach(f => hash += f.GetHashCode() * 17);
            PixelShaderFragments.ForEach(f => hash += f.GetHashCode() * 17);
            ComputeShaderFragments.ForEach(f => hash += f.GetHashCode() * 17);
            return hash;
        }
    }
}
