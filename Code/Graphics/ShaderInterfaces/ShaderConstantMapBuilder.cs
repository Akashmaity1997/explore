﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using DataModel;
using Assets;
using System.Reflection;
using Assets.Materials;

namespace Graphics.ShaderInterfaces
{
    public class ShaderConstantMapBuilder
    {
        private readonly ShaderProductionContext _context;
        private Dictionary<string, IShaderConstantProducer> _constantRegister = new Dictionary<string, IShaderConstantProducer>();

        public ShaderConstantMapBuilder(ShaderProductionContext context)
        {
            _context = context;


            RegisterType(typeof(Drawable));
            RegisterType(typeof(MaterialDefinition), "Material");
            RegisterType(typeof(BasicMaterial), "Material");
            RegisterType(typeof(PostProcessMaterial), "Material");
            RegisterType(typeof(PlasticMaterial), "Material");

            RegisterConstant("GWorld", (Drawable drawable) =>
            {
                return drawable.World.GetWorldPosition();
            });
            RegisterConstant("GView", (Drawable drawable) =>
            {
                return drawable.Camera.ViewMatrix;
            });
            RegisterConstant("GProj", (Drawable drawable) =>
            {
                return drawable.Camera.ProjectionMatrix;
            });
            RegisterConstant("EyePos", (Drawable drawable) =>
            {
                RawVector3 pos = new RawVector3(drawable.Camera.Position.M41, drawable.Camera.Position.M42,
                    drawable.Camera.Position.M43);
                return pos;
            });
            RegisterConstant("DiffuseMap", (Drawable drawable) =>
            {
                return drawable.TextureUsages.First(t => t.Usage == TextureMapping.Diffuse);
            });
        }

        private void RegisterType(Type type)
        {
            PropertyInfo[] pis = type.GetProperties();
            foreach(PropertyInfo pi in pis)
            {
                if(_constantRegister.ContainsKey(pi.Name))
                {
                    continue;
                }

                RegisterConstant(pi.Name);
            }
        }

        private void RegisterType(Type type, params string[] propertyChain)
        {
            PropertyInfo[] pis = type.GetProperties();
            foreach (PropertyInfo pi in pis)
            {
                if (_constantRegister.ContainsKey(pi.Name))
                {
                    continue;
                }

                RegisterConstant(pi.Name, propertyChain);
            }
        }

        private void RegisterConstant(string constantName)
        {
            _constantRegister.Add(constantName, new ReflectionBasedShaderConstantProducer(_context, constantName));
        }

        private void RegisterConstant(string constantName, string[] propertyNames)
        {
            _constantRegister.Add(constantName, new ReflectionBasedShaderConstantProducer(_context, constantName, propertyNames));
        }

        private void RegisterConstant(string constantName, Func<Drawable, object> objectDelegate)
        {
            _constantRegister.Add(constantName, new ShaderConstantProducer(_context, objectDelegate));
        }

        public bool GetProducer(string constantName, out IShaderConstantProducer producer)
        {
            return _constantRegister.TryGetValue(constantName, out producer);
        }
    }
}
