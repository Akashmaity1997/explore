﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public enum ShaderToken
    {
        Undef = -1,
        Uint = 0,
        Uint2 = 1,
        Uint3 = 2,
        Uint4 = 3,
        Int = 4,
        Int2 = 5,
        Int3 = 6,
        Int4 = 7,
        Float = 8,
        Float2 = 9,
        Float3 = 10,
        Float4 = 11,
        Bool = 12,
        Void = 13,
        In = 14,
        Out = 15,
        InOut = 16,
        Texture2D = 17,
        Texture2DF4 = 18,
        Texture2DArrayF4 = 19,
        RWTexture2D = 20,
        RWTexture2DArray = 21,
        TextureCube = 22,
        SamplerState = 23,
        Float21 = 24,
        Float22 = 25,
        Float23 = 26,
        Float24 = 27,
        Float31 = 28,
        Float32 = 29,
        Float33 = 30,
        Float34 = 31,
        Float41 = 32,
        Float42 = 33,
        Float43 = 34,
        Float44 = 35,
    }

    public enum ShaderType
    {
        Vertex,
        Pixel,
        Compute
    }

    public enum Semantic
    {
        POSITION,
        POSITION2,
        POSITION3,
        COLOR,
        TEXCOORD,
        TEXCOORD2,
        TEXCOORD3,
        NORMAL,
        TANGENT,
        SV_POSITION,
        OUTWORLDPOSITION,
        OUTCOLOR,
        OUTTEXCOORD,
        OUTNORMAL,
        OUTWORLDNORMAL,
        OUTTANGENT,
        OUTWORLDTANGENT,
        SV_TARGET,
        SV_DispatchThreadID
    }
}
