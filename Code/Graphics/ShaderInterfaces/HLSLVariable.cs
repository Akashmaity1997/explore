﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class HLSLVariable
    {
        public string Name { get; set; }

        public ShaderToken Type { get; set; }

        public override string ToString()
        {
            return $"{Type} {Name}";
        }
    }
}
