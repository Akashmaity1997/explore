﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderSequenceGenerator
    {
        private List<ShaderFragment> _vertexShaderFragments = new List<ShaderFragment>();
        private List<ShaderFragment> _pixelShaderFragments = new List<ShaderFragment>();
        private List<ShaderFragment> _computeShaderFragments = new List<ShaderFragment>();

        public FunctionSequence GenerateFragmentSequence(ShaderRequirement requirement)
        {
            AddVertexShaderFragment("vsOpen");
            if (!requirement.IsPicking)
            {
                if (requirement.HasUVs)
                {
                    AddVertexShaderFragment("passTexCoord");
                }
                if (requirement.HasTangents)
                {
                    AddVertexShaderFragment("passTangent");
                }
            }
            
            AddVertexShaderFragment("toWorld");
            if(!requirement.IsPicking && requirement.HasTangents)
            {
                AddVertexShaderFragment("toWorldTangent");
            }
            if(requirement.DoReflection && !requirement.IsPicking)
            {
                //AddVertexShaderFragment("passEye");
            }
            AddVertexShaderFragment("toProj");
            //AddVertexShaderFragment("passColor");
            AddVertexShaderFragment("vsClose");

            AddPixelShaderFragment("psOpen");
            AddPixelShaderFragment("readColor");
            if (requirement.IsPicking)
            {
                //AddPixelShaderFragment("readPickingColor");
            }
            else
            {
                if (requirement.HasUVs)
                {
                    AddPixelShaderFragment("inputTex");
                }
                if(requirement.HasTangents)
                {
                    AddPixelShaderFragment("inputTangents");
                }
                
                if (requirement.DoIrradiance)
                {
                    AddPixelShaderFragment("computeIrradiance");
                }
                else if(requirement.DoPreFilter)
                {
                    AddPixelShaderFragment("computePrefilter");
                }
                else if(requirement.DoBRDFLUT)
                {
                    AddPixelShaderFragment("computeBRDF");
                }
                else
                {
                    if (requirement.DoDiffuse)
                    {
                        AddPixelShaderFragment("readDiffuseMap");
                    }
                    if (requirement.DoReflection)
                    {
                        AddPixelShaderFragment("lightingIBL");
                    }
                }
            }
            if (requirement.HasTangents)
            {
                //AddPixelShaderFragment("inputTangents");
            }
            if(!requirement.IsPicking)
            {
                //AddPixelShaderFragment("gammaCorrect");
            }
            if(requirement.DoPostProcess)
            {
                if (requirement.DoSelectionEdge)
                {
                    AddPixelShaderFragment("selectionEdge");
                }
                else if (requirement.DoMerge)
                {
                    AddPixelShaderFragment("mergeTargets");
                }
                else
                {
                    AddPixelShaderFragment("postProcess");
                }
            }
            //AddPixelShaderFragment("inputColor");
            AddPixelShaderFragment("psClose");

            FunctionSequence sequence = new FunctionSequence
            {
                VertexShaderFragments = _vertexShaderFragments.ToList(),
                PixelShaderFragments = _pixelShaderFragments.ToList(),
                ComputeShaderFragments = _computeShaderFragments.ToList()
            };

            return sequence;
        }

        private void AddVertexShaderFragment(string functionName, params string[] args)
        {
            _vertexShaderFragments.Add(new ShaderFragment { FuncName = functionName, Args = args });
        }

        private void AddPixelShaderFragment(string functionName, params string[] args)
        {
            _pixelShaderFragments.Add(new ShaderFragment { FuncName = functionName, Args = args });
        }

        private void AddComputeShaderFragment(string functionName, params string[] args)
        {
            _computeShaderFragments.Add(new ShaderFragment { FuncName = functionName, Args = args });
        }
    }
}
