﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

namespace Graphics.ShaderInterfaces
{
    public class GeneratedShader
    {
        public ShaderBytecode VertexShaderByteCode { get; private set; }

        public ShaderBytecode PixelShaderByteCode { get; private set; }

        public ShaderBytecode ComputeShaderByteCode { get; private set; }

        public VertexShader VertexShader { get; private set; }

        public PixelShader PixelShader { get; private set; }

        public ComputeShader ComputeShader { get; private set; }

        public static GeneratedShader InitializeFromShaderTexts(Device device, ShaderConstantMapper shaderConstantMapper, string vertexShaderText, string pixelShaderText, string computeShaderText)
        {
            GeneratedShader shader = new GeneratedShader();
            if (!string.IsNullOrEmpty(vertexShaderText))
            {
                try
                {
                    ShaderBytecode vertexShaderBytecode = ShaderBytecode.Compile(vertexShaderText, "VertexShaderMain", "vs_5_0", ShaderFlags.None, EffectFlags.None);
                    shader.VertexShaderByteCode = vertexShaderBytecode;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            if (!string.IsNullOrEmpty(pixelShaderText))
            {
                try
                {
                    ShaderBytecode pixelShaderByteCode = ShaderBytecode.Compile(pixelShaderText, "PixelShaderMain", "ps_5_0", ShaderFlags.None, EffectFlags.None);
                    shader.PixelShaderByteCode = pixelShaderByteCode;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            if(!string.IsNullOrEmpty(computeShaderText))
            {
                ShaderBytecode computeShaderByteCode = ShaderBytecode.Compile(computeShaderText, "ComputeShaderMain", "cs_5_0", ShaderFlags.None, EffectFlags.None);
                shader.ComputeShaderByteCode = computeShaderByteCode;
            }

            if (shader.VertexShaderByteCode != null)
            {
                shader.VertexShader = new VertexShader(device, shader.VertexShaderByteCode);
            }
            if (shader.PixelShaderByteCode != null)
            {
                shader.PixelShader = new PixelShader(device, shader.PixelShaderByteCode);
            }
            if(shader.ComputeShaderByteCode != null)
            {
                shader.ComputeShader = new ComputeShader(device, shader.ComputeShaderByteCode);
            }

            shaderConstantMapper.Init(shader);

            return shader;
        }
    }
}
