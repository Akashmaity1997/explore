﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;
using SharpDX.Mathematics.Interop;
using Assets.Interfaces;

namespace Graphics.ShaderInterfaces
{
    public class ShaderConstantMapper
    {
        private readonly Device _device;
        private readonly ShaderConstantMapBuilder _shaderConstantMapBuilder;
        private readonly DirectXTexture _directXTexture;
        private readonly Dictionary<GeneratedShader, ShaderConstantBufferRecord> _record = new Dictionary<GeneratedShader, ShaderConstantBufferRecord>();
        private Dictionary<string, object> _providedConstants = new Dictionary<string, object>();

        public ShaderConstantMapper(Device device, ShaderConstantMapBuilder shaderConstantMapBuilder)
        {
            _device = device;
            _shaderConstantMapBuilder = shaderConstantMapBuilder;
            _directXTexture = new DirectXTexture();
        }

        public void AddProvidedConstant(string constantName, object value)
        {
            _providedConstants.Add(constantName, value);
        }

        public void InitializeProvidedConstants(Dictionary<string, object> constantMap)
        {
            _providedConstants = constantMap;
        }

        public void Init(GeneratedShader shader)
        {
            ShaderConstantBufferRecord record = new ShaderConstantBufferRecord
            {
                VsBufs = new List<ShaderConstantBuffer>(),
                PsBufs = new List<ShaderConstantBuffer>(),
                CsBufs = new List<ShaderConstantBuffer>(),
                VsResourceBindings = new List<ShaderResourceBinding>(),
                PsResourceBindings = new List<ShaderResourceBinding>(),
                CsResourceBindings = new List<ShaderResourceBinding>()
            };
            ShaderReflection r = null;
            if (shader.VertexShader != null)
            {
                r = new ShaderReflection(shader.VertexShaderByteCode);
                {
                    for (int i = 0; i < r.Description.ConstantBuffers; ++i)
                    {
                        ConstantBuffer cbuf = r.GetConstantBuffer(i);
                        ShaderConstantBuffer buf = new ShaderConstantBuffer
                        {
                            Slot = i,
                            Description = cbuf.Description,
                            Variables = new List<ShaderVariable>()
                        };
                        for (int j = 0; j < cbuf.Description.VariableCount; ++j)
                        {
                            ShaderReflectionVariable v = cbuf.GetVariable(j);
                            ShaderVariable sv = new ShaderVariable
                            {
                                Description = v.Description
                            };
                            buf.Variables.Add(sv);
                        }

                        record.VsBufs.Add(buf);
                    }

                    for (int i = 0; i < r.Description.BoundResources; ++i)
                    {
                        InputBindingDescription desc = r.GetResourceBindingDescription(i);
                        record.VsResourceBindings.Add(new ShaderResourceBinding
                        {
                            Desc = desc
                        });
                    }
                }
            }
            if (shader.PixelShader != null)
            {
                r = new ShaderReflection(shader.PixelShaderByteCode);
                {
                    for (int i = 0; i < r.Description.ConstantBuffers; ++i)
                    {
                        ConstantBuffer cbuf = r.GetConstantBuffer(i);
                        ShaderConstantBuffer buf = new ShaderConstantBuffer
                        {
                            Slot = i,
                            Description = cbuf.Description,
                            Variables = new List<ShaderVariable>()
                        };
                        for (int j = 0; j < cbuf.Description.VariableCount; ++j)
                        {
                            ShaderReflectionVariable v = cbuf.GetVariable(j);
                            ShaderVariable sv = new ShaderVariable
                            {
                                Description = v.Description
                            };
                            buf.Variables.Add(sv);
                        }

                        record.PsBufs.Add(buf);
                    }

                    for (int i = 0; i < r.Description.BoundResources; ++i)
                    {
                        InputBindingDescription desc = r.GetResourceBindingDescription(i);
                        record.PsResourceBindings.Add(new ShaderResourceBinding
                        {
                            Desc = desc
                        });
                    }
                }
            }
            if (shader.ComputeShader != null)
            {
                r = new ShaderReflection(shader.ComputeShaderByteCode);
                {
                    for (int i = 0; i < r.Description.ConstantBuffers; ++i)
                    {
                        ConstantBuffer cbuf = r.GetConstantBuffer(i);
                        ShaderConstantBuffer buf = new ShaderConstantBuffer
                        {
                            Slot = i,
                            Description = cbuf.Description,
                            Variables = new List<ShaderVariable>()
                        };
                        for (int j = 0; j < cbuf.Description.VariableCount; ++j)
                        {
                            ShaderReflectionVariable v = cbuf.GetVariable(j);
                            ShaderVariable sv = new ShaderVariable
                            {
                                Description = v.Description
                            };
                            buf.Variables.Add(sv);
                        }

                        record.CsBufs.Add(buf);
                    }

                    for (int i = 0; i < r.Description.BoundResources; ++i)
                    {
                        InputBindingDescription desc = r.GetResourceBindingDescription(i);
                        record.CsResourceBindings.Add(new ShaderResourceBinding
                        {
                            Desc = desc
                        });
                    }
                }
            }

            _record.Add(shader, record);
        }

        private void WriteConstant(object constant, DataStream dataStream, long offset)
        {
            dataStream.Seek(offset, System.IO.SeekOrigin.Begin);
            if(constant is RawMatrix cm)
            {
                dataStream.Write(cm.M11);
                dataStream.Write(cm.M21);
                dataStream.Write(cm.M31);
                dataStream.Write(cm.M41);
                dataStream.Write(cm.M12);
                dataStream.Write(cm.M22);
                dataStream.Write(cm.M32);
                dataStream.Write(cm.M42);
                dataStream.Write(cm.M13);
                dataStream.Write(cm.M23);
                dataStream.Write(cm.M33);
                dataStream.Write(cm.M43);
                dataStream.Write(cm.M14);
                dataStream.Write(cm.M24);
                dataStream.Write(cm.M34);
                dataStream.Write(cm.M44);
            }
            else if(constant is RawVector3 vector)
            {
                dataStream.Write(vector.X);
                dataStream.Write(vector.Y);
                dataStream.Write(vector.Z);
            }
            else if (constant is RawVector2 vector2)
            {
                dataStream.Write(vector2.X);
                dataStream.Write(vector2.Y);
            }
            else if(constant is RawColor3 color)
            {
                dataStream.Write(color.R);
                dataStream.Write(color.G);
                dataStream.Write(color.B);
            }
            else if(constant is float f)
            {
                dataStream.Write(f);
            }
            else
            {
                throw new System.NotImplementedException();
            }
        }

        private void WriteBuffer(ShaderConstantBuffer buf, ShaderType type)
        {
            using (DataStream stream = new DataStream(buf.Description.Size, false, true))
            {
                foreach (ShaderVariable v in buf.Variables)
                {
                    string name = v.Name;
                    if (_shaderConstantMapBuilder.GetProducer(name, out IShaderConstantProducer producer))
                    {
                        object constant = producer.ProduceConstant();
                        WriteConstant(constant, stream, v.Description.StartOffset);
                    }
                    else if(_providedConstants.TryGetValue(name, out object constant))
                    {
                        WriteConstant(constant, stream, v.Description.StartOffset);
                    }
                }

                BufferDescription desc = new BufferDescription
                {
                    Usage = ResourceUsage.Default,
                    BindFlags = BindFlags.ConstantBuffer,
                    SizeInBytes = buf.Description.Size,
                    CpuAccessFlags = CpuAccessFlags.None,
                    OptionFlags = ResourceOptionFlags.None,
                    StructureByteStride = 0
                };
                stream.Position = 0;
                using (Buffer gpuBuf = new Buffer(_device, stream, desc))
                {
                    if (type == ShaderType.Vertex)
                    {
                        _device.ImmediateContext.VertexShader.SetConstantBuffer(buf.Slot, gpuBuf);
                    }
                    else if(type == ShaderType.Pixel)
                    {
                        _device.ImmediateContext.PixelShader.SetConstantBuffer(buf.Slot, gpuBuf);
                    }
                    else if(type == ShaderType.Compute)
                    {
                        _device.ImmediateContext.ComputeShader.SetConstantBuffer(buf.Slot, gpuBuf);
                    }
                }
            }
        }

        private void WriteTexture(ITextureResource resource, int textureSlot, int samplerSlot, ShaderType type)
        {
            ShaderResourceView resourceView = resource.GetShaderResourceView(_device);
            SamplerState samplerState = resource.GetSamplerState(_device);
            if (type == ShaderType.Vertex)
            {
                _device.ImmediateContext.VertexShader.SetShaderResource(textureSlot, resourceView);
                _device.ImmediateContext.VertexShader.SetSampler(samplerSlot, samplerState);
            }
            else if (type == ShaderType.Pixel)
            {
                _device.ImmediateContext.PixelShader.SetShaderResource(textureSlot, resourceView);
                _device.ImmediateContext.PixelShader.SetSampler(samplerSlot, samplerState);
            }
            else if (type == ShaderType.Compute)
            {
                _device.ImmediateContext.ComputeShader.SetShaderResource(textureSlot, resourceView);
                _device.ImmediateContext.ComputeShader.SetSampler(samplerSlot, samplerState);
            }
        }

        public void UploadConstants(GeneratedShader shader)
        {
            ShaderConstantBufferRecord record = _record[shader];
            foreach (ShaderConstantBuffer buf in record.VsBufs)
            {
                WriteBuffer(buf, ShaderType.Vertex);
            }

            foreach (ShaderConstantBuffer buf in record.PsBufs)
            {
                WriteBuffer(buf, ShaderType.Pixel);
            }

            foreach(ShaderConstantBuffer buf in record.CsBufs)
            {
                WriteBuffer(buf, ShaderType.Compute);
            }

            foreach(ShaderResourceBinding binding in record.VsResourceBindings)
            {
                if(binding.Desc.Type != ShaderInputType.Texture)
                {
                    continue;
                }

                if (_shaderConstantMapBuilder.GetProducer(binding.Desc.Name, out IShaderConstantProducer producer))
                {
                    object constant = producer.ProduceConstant();
                    if (constant is TextureUsage texUsage)
                    {
                        TextureWrapper texWrapper = _directXTexture.Draw(texUsage.Texture.Asset, _device);
                        WriteTexture(texWrapper, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Vertex);
                    }
                    else if(constant is RenderTarget renderTarget)
                    {
                        WriteTexture(renderTarget, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Vertex);
                    }
                }
            }

            foreach (ShaderResourceBinding binding in record.PsResourceBindings)
            {
                if (binding.Desc.Type != ShaderInputType.Texture)
                {
                    continue;
                }

                if (_shaderConstantMapBuilder.GetProducer(binding.Desc.Name, out IShaderConstantProducer producer))
                {
                    object constant = producer.ProduceConstant();
                    if (constant is TextureUsage texUsage)
                    {
                        TextureWrapper texWrapper = _directXTexture.Draw(texUsage.Texture.Asset, _device);
                        WriteTexture(texWrapper, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Pixel);
                    }
                    else if (constant is RenderTarget renderTarget)
                    {
                        WriteTexture(renderTarget, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Pixel);
                    }
                }
            }

            foreach (ShaderResourceBinding binding in record.CsResourceBindings)
            {
                if (binding.Desc.Type != ShaderInputType.Texture)
                {
                    continue;
                }

                if (_shaderConstantMapBuilder.GetProducer(binding.Desc.Name, out IShaderConstantProducer producer))
                {
                    object constant = producer.ProduceConstant();
                    if (constant is TextureUsage texUsage)
                    {
                        TextureWrapper texWrapper = _directXTexture.Draw(texUsage.Texture.Asset, _device);
                        WriteTexture(texWrapper, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Compute);
                    }
                    else if (constant is RenderTarget renderTarget)
                    {
                        WriteTexture(renderTarget, binding.Desc.BindPoint, binding.Desc.BindPoint, ShaderType.Compute);
                    }
                }
            }
        }

        public struct ShaderConstantBuffer
        {
            public int Slot { get; set; }

            public ConstantBufferDescription Description { get; set; }

            public List<ShaderVariable> Variables { get; set; }
        }

        struct ShaderConstantBufferRecord
        {
            public List<ShaderConstantBuffer> VsBufs { get; set; }

            public List<ShaderConstantBuffer> PsBufs { get; set; }

            public List<ShaderConstantBuffer> CsBufs { get; set; }

            public List<ShaderResourceBinding> VsResourceBindings { get; set; }

            public List<ShaderResourceBinding> PsResourceBindings { get; set; }

            public List<ShaderResourceBinding> CsResourceBindings { get; set; }
        }

        public struct ShaderVariable
        {
            public string Name => Description.Name;

            public ShaderVariableDescription Description { get; set; }
        }

        public struct ShaderResourceBinding
        {
            public InputBindingDescription Desc { get; set; }
        }
    }
}
