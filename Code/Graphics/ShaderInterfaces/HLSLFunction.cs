﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public struct FuncArg
    {
        public ShaderToken PassType { get; set; }

        public ShaderToken Type { get; set; }

        public string Name { get; set; }

        public string Semantic { get; set; }

        public override string ToString()
        {
            if(string.IsNullOrEmpty(Semantic))
            {
                return $"{PassType} {Type} {Name}";
            }
            return $"{PassType} {Type} {Name} : {Semantic}";
        }
    }

    public class HLSLFunction
    {
        public string Name { get; set; }

        public ShaderToken Type { get; set; }

        public List<FuncArg> Args = new List<FuncArg>();

        public (int, int) BracketBeginEnd;

        public override string ToString()
        {
            return $"{Type} {Name} {BracketBeginEnd.Item1}->{BracketBeginEnd.Item2}";
        }
    }
}
