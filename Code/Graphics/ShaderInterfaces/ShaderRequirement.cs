﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    /// <summary>
    /// This holds the required items to construct a shader
    /// </summary>
    public class ShaderRequirement
    {
        public bool EnableLighting { get; set; }

        public bool HasUVs { get; set; }

        public bool HasTangents { get; set; }

        public bool IsPicking { get; set; }

        public bool DoDiffuse { get; set; }

        public bool DoReflection { get; set; }

        public bool DoIrradiance { get; set; }

        public bool DoPreFilter { get; set; }

        public bool DoBRDFLUT { get; set; }

        public bool DoPostProcess { get; set; }

        public bool DoSelectionEdge { get; set; }

        public bool DoMerge { get; set; }

        public override bool Equals(object obj)
        {
            if(obj is ShaderRequirement other)
            {
                return EnableLighting == other.EnableLighting &&
                    HasTangents == other.HasTangents &&
                    HasUVs == other.HasUVs &&
                    IsPicking == other.IsPicking &&
                    DoDiffuse == other.DoDiffuse &&
                    DoReflection == other.DoReflection &&
                    DoIrradiance == other.DoIrradiance &&
                    DoPreFilter == other.DoPreFilter &&
                    DoBRDFLUT == other.DoBRDFLUT &&
                    DoPostProcess == other.DoPostProcess &&
                    DoSelectionEdge == other.DoSelectionEdge &&
                    DoMerge == other.DoMerge;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return EnableLighting.GetHashCode() * 17 +
                HasTangents.GetHashCode() * 17 +
                HasUVs.GetHashCode() * 17 +
                IsPicking.GetHashCode() * 17 +
                DoDiffuse.GetHashCode() * 17 +
                DoReflection.GetHashCode() * 17 +
                DoIrradiance.GetHashCode() * 17 +
                DoPreFilter.GetHashCode() * 17 +
                DoBRDFLUT.GetHashCode() * 17 +
                DoPostProcess.GetHashCode() * 17 +
                DoSelectionEdge.GetHashCode() * 17 +
                DoMerge.GetHashCode() + 17;
        }
    }
}
