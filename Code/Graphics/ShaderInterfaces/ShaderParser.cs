﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderParser
    {
        private readonly string _shaderText;
        private readonly List<HLSLVariable> hLSLVariables = new List<HLSLVariable>();
        private readonly List<HLSLFunction> hLSLFunctions = new List<HLSLFunction>();
        private readonly Dictionary<HLSLFunction, DependencyMap> _functionToDependencyMapping = new Dictionary<HLSLFunction, DependencyMap>();

        private int pos;
        private int _foundToken;
        private string _buffer;
        private const int _uint = 0;
        private const int _uint2 = 1;
        private const int _uint3 = 2;
        private const int _uint4 = 3;
        private const int _int = 4;
        private const int _int2 = 5;
        private const int _int3 = 6;
        private const int _int4 = 7;
        private const int _float = 8;
        private const int _float2 = 9;
        private const int _float3 = 10;
        private const int _float4 = 11;
        private const int _bool = 12;
        private const int _void = 13;
        private const int _in = 14;
        private const int _out = 15;
        private const int _inout = 16;
        private const int _texture2D = 17;
        private const int _texture2DF4 = 18;
        private const int _texture2DArrayF4 = 19;
        private const int _rwTexture2D = 20;
        private const int _rwTexture2DArray = 21;
        private const int _textureCube = 22;
        private const int _samplerState = 23;
        private const int _float21 = 24;
        private const int _float22 = 25;
        private const int _float23 = 26;
        private const int _float24 = 27;
        private const int _float31 = 28;
        private const int _float32 = 29;
        private const int _float33 = 30;
        private const int _float34 = 31;
        private const int _float41 = 32;
        private const int _float42 = 33;
        private const int _float43 = 34;
        private const int _float44 = 35;

        public ShaderParser(string shaderText)
        {
            _shaderText = shaderText;
            _buffer = "";
            pos = 0;
            _foundToken = -1;
        g:  char ch = Get();
            _buffer += ch;
            switch(ch)
            {
                case ' ':
                    _buffer = _buffer.Trim(new[] { ' ' });
                    char peeked = Peek();
                    if (peeked == ' ')
                    {

                    }
                    else if(peeked == '(')
                    {
                        if(_foundToken != -1 && !string.IsNullOrEmpty(_buffer))
                        {
                            ExtractHLSLFunction();
                            _foundToken = -1;
                            _buffer = "";
                        }
                    }
                    else if(peeked == ';')
                    {
                        if (_foundToken != -1 && !string.IsNullOrEmpty(_buffer))
                        {
                            ExtractHLSLVariable();
                            _foundToken = -1;
                            _buffer = "";
                        }
                    }
                    else
                    {
                        _foundToken = ExtractToken(_buffer);
                        if(_foundToken != -1)
                        {
                            _buffer = "";
                        }
                    }
                    _buffer = "";
                    goto g;
                case '(':
                    if (_foundToken != -1 && !string.IsNullOrEmpty(_buffer))
                    {
                        ExtractHLSLFunction();
                        _foundToken = -1;
                        _buffer = "";
                    }
                    goto g;
                case ';':
                    _buffer = _buffer.Trim(new[] { ';' });
                    if (_foundToken != -1 && !string.IsNullOrEmpty(_buffer))
                    {
                        ExtractHLSLVariable();
                        _foundToken = -1;
                        _buffer = "";
                    }
                    goto g;
                case '\r':
                    if(Peek() == '\n')
                    {
                        Get();
                        _buffer = "";
                    }
                    goto g;
                case '\t':
                    _buffer = "";
                    goto g;
                case '\n':
                    _buffer = "";
                    goto g;
                case '\0':
                    break;
                default:
                    goto g;
            }
            
            foreach(HLSLFunction func in hLSLFunctions)
            {
                ComputeDependency(func);
            }
        }

        private char Get()
        {
            if(pos == _shaderText.Length)
            {
                return '\0';
            }

            char ch = _shaderText[pos];
            ++pos;
            return ch;
        }

        private char Peek()
        {
            if (pos == _shaderText.Length)
            {
                return '\0';
            }

            return _shaderText[pos];
        }

        private int ExtractToken(string token)
        {
            switch(token)
            {
                case "uint":
                    return _uint;
                case "uint2":
                    return _uint2;
                case "uint3":
                    return _uint3;
                case "uint4":
                    return _uint4;
                case "int":
                    return _int;
                case "int2":
                    return _int2;
                case "int3":
                    return _int3;
                case "int4":
                    return _int4;
                case "float":
                    return _float;
                case "float2":
                    return _float2;
                case "float3":
                    return _float3;
                case "float4":
                    return _float4;
                case "bool":
                    return _bool;
                case "void":
                    return _void;
                case "in":
                    return _in;
                case "out":
                    return _out;
                case "inout":
                    return _inout;
                case "Texture2D":
                    return _texture2D;
                case "Texture2D<float4>":
                    return _texture2DF4;
                case "Texture2DArray<float4>":
                    return _texture2DArrayF4;
                case "RWTexture2D":
                    return _rwTexture2D;
                case "RWTexture2DArray":
                    return _rwTexture2DArray;
                case "TextureCube":
                    return _textureCube;
                case "SamplerState":
                    return _samplerState;
                case "float2x1":
                    return _float21;
                case "float2x2":
                    return _float22;
                case "float2x3":
                    return _float23;
                case "float2x4":
                    return _float24;
                case "float3x1":
                    return _float31;
                case "float3x2":
                    return _float32;
                case "float3x3":
                    return _float33;
                case "float3x4":
                    return _float34;
                case "float4x1":
                    return _float41;
                case "float4x2":
                    return _float42;
                case "float4x3":
                    return _float43;
                case "float4x4":
                    return _float44;
            }

            return -1;
        }

        private void ExtractHLSLVariable()
        {
            HLSLVariable variable = new HLSLVariable 
            { 
                Name = _buffer, 
                Type = (ShaderToken)_foundToken 
            };
            hLSLVariables.Add(variable);
        }

        private void ExtractHLSLFunction()
        {
            HLSLFunction func = new HLSLFunction
            {
                Name = _buffer.Trim(new[] { '(' }),
                Type = (ShaderToken)_foundToken
            };

            string buf = "";
            char c = Get();
            while (c != ')')
            {
                buf += c;
                c = Get();
            }

            buf = buf.Trim(new[] { '(' });
            string[] tokens = buf.Split(',').Select(t => t.Trim(new[] { ' ', '\n', '\t', '\r' }))
                .Select(t => t.Trim(new[] { ' ', '\n', '\t', '\r' }))
                .Select(t => t.Trim(new[] { ' ', '\n', '\t', '\r' }))
                .Select(t => t.Trim(new[] { ' ', '\n', '\t', '\r' }))
                .ToArray();
            foreach(string token in tokens)
            {
                FuncArg arg = new FuncArg();
                arg.PassType = ShaderToken.In;
                arg.Type = ShaderToken.Undef;
                string[] subTokens = token.Split(' ');
                foreach(string subToken in subTokens)
                {
                    int tokenNum = ExtractToken(subToken);
                    if (tokenNum == _in ||
                        tokenNum == _inout ||
                        tokenNum == _out)
                    {
                        arg.PassType = (ShaderToken)tokenNum;
                    }
                    else if(tokenNum != -1)
                    {
                        arg.Type = (ShaderToken)tokenNum;
                    }
                    else if(arg.Type != ShaderToken.Undef && string.IsNullOrEmpty(arg.Name))
                    {
                        arg.Name = subToken;
                    }
                    else if(!string.IsNullOrEmpty(arg.Name))
                    {
                        arg.Semantic = subToken;
                    }
                }

                func.Args.Add(arg);
            }

            int open = -1, openPos = -1, close = -1, closePos = -1;
            while(true)
            {
                char ch = Get();
                if (ch == '{')
                {
                    if(openPos == -1)
                    {
                        openPos = pos - 1;
                    }
                    ++open;
                }
                else if (ch == '}')
                {
                    closePos = pos - 1;
                    ++close;
                }

                if(open == close && open != -1)
                {
                    break;
                }
            }

            func.BracketBeginEnd = (openPos, closePos);
            // TODO: Add asserts
            hLSLFunctions.Add(func);
        }

        private string GetFunctionBody(HLSLFunction function)
        {
            return _shaderText.Substring(function.BracketBeginEnd.Item1, function.BracketBeginEnd.Item2 - function.BracketBeginEnd.Item1 + 1);
        }

        private void ComputeVariableDependency(HLSLFunction function, ref IList<HLSLVariable> dependentVariables)
        {
            string functionBody = GetFunctionBody(function);
            foreach (HLSLVariable variable in hLSLVariables)
            {
                if (functionBody.Contains(variable.Name))
                {
                    dependentVariables.Add(variable);
                }
            }
        }

        private void ComputeFunctionDependency(HLSLFunction function, ref IList<HLSLVariable> dependentVariables, ref IList<HLSLFunction> dependentFunctions)
        {
            ComputeVariableDependency(function, ref dependentVariables);
            string functionBody = GetFunctionBody(function);
            foreach (HLSLFunction func in hLSLFunctions.Except(new[] { function}))
            {
                if (functionBody.Contains(func.Name))
                {
                    dependentFunctions.Add(func);
                    ComputeFunctionDependency(func, ref dependentVariables, ref dependentFunctions);
                }
            }
        }

        private void ComputeDependency(HLSLFunction function)
        {
            string functionBody = GetFunctionBody(function);
            IList<HLSLVariable> dependentVariables = new List<HLSLVariable>();
            IList<HLSLFunction> dependentFunctions = new List<HLSLFunction>();
            ComputeFunctionDependency(function, ref dependentVariables, ref dependentFunctions);
            _functionToDependencyMapping.Add(function, new DependencyMap
            {
                DependentVariables = dependentVariables.Distinct().ToList(),
                DependentFunctions = dependentFunctions.Distinct().ToList()
            });
        }

        private string GetVariable(HLSLVariable var)
        {
            return $"{FromTokenToString(var.Type)} {var.Name};";
        }

        private string GetFunction(HLSLFunction func)
        {
            string retVal = $"{FromTokenToString(func.Type)} {func.Name}(";
            for (int i = 0; i < func.Args.Count - 1; ++i)
            {
                FuncArg arg = func.Args[i];
                if (string.IsNullOrEmpty(arg.Semantic))
                {
                    retVal += $"{FromTokenToString(arg.PassType)} {FromTokenToString(arg.Type)} {arg.Name},";
                }
                else
                {
                    retVal += $"{FromTokenToString(arg.PassType)} {FromTokenToString(arg.Type)} {arg.Name} : {arg.Semantic},";
                }
                retVal += Environment.NewLine;
            }

            FuncArg argLast = func.Args.Last();
            if (string.IsNullOrEmpty(argLast.Semantic))
            {
                retVal += $"{FromTokenToString(argLast.PassType)} {FromTokenToString(argLast.Type)} {argLast.Name}";
            }
            else
            {
                retVal += $"{FromTokenToString(argLast.PassType)} {FromTokenToString(argLast.Type)} {argLast.Name} : {argLast.Semantic}";
            }
            retVal += $"){Environment.NewLine}";
            retVal += GetFunctionBody(func);
            return retVal;
        }

        public static string FromTokenToString(ShaderToken token)
        {
            switch (token)
            {
                case ShaderToken.Uint:
                    return "uint";
                case ShaderToken.Uint2:
                    return "uint2";
                case ShaderToken.Uint3:
                    return "uint3";
                case ShaderToken.Uint4:
                    return "uint4";
                case ShaderToken.Int:
                    return "int";
                case ShaderToken.Int2:
                    return "int2";
                case ShaderToken.Int3:
                    return "int3";
                case ShaderToken.Int4:
                    return "int4";
                case ShaderToken.Float:
                    return "float";
                case ShaderToken.Float2:
                    return "float2";
                case ShaderToken.Float3:
                    return "float3";
                case ShaderToken.Float4:
                    return "float4";
                case ShaderToken.Bool:
                    return "bool";
                case ShaderToken.Void:
                    return "void";
                case ShaderToken.In:
                    return "in";
                case ShaderToken.Out:
                    return "out";
                case ShaderToken.InOut:
                    return "inout";
                case ShaderToken.Texture2D:
                    return "Texture2D";
                case ShaderToken.Texture2DF4:
                    return "Texture2D<float4>";
                case ShaderToken.Texture2DArrayF4:
                    return "Texture2DArray<float4>";
                case ShaderToken.RWTexture2D:
                    return "RWTexture2D";
                case ShaderToken.RWTexture2DArray:
                    return "RWTexture2DArray";
                case ShaderToken.TextureCube:
                    return "TextureCube";
                case ShaderToken.SamplerState:
                    return "SamplerState";
                case ShaderToken.Float21:
                    return "float2x1";
                case ShaderToken.Float22:
                    return "float2x2";
                case ShaderToken.Float23:
                    return "float2x3";
                case ShaderToken.Float24:
                    return "float2x4";
                case ShaderToken.Float31:
                    return "float3x1";
                case ShaderToken.Float32:
                    return "float3x2";
                case ShaderToken.Float33:
                    return "float3x3";
                case ShaderToken.Float34:
                    return "float3x4";
                case ShaderToken.Float41:
                    return "float4x1";
                case ShaderToken.Float42:
                    return "float4x2";
                case ShaderToken.Float43:
                    return "float4x3";
                case ShaderToken.Float44:
                    return "float4x4";
            }

            throw new InvalidOperationException("Undefined token");
        }

        public string GetVariable(string name)
        {
            HLSLVariable var = hLSLVariables.First(v => v.Name == name);
            return GetVariable(var);
        }

        public string GetFunction(string name)
        {
            HLSLFunction func = hLSLFunctions.First(f => f.Name == name);
            return GetFunction(func);
        }

        public IEnumerable<FuncArg> GetFunctionArgs(string functionName)
        {
            HLSLFunction func = hLSLFunctions.First(f => f.Name == functionName);
            return func.Args;
        }

        public IEnumerable<string> GetDependentVariables(string functionName)
        {
            List<string> variables = new List<string>();
            HLSLFunction func = hLSLFunctions.First(f => f.Name == functionName);
            foreach(HLSLVariable variable in _functionToDependencyMapping[func].DependentVariables)
            {
                variables.Add(GetVariable(variable));
            }

            return variables;
        }

        public IEnumerable<string> GetDependentFunctions(string functionName)
        {
            List<string> functions = new List<string>();
            HLSLFunction func = hLSLFunctions.First(f => f.Name == functionName);
            foreach (HLSLFunction function in _functionToDependencyMapping[func].DependentFunctions)
            {
                functions.Add(GetFunction(function));
            }

            return functions;
        }

        private struct DependencyMap
        {
            public HLSLFunction Function { get; set; }
            public List<HLSLVariable> DependentVariables { get; set; }
            public List<HLSLFunction> DependentFunctions { get; set; }
        }
    }
}
