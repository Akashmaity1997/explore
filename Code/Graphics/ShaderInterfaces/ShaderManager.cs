﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.D3DCompiler;
using Assets.Materials;

namespace Graphics.ShaderInterfaces
{
    public class ShaderManager
    {
        private readonly ShaderTextConstructor _shaderTextConstructor;
        private readonly ShaderProductionContext _shaderProductionContext;
        private readonly ShaderConstantMapBuilder _shaderConstantMapBuilder;
        private readonly ShaderConstantMapper _shaderConstantMapper;
        private readonly Dictionary<ShaderRequirement, GeneratedShader> _generatedShaderCache = new Dictionary<ShaderRequirement, GeneratedShader>();
        
        public ShaderManager(D3D11 renderer)
        {
            string completeShaderListing = "";
            using(Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Graphics.Shaders.allshaders.fx"))
            {
                using(StreamReader reader = new StreamReader(stream))
                {
                    completeShaderListing = reader.ReadToEnd();
                }
            }
            string completeShaderText = ShaderBytecode.Preprocess(completeShaderListing, null, new ShaderIncludeHandler());
            completeShaderText = completeShaderText.Replace(" < ", "<");
            completeShaderText = completeShaderText.Replace(" > ", "> ");
            _shaderProductionContext = new ShaderProductionContext();
            _shaderConstantMapBuilder = new ShaderConstantMapBuilder(_shaderProductionContext);
            _shaderConstantMapper = new ShaderConstantMapper(renderer.Device, _shaderConstantMapBuilder);
            _shaderTextConstructor = new ShaderTextConstructor(renderer, _shaderConstantMapper, new ShaderParser(completeShaderText));
        }

        public GeneratedShader GetShaderForDrawable(Drawable drawable)
        {
            ShaderRequirement requirement = new ShaderRequirement
            {
                HasTangents = drawable.MeshData.Tangents.Any(),
                HasUVs = drawable.MeshData.UVs.Any()
            };

            requirement.IsPicking = drawable.IsPicking == true;
            requirement.DoDiffuse = drawable.TextureUsages.Any(t => t.Usage == TextureMapping.Diffuse);
            requirement.DoReflection = drawable.ReflectionMap != null;
            requirement.DoIrradiance = drawable.SpecialOutputMode == OutputColorMode.Irradiance;
            requirement.DoPreFilter = drawable.SpecialOutputMode == OutputColorMode.SpecularIBL;
            requirement.DoBRDFLUT = drawable.SpecialOutputMode == OutputColorMode.BRDFLUT;
            requirement.DoPostProcess = drawable.Material is PostProcessMaterial;
            requirement.DoSelectionEdge = drawable.SpecialOutputMode == OutputColorMode.SelectionEdge;
            requirement.DoMerge = drawable.SpecialOutputMode == OutputColorMode.Merge;

            int hash = requirement.GetHashCode();
            if (_generatedShaderCache.ContainsKey(requirement))
            {
                return _generatedShaderCache[requirement];
            }

            ShaderSequenceGenerator shaderSequenceGenerator = new ShaderSequenceGenerator();
            FunctionSequence seq = shaderSequenceGenerator.GenerateFragmentSequence(requirement);
            GeneratedShader shader = _shaderTextConstructor.GenerateShaderFromFunctionSequence(seq);
            _generatedShaderCache.Add(requirement, shader);
            return shader;
        }

        public void UploadConstants(Drawable drawable, GeneratedShader shader)
        {
            _shaderProductionContext.ProductionObject = drawable;
            _shaderConstantMapper.UploadConstants(shader);
        }
    }
}
