﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharpDX.D3DCompiler;

namespace Graphics.ShaderInterfaces
{
    public class ShaderIncludeHandler : Include
    {
        public IDisposable Shadow 
        { 
            get; 
            set; 
        }

        public void Close(Stream stream)
        {
            stream.Dispose();
        }

        public void Dispose()
        {
            
        }

        public Stream Open(IncludeType type, string fileName, Stream parentStream)
        {
            Stream s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream($"Graphics.Shaders.{fileName}.fx");
            if(s == null)
            {
                s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream($"Graphics.Shaders.{fileName}.hlsl");
            }

            return s;
        }
    }
}
