﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Graphics.ShaderInterfaces
{
    public class ReflectionBasedShaderConstantProducer : IShaderConstantProducer
    {
        private readonly ShaderProductionContext _context;
        private readonly string _propertyName;
        private readonly string[] _propertyChain;

        public ReflectionBasedShaderConstantProducer(ShaderProductionContext context, string propertyName)
        {
            _context = context;
            _propertyName = propertyName;
        }

        public ReflectionBasedShaderConstantProducer(ShaderProductionContext context, string propertyName, string[] propertyChain)
        {
            _context = context;
            _propertyName = propertyName;
            _propertyChain = propertyChain;
        }

        public object ProduceConstant()
        {
            object productionObject = _context.ProductionObject;
            Type t = productionObject.GetType();
            if(_propertyChain != null)
            {
                foreach(string prop in _propertyChain)
                {
                    productionObject = t.GetProperty(prop).GetGetMethod().Invoke(productionObject, null);
                    t = productionObject.GetType();
                }
            }
            PropertyInfo pi = t.GetProperty(_propertyName);
            return pi.GetGetMethod().Invoke(productionObject, null);
        }
    }
}
