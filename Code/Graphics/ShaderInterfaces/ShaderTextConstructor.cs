﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderTextConstructor
    {
        private readonly D3D11 _renderer;
        private readonly ShaderConstantMapper _shaderConstantMapper;
        private readonly ShaderParser _shaderParser;

        public ShaderTextConstructor(D3D11 renderer, ShaderConstantMapper shaderConstantMapper, ShaderParser parser)
        {
            _renderer = renderer;
            _shaderConstantMapper = shaderConstantMapper;
            _shaderParser = parser;
        }

        public GeneratedShader GenerateShaderFromFunctionSequence(FunctionSequence sequence)
        {
            string vertexShaderText = GenerateShaderFromFunctionSequence(sequence, ShaderType.Vertex);
            string pixelShaderText = GenerateShaderFromFunctionSequence(sequence, ShaderType.Pixel);
            string computeShaderText = GenerateShaderFromFunctionSequence(sequence, ShaderType.Compute);
            return GeneratedShader.InitializeFromShaderTexts(_renderer.Device, _shaderConstantMapper, vertexShaderText, pixelShaderText, computeShaderText); 
        }

        private string GenerateShaderFromFunctionSequence(FunctionSequence sequence, ShaderType type)
        {
            string shaderText = "";
            List<ShaderFragment> shaderFragments = type == ShaderType.Vertex ? sequence.VertexShaderFragments : type == ShaderType.Pixel ? sequence.PixelShaderFragments : sequence.ComputeShaderFragments;
            
            if(!shaderFragments.Any())
            {
                return "";
            }

            foreach (ShaderFragment frament in shaderFragments)
            {
                IEnumerable<string> variables = _shaderParser.GetDependentVariables(frament.FuncName);
                foreach(string variableText in variables.Reverse().Distinct())
                {
                    if(shaderText.Contains(variableText))
                    {
                        continue;
                    }

                    shaderText += variableText;
                    shaderText += Environment.NewLine;
                }

                //shaderText += Environment.NewLine;
            }

            shaderText += Environment.NewLine;

            foreach (ShaderFragment frament in shaderFragments)
            {
                IEnumerable<string> functions = _shaderParser.GetDependentFunctions(frament.FuncName);
                foreach (string functionText in functions.Reverse().Distinct())
                {
                    if(shaderText.Contains(functionText))
                    {
                        continue;
                    }

                    shaderText += functionText;
                    shaderText += Environment.NewLine;
                    shaderText += Environment.NewLine;
                }

                shaderText += _shaderParser.GetFunction(frament.FuncName);
                shaderText += Environment.NewLine;
                shaderText += Environment.NewLine;
            }

            if (type == ShaderType.Vertex)
            {
                shaderText += "void VertexShaderMain(";
            }
            else if(type == ShaderType.Pixel)
            {
                shaderText += "void PixelShaderMain(";
            }
            else if(type == ShaderType.Compute)
            {
                shaderText += "numthreads[32, 32, 1]";
                shaderText += Environment.NewLine;
                shaderText += "void ComputeShaderMain(";
            }

            List<FuncArg> funcArgsGlobal = new List<FuncArg>();
            foreach (ShaderFragment frament in shaderFragments)
            {
                List<FuncArg> funcArgs = new List<FuncArg>();
                IEnumerable<FuncArg> args = _shaderParser.GetFunctionArgs(frament.FuncName);
                args = args.Where(a => !string.IsNullOrEmpty(a.Semantic));
                foreach (FuncArg arg in args)
                {
                    if (Enum.TryParse<Semantic>(arg.Semantic, out _))
                    {
                        funcArgs.Add(arg);
                    }
                }
                //funcArgs = funcArgs.OrderBy(a => (int)Enum.Parse(typeof(Semantic), a.Semantic)).ToList();
                foreach(FuncArg arg in funcArgs)
                {
                    if(!funcArgsGlobal.Any(f => f.Semantic == arg.Semantic))
                    {
                        funcArgsGlobal.Add(arg);
                    }
                }
            }
            funcArgsGlobal = funcArgsGlobal.OrderBy(a => (int)Enum.Parse(typeof(Semantic), a.Semantic)).ToList();
            Dictionary<string, string> standardSemanticToNameMapping = new Dictionary<string, string>();
            foreach (FuncArg arg in funcArgsGlobal)
            {
                FuncArg lastArg = funcArgsGlobal.Last();
                if(!standardSemanticToNameMapping.ContainsKey(arg.Semantic))
                {
                    standardSemanticToNameMapping.Add(arg.Semantic, $"{arg.Name}_{arg.Semantic.ToLowerInvariant()}");
                }
                FuncArg newArg = new FuncArg
                {
                    PassType = arg.PassType == ShaderToken.Out ? ShaderToken.Out : ShaderToken.In,
                    Type = arg.Type,
                    Name = standardSemanticToNameMapping[arg.Semantic],
                    Semantic = arg.Semantic
                };

                if (arg.Equals(lastArg))
                {
                    shaderText += $"{ShaderParser.FromTokenToString(newArg.PassType)} {ShaderParser.FromTokenToString(newArg.Type)} {newArg.Name} : {newArg.Semantic})";
                    shaderText += Environment.NewLine;
                }
                else
                {
                    shaderText += $"{ShaderParser.FromTokenToString(newArg.PassType)} {ShaderParser.FromTokenToString(newArg.Type)} {newArg.Name} : {newArg.Semantic},";
                    shaderText += Environment.NewLine;
                }
            }

            shaderText += "{";
            shaderText += Environment.NewLine;

            Dictionary<string, string> nonStandardSemanticToNameMapping = new Dictionary<string, string>();
            foreach (ShaderFragment frament in shaderFragments)
            {
                IEnumerable<FuncArg> args = _shaderParser.GetFunctionArgs(frament.FuncName);

                foreach (FuncArg a in args)
                {
                    if (!funcArgsGlobal.Any(ga => ga.Semantic == a.Semantic))
                    {
                        string nonStdName = $"{a.Name}_{a.Semantic.ToLowerInvariant()}";
                        if (!nonStandardSemanticToNameMapping.ContainsKey(a.Semantic))
                        {
                            nonStandardSemanticToNameMapping.Add(a.Semantic, nonStdName);

                            shaderText += $"{ShaderParser.FromTokenToString(a.Type)} {nonStdName};";
                            shaderText += Environment.NewLine;
                        }
                    }
                }
            }

            shaderText += Environment.NewLine;

            foreach (ShaderFragment frament in shaderFragments)
            {
                shaderText += $"{frament.FuncName}(";
                IEnumerable<FuncArg> args = _shaderParser.GetFunctionArgs(frament.FuncName);
                FuncArg lastArg = args.Last();
                foreach (FuncArg arg in args)
                {
                    string name = "";
                    if (funcArgsGlobal.Any(ga => ga.Semantic == arg.Semantic))
                    {
                        name = standardSemanticToNameMapping[arg.Semantic];
                    }
                    else
                    {
                        name = nonStandardSemanticToNameMapping[arg.Semantic];
                    }


                    if(arg.Equals(lastArg))
                    {
                        shaderText += $"{name});";
                    }
                    else
                    {
                        shaderText += $"{name},";
                    }
                }

                shaderText += Environment.NewLine;
            }

            shaderText += "}";
            return shaderText;
        }
    }
}
