﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderProductionContext
    {
        public Drawable ProductionObject { get; set; }
    }
}
