﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderFragment
    {
        public string FuncName { get; set; }

        public string[] Args { get; set; }

        public override string ToString()
        {
            return FuncName;
        }

        public override bool Equals(object obj)
        {
            if(obj is ShaderFragment other)
            {
                return FuncName == other.FuncName &&
                    Args.SequenceEqual(other.Args);
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = FuncName.GetHashCode() * 17;
            Args.ToList().ForEach(a => hash += a.GetHashCode() * 17);
            return hash;
        }
    }
}
