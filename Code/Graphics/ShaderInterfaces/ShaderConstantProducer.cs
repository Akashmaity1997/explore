﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.ShaderInterfaces
{
    public class ShaderConstantProducer : IShaderConstantProducer
    {
        private readonly ShaderProductionContext _context;
        private readonly Func<Drawable, object> _productionDelegate;
        
        public ShaderConstantProducer(ShaderProductionContext context, Func<Drawable, object> productionDelegate)
        {
            _context = context;
            _productionDelegate = productionDelegate;
        }

        public object ProduceConstant()
        {
            return _productionDelegate(_context.ProductionObject);
        }
    }
}
