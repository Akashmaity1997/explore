﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using Graphics.ShaderInterfaces;

namespace Graphics
{
    public enum OutputColorMode
    {
        // nothing special to do (just the usual stuff...)
        None,
        // color output will be irradiance (used for computing
        // irradiance from environment map)
        Irradiance,
        // color output mode will be mip LOD specular IBL
        // (used for computing prefilter specular IBL)
        SpecularIBL,
        // color output mode will be BRDF LUT
        // (used for computing BRDF for specular IBL)
        BRDFLUT,
        SelectionEdge,
        Merge
    }

    public class Drawable
    {
        public Mesh MeshData { get; set; }

        public RawMatrix Position { get; set; }

        public IWorldDm World { get; set; }

        public ICameraDm Camera { get; set; }

        public OutputColorMode SpecialOutputMode { get; set; } = OutputColorMode.None;

        public MaterialDefinition Material { get; set; } = new MaterialDefinition();

        public bool IsPicking { get; set; }

        public Guid ObjectGuid { get; set; }

        public List<TextureUsage> TextureUsages { get; set; } = new List<TextureUsage>();

        public RenderTarget ReflectionMap { get; set; }

        public RenderTarget IrradianceMap { get; set; }

        public RenderTarget PrefilterMap { get; set; }

        public RenderTarget BRDFLUTMap { get; set; }
    }
}
