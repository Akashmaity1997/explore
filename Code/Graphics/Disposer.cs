﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    public static class Disposer 
    {
        public static void Dispose<T>(ref T obj) where T: class, IDisposable
        {
            obj.Dispose();
            obj = null;
        }
    }
}
