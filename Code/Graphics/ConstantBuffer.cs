﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.Mathematics.Interop;
using System.Runtime.InteropServices;
using Assets;

namespace Graphics
{
    [StructLayout(LayoutKind.Explicit, Size = 208)]
    internal struct WorldCameraParams
    {
        [FieldOffset(0)]
        public RawMatrix World;

        [FieldOffset(64)]
        public RawMatrix View;

        [FieldOffset(128)]
        public RawMatrix Proj;

        [FieldOffset(192)]
        public RawVector4 CameraPosition;
    }

    [StructLayout(LayoutKind.Explicit, Size = 192)]
    internal struct PerObjectParams
    {
        [FieldOffset(0)]
        public RawMatrix Position;
        [FieldOffset(64)]
        public RawMatrix ScaleMatrix;
        [FieldOffset(128)]
        public RawMatrix ScaleInvTranspose;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    internal struct PerObjectDecalCount
    {
        [FieldOffset(0)]
        public int Count;
        [FieldOffset(4)]
        public int padding1;
        [FieldOffset(8)]
        public RawVector2 padding2;
    }

    [StructLayout(LayoutKind.Explicit, Size = 80)]
    internal struct LightParams
    {
        [FieldOffset(0)]
        public RawVector4 Position;
        [FieldOffset(16)]
        public float Brightness;
        [FieldOffset(20)]
        public RawVector3 Colour;
        [FieldOffset(32)]
        public RawVector4 Direction;
        [FieldOffset(48)]
        public float ConeAngle;
        [FieldOffset(52)]
        public RawVector3 padding1;
        [FieldOffset(64)]
        public int IsPoint;
        [FieldOffset(68)]
        public int IsDirectional;
        [FieldOffset(72)]
        public int IsSpot;
        [FieldOffset(76)]
        public int IsCircular;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    internal struct LightCountParams
    {
        [FieldOffset(0)]
        public int Count;
        [FieldOffset(4)]
        public int padding1;
        [FieldOffset(8)]
        public RawVector2 padding2;
    }

    internal class ConstantBuffer<T> : IDisposable
    where T : struct
    {
        private readonly Device _device;
        private readonly SharpDX.Direct3D11.Buffer _buffer;
        private readonly DataStream _dataStream;

        public SharpDX.Direct3D11.Buffer Buffer => _buffer;

        public ConstantBuffer(Device device)
        {
            _device = device;

            // If no specific marshalling is needed, can use
            // SharpDX.Utilities.SizeOf<T>() for better performance.
            int size = SharpDX.Utilities.SizeOf<T>();

            _buffer = new SharpDX.Direct3D11.Buffer(device, new BufferDescription
            {
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.ConstantBuffer,
                SizeInBytes = size,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                StructureByteStride = 0
            });

            _dataStream = new DataStream(size, true, true);
        }

        public void UpdateValue(T value)
        {
            // If no specific marshalling is needed, can use 
            // dataStream.Write(value) for better performance.
            //Marshal.StructureToPtr(value, _dataStream.DataPointer, false);
            _dataStream.Write(value);
            _dataStream.Position = 0;

            var dataBox = new DataBox(_dataStream.DataPointer);
            _device.ImmediateContext.UpdateSubresource(dataBox, _buffer, 0);
        }

        public void Dispose()
        {
            if (_dataStream != null)
                _dataStream.Dispose();
            if (_buffer != null)
                _buffer.Dispose();
        }
    }
}
