﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    public class DrawState
    {
        public bool ClearDepth { get; set; } = true;

        public bool ClearTarget { get; set; } = true;
    }
}
