﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Graphics
{
    public class DrawEventArgs : EventArgs
    {
        public DrawEventArgs()
        {
            TotalTime = TimeSpan.Zero;
        }
        public TimeSpan TotalTime { get; set; }
        public TimeSpan DeltaTime { get; set; }
        public Size RenderSize { get; set; }
        public DXImageSource Target { get; set; }
    }
}
