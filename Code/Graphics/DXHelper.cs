﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using SharpDX.Direct3D9;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.IO;

namespace Graphics
{
    public static class DXHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static int AdapterCount
        {
            get
            {
                if (sAdapterCount == -1)
                    using (var f = new Factory1())
                        sAdapterCount = f.GetAdapterCount();
                return sAdapterCount;
            }
        }

        /// <summary>
        ///  cache it, as the underlying code rely on Exception to find the value!!!
        /// </summary>
        private static int sAdapterCount = -1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dg"></param>
        /// <returns></returns>
        public static IEnumerable<Adapter> GetAdapters(DisposeGroup dg)
        {
            // NOTE: SharpDX 1.3 requires explicit Dispose() of everything
            // hence the DisposeGroup, to enforce it
            using (var f = new Factory1())
            {
                int n = AdapterCount;
                for (int i = 0; i < n; i++)
                {
                    Adapter adapter = f.GetAdapter(i);
                    dg.Add(adapter);
                    yield return adapter;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dg"></param>
        /// <returns></returns>
        public static Adapter GetBestAdapter(DisposeGroup dg)
        {
            FeatureLevel high = FeatureLevel.Level_9_1;
            Adapter ada = null;
            foreach (var item in GetAdapters(dg))
            {
                var level = SharpDX.Direct3D11.Device.GetSupportedFeatureLevel(item);
                if (ada == null || level > high)
                {
                    ada = item;
                    high = level;
                }
            }
            return ada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cFlags"></param>
        /// <param name="minLevel"></param>
        /// <returns></returns>
        public static SharpDX.Direct3D11.Device Create11(
            DeviceCreationFlags cFlags = DeviceCreationFlags.None,
            FeatureLevel minLevel = FeatureLevel.Level_9_1)
        {
            using (var dg = new DisposeGroup())
            {
                var ada = GetBestAdapter(dg);
                if (ada == null)
                    return null;
                var level = SharpDX.Direct3D11.Device.GetSupportedFeatureLevel(ada);
                if (level < minLevel)
                    return null;
                return new SharpDX.Direct3D11.Device(ada, cFlags, level);
            }
        }

        public static SharpDX.Direct3D9.Format ToD3D9(this SharpDX.DXGI.Format dxgiformat)
        {
            switch (dxgiformat)
            {
                case SharpDX.DXGI.Format.R10G10B10A2_UNorm:
                    return SharpDX.Direct3D9.Format.A2B10G10R10;
                case SharpDX.DXGI.Format.B8G8R8A8_UNorm:
                    return SharpDX.Direct3D9.Format.A8R8G8B8;
                case SharpDX.DXGI.Format.R16G16B16A16_Float:
                    return SharpDX.Direct3D9.Format.A16B16G16R16F;

                // not sure those one below will work...

                case SharpDX.DXGI.Format.R32G32B32A32_Float:
                    return SharpDX.Direct3D9.Format.A32B32G32R32F;

                case SharpDX.DXGI.Format.R16G16B16A16_UNorm:
                    return SharpDX.Direct3D9.Format.A16B16G16R16;
                case SharpDX.DXGI.Format.R32G32_Float:
                    return SharpDX.Direct3D9.Format.G32R32F;

                case SharpDX.DXGI.Format.R8G8B8A8_UNorm:
                    return SharpDX.Direct3D9.Format.A8R8G8B8;

                case SharpDX.DXGI.Format.R16G16_UNorm:
                    return SharpDX.Direct3D9.Format.G16R16;

                case SharpDX.DXGI.Format.R16G16_Float:
                    return SharpDX.Direct3D9.Format.G16R16F;
                case SharpDX.DXGI.Format.R32_Float:
                    return SharpDX.Direct3D9.Format.R32F;

                case SharpDX.DXGI.Format.R16_Float:
                    return SharpDX.Direct3D9.Format.R16F;

                case SharpDX.DXGI.Format.A8_UNorm:
                    return SharpDX.Direct3D9.Format.A8;
                case SharpDX.DXGI.Format.R8_UNorm:
                    return SharpDX.Direct3D9.Format.L8;

                case SharpDX.DXGI.Format.BC1_UNorm:
                    return SharpDX.Direct3D9.Format.Dxt1;
                case SharpDX.DXGI.Format.BC2_UNorm:
                    return SharpDX.Direct3D9.Format.Dxt3;
                case SharpDX.DXGI.Format.BC3_UNorm:
                    return SharpDX.Direct3D9.Format.Dxt5;

                default:
                    return SharpDX.Direct3D9.Format.Unknown;
            }
        }

        public static Texture GetSharedD3D9(this DeviceEx device, SharpDX.Direct3D11.Texture2D renderTarget)
        {
            if (renderTarget == null)
                return null;

            if ((renderTarget.Description.OptionFlags & SharpDX.Direct3D11.ResourceOptionFlags.Shared) == 0)
                throw new ArgumentException("Texture must be created with ResourceOptionFlags.Shared");

            SharpDX.Direct3D9.Format format = ToD3D9(renderTarget.Description.Format);
            if (format == SharpDX.Direct3D9.Format.Unknown)
                throw new ArgumentException("Texture format is not compatible with OpenSharedResource");

            using (var resource = renderTarget.QueryInterface<SharpDX.DXGI.Resource>())
            {
                IntPtr handle = resource.SharedHandle;
                if (handle == IntPtr.Zero)
                    throw new ArgumentNullException("Handle");
                return new Texture(device, renderTarget.Description.Width, renderTarget.Description.Height, 1, SharpDX.Direct3D9.Usage.RenderTarget, format, Pool.Default, ref handle);
            }
        }

        public static Texture2D CreateTexture2D(this SharpDX.Direct3D11.Device device,
            int w, int h,
            BindFlags flags = BindFlags.RenderTarget | BindFlags.ShaderResource,
            SharpDX.DXGI.Format format = SharpDX.DXGI.Format.B8G8R8A8_UNorm,
            ResourceOptionFlags options = ResourceOptionFlags.Shared)
        {
            var colordesc = new Texture2DDescription
            {
                BindFlags = flags,
                Format = format,
                Width = w,
                Height = h,
                MipLevels = 1,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                OptionFlags = options,
                CpuAccessFlags = CpuAccessFlags.None,
                ArraySize = 1
            };
            return new Texture2D(device, colordesc);
        }

        public static unsafe WriteableBitmap GetBitmap(this Texture2D tex)
        {
            DataRectangle db;
            using (var copy = tex.GetCopy())
            using (var surface = copy.QueryInterface<SharpDX.DXGI.Surface>())
            {
                // can't destroy the surface now with WARP driver
                DataStream ds;
                db = surface.Map(SharpDX.DXGI.MapFlags.Read, out ds);

                int w = tex.Description.Width;
                int h = tex.Description.Height;
                var wb = new WriteableBitmap(w, h, 96.0, 96.0, PixelFormats.Pbgra32, null);
                wb.Lock();
                try
                {
                    uint* wbb = (uint*)wb.BackBuffer;

                    ds.Position = 0;
                    for (int y = 0; y < h; y++)
                    {
                        ds.Position = y * db.Pitch;
                        for (int x = 0; x < w; x++)
                        {
                            var c = ds.Read<uint>();
                            wbb[y * w + x] = c;
                        }
                    }
                    ds.Dispose();
                }
                finally
                {
                    wb.AddDirtyRect(new Int32Rect(0, 0, w, h));
                    wb.Unlock();
                }
                return wb;
            }
        }

        private static Texture2D GetCopy(this Texture2D tex)
        {
            var teximg = new Texture2D(tex.Device, new Texture2DDescription
            {
                Usage = SharpDX.Direct3D11.ResourceUsage.Staging,
                BindFlags = SharpDX.Direct3D11.BindFlags.None,
                CpuAccessFlags = SharpDX.Direct3D11.CpuAccessFlags.Read,
                Format = tex.Description.Format,
                OptionFlags = SharpDX.Direct3D11.ResourceOptionFlags.None,
                ArraySize = tex.Description.ArraySize,
                Height = tex.Description.Height,
                Width = tex.Description.Width,
                MipLevels = tex.Description.MipLevels,
                SampleDescription = tex.Description.SampleDescription,
            });
            tex.Device.ImmediateContext.CopyResource(tex, teximg);
            return teximg;
        }

        public static BitmapImage ToBitmapImage(this WriteableBitmap wbm)
        {
            BitmapImage bmImage = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbm));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }
    }
}
