﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D9;
using System.Runtime.InteropServices;

namespace Graphics
{
    public class D3D9 : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        public D3D9()
            : this(null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        public D3D9(DeviceEx device)
        {
            if (device != null)
            {
                //context = ???
                throw new NotSupportedException("dunno how to get the context");

                //this.device = device;
                //device.AddReference();
            }
            else
            {
                m_context = new Direct3DEx();

                PresentParameters presentparams = new PresentParameters();
                presentparams.Windowed = true;
                presentparams.SwapEffect = SwapEffect.Discard;
                presentparams.DeviceWindowHandle = GetDesktopWindow();
                presentparams.PresentationInterval = PresentInterval.Default;
                this.m_device = new DeviceEx(m_context, 0, DeviceType.Hardware, IntPtr.Zero, CreateFlags.HardwareVertexProcessing | CreateFlags.Multithreaded | CreateFlags.FpuPreserve, presentparams);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDisposed { get { return m_device == null; } }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = false)]
        static extern IntPtr GetDesktopWindow();

        /// <summary>
        /// 
        /// </summary>
        public DeviceEx Device => m_device;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public void Reset(int w, int h)
        {
            //m_device.GetOrThrow();

            if (w < 1)
                throw new ArgumentOutOfRangeException("w");
            if (h < 1)
                throw new ArgumentOutOfRangeException("h");

            Disposer.Dispose(ref m_renderTarget);
            m_renderTarget = new Texture(this.m_device, w, h, 1, Usage.RenderTarget, Format.A8R8G8B8, Pool.Default);

            // TODO test that...
            using (var surface = m_renderTarget.GetSurfaceLevel(0))
                m_device.SetRenderTarget(0, surface);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <returns></returns>
        protected T Prepared<T>(ref T property)
        {
            //m_device.GetOrThrow();
            if (property == null)
                Reset(1, 1);
            return property;
        }

        /// <summary>
        /// 
        /// </summary>
        public Texture RenderTarget { get { return Prepared(ref m_renderTarget); } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dximage"></param>
        public void SetBackBuffer(DXImageSource dximage) { dximage.SetBackBuffer(RenderTarget); }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Windows.Media.Imaging.WriteableBitmap ToImage() { throw new NotImplementedException(); }


        protected Direct3DEx m_context;
        protected DeviceEx m_device;
        private Texture m_renderTarget;

        public void Dispose()
        {
            Disposer.Dispose(ref m_context);
            Disposer.Dispose(ref m_device);
        }
    }
}
