﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace Graphics
{
    public enum TextureMapping
    {
        Diffuse
    }

    public class TextureUsage
    {
        public TextureMapping Usage { get; set; }

        public ITextureDm Texture { get; set; }
    }
}
