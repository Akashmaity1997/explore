﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    public class DisposeGroup : IDisposable
    {
        private List<IDisposable> _objects;

        public DisposeGroup()
        {
            _objects = new List<IDisposable>();
        }

        public void Dispose()
        {
            foreach (IDisposable obj in new List<IDisposable>(_objects))
                obj.Dispose();

            _objects = null;
        }

        public void Add(IDisposable[] obj)
        {
            _objects.AddRange(obj);
        }

        public void Add(IDisposable obj)
        {
            _objects.Add(obj);
        }
    }
}
