﻿using System.Collections.Generic;
using System.Linq;
using Assets.Interfaces;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

namespace Graphics
{
    public class DirectXMesh
    {
        private Dictionary<ShaderBytecode, InputLayout> _layouts = new Dictionary<ShaderBytecode, InputLayout>();
        private Dictionary<Mesh, DxMesh> _meshes = new Dictionary<Mesh, DxMesh>();

        public void Draw(Device device, Mesh mesh, ShaderBytecode vertexShaderByteCode)
        {
            DxMesh dxMesh;
            if (!_meshes.TryGetValue(mesh, out dxMesh))
            {
                dxMesh = new DxMesh
                {
                    Stride = GetBufferStride(mesh),
                    IndexCount = mesh.Faces.Count * 3,
                    InputElements = GetInputElements(mesh)
                };
                int size = dxMesh.Stride * mesh.Vertices.Count;
                int indexSize = 4 * dxMesh.IndexCount;
                using (DataStream stream = new DataStream(size, false, true))
                {
                    bool writeUVs = mesh.UVs.Any();
                    bool writeTangents = mesh.Tangents.Any();
                    for(int i = 0; i < mesh.Vertices.Count; ++i)
                    {
                        stream.Write(mesh.Vertices[i]);
                        if(writeUVs)
                        {
                            stream.Write(mesh.UVs[i]);
                        }
                        stream.Write(mesh.Normals[i]);
                        if(writeTangents)
                        {
                            stream.Write(mesh.Tangents[i]);
                        }
                    }

                    stream.Position = 0;
                    // Instantiate Vertex buffer from vertex data
                    dxMesh.VertexBuffer = new Buffer(device, stream, new BufferDescription()
                    {
                        BindFlags = BindFlags.VertexBuffer,
                        CpuAccessFlags = CpuAccessFlags.None,
                        OptionFlags = ResourceOptionFlags.None,
                        SizeInBytes = size,
                        Usage = ResourceUsage.Default,
                        StructureByteStride = 0
                    });
                }

                using (DataStream stream = new DataStream(indexSize, false, true))
                {
                    foreach ((int, int, int) face in mesh.Faces)
                    {
                        stream.Write(face.Item1);
                        stream.Write(face.Item2);
                        stream.Write(face.Item3);
                    }

                    stream.Position = 0;

                    // Instantiate Index buffer from face data
                    dxMesh.IndexBuffer = new Buffer(device, stream, new BufferDescription()
                    {
                        BindFlags = BindFlags.IndexBuffer,
                        CpuAccessFlags = CpuAccessFlags.None,
                        OptionFlags = ResourceOptionFlags.None,
                        SizeInBytes = indexSize,
                        Usage = ResourceUsage.Default,
                        StructureByteStride = 0
                    });
                }

                _meshes.Add(mesh, dxMesh);
            }

            device.ImmediateContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(dxMesh.VertexBuffer, dxMesh.Stride, 0));
            device.ImmediateContext.InputAssembler.SetIndexBuffer(dxMesh.IndexBuffer, SharpDX.DXGI.Format.R32_UInt, 0);
            if (_layouts.TryGetValue(vertexShaderByteCode, out InputLayout layout))
            {
                device.ImmediateContext.InputAssembler.InputLayout = layout;
            }
            else
            {
                InputLayout newLayout = new InputLayout(device, vertexShaderByteCode, dxMesh.InputElements);
                _layouts.Add(vertexShaderByteCode, newLayout);
            }

            device.ImmediateContext.DrawIndexed(dxMesh.IndexCount, 0, 0);
        }

        private int GetBufferStride(Mesh mesh)
        {
            int offset = 24;
            if (mesh.UVs.Any())
            {
                offset += 8;
            }
            if (mesh.Tangents.Any())
            {
                offset += 12;
            }
            return offset;
        }

        private InputElement[] GetInputElements(Mesh mesh)
        {
            List<InputElement> elements = new List<InputElement>();
            int offset = 0;
            bool writeUVs = false;
            bool writeTangents = false;
            elements.Add(new InputElement
            {
                SemanticName = "POSITION",
                Format = SharpDX.DXGI.Format.R32G32B32_Float,
                AlignedByteOffset = offset
            });
            offset += 12;
            if (mesh.UVs.Any())
            {
                writeUVs = true;
                elements.Add(new InputElement
                {
                    SemanticName = "TEXCOORD",
                    Format = SharpDX.DXGI.Format.R32G32_Float,
                    AlignedByteOffset = offset
                });
                offset += 8;
            }
            elements.Add(new InputElement
            {
                SemanticName = "NORMAL",
                Format = SharpDX.DXGI.Format.R32G32B32_Float,
                AlignedByteOffset = offset
            });
            offset += 12;
            if (mesh.Tangents.Any())
            {
                writeTangents = true;
                elements.Add(new InputElement
                {
                    SemanticName = "TANGENT",
                    Format = SharpDX.DXGI.Format.R32G32B32_Float,
                    AlignedByteOffset = offset
                });
                offset += 12;
            }

            return elements.ToArray();
        }

        struct DxMesh
        {
            public Buffer VertexBuffer { get; set; }

            public int Stride { get; set; }

            public Buffer IndexBuffer { get; set; }

            public InputElement[] InputElements { get; set; }

            public int IndexCount { get; set; }
        }
    }
}
