﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using SharpDX;
using Assets.Interfaces;
using SharpDX.Direct3D11;
using SharpDX.Mathematics.Interop;
using System.IO;
using System.Windows.Media.Imaging;

namespace Graphics
{
    public interface IDrawPicker
    {
        /// <summary>
        /// This draws a picking pass for each object assigned with
        /// a unique signature colour. The drawing manager draws the texture
        /// into the picking render target (using picking shader). We read the 
        /// pixels from the render target to find which pixel is under the mouse cursor.
        /// Then the colour of the pixel is looked up in the object dictionary
        /// to find out the picked object
        /// </summary>
        /// <param name="viewportSize"></param>
        /// <param name="mousePos"></param>
        void DoPicking(System.Windows.Size viewportSize, System.Windows.Point mousePos);
    }

    /// <summary>
    /// This class generates a pickable render target and depth target and draws
    /// the picking pass (using picking shader) onto it. It tries to find out the
    /// pixel colour under mouse position and looks up in the object dictionary.
    /// </summary>
    public class DrawPicker : IDrawPicker
    {
        private const float STEP_SIZE = 0.01f;

        private readonly IDataProvider _dataProvider;
        private readonly IDrawingManager _drawingManager;
        private readonly PassGenerator _passGenerator;

        public DrawPicker(IDataProvider dataProvider, IDrawingManager drawingManager, PassGenerator passGenerator)
        {
            _dataProvider = dataProvider;
            _drawingManager = drawingManager;
            _passGenerator = passGenerator;
        }

        public RenderTarget RenderTarget { get; private set; }

        public RenderTarget DepthTarget { get; private set; }

        public void DoPicking(System.Windows.Size viewportSize, System.Windows.Point mousePos)
        {
            IDrawable resolvedObject = DoPickingByOffset(viewportSize, mousePos); // might not be the selected object
            _dataProvider.SelectedObject = resolvedObject;
            foreach(IDrawable obj in _dataProvider.CurrentWorld.Objects.Where(ob => ob != resolvedObject))
            {
                obj.IsSelected = false;
            }
        }

        private IDrawable DoPickingByOffset(System.Windows.Size viewportSize, System.Windows.Point mousePos)
        {
            // NUKE THIS ONCE EVERYTHING IS WORKING NORMALLY
            //for (float i = 0.0f; i <= 1.0f; i += STEP_SIZE)
            //{
            //    Point testPoint = new Point((float)((mousePos.X - viewportSize.Width / 2) / (viewportSize.Width / 2)), -(float)((mousePos.Y - viewportSize.Height / 2) / (viewportSize.Height / 2)), i); // projectedTestPoint

            //    foreach (IDrawable obj in _dataProvider.CurrentWorld.Objects)
            //    {
            //        if (_dataProvider.CurrentCamera.Representor == obj) // HACK - we don't friggin pick the current camera representor
            //            continue;

            //        //test boundary
            //        if (obj.BoundaryTest(testPoint))
            //        {
            //            obj.IsSelected = true;
            //            return obj;
            //        }

            //    }
            //}

            CreatePickableRenderTarget();

            IEnumerable<Pass> pickingPasses = _passGenerator.ConstructPickingPasses(RenderTarget, DepthTarget);

            int i = 0;
            foreach (Pass pass in pickingPasses)
            {
                int j = 0;
                for (; j < pass.Drawables.Count; ++j)
                {

                    Drawable d = pass.Drawables[j];
                    d.IsPicking = true;
                    d.Material.Diffuse = VectorExtensions.PickingColor(i + j);
                }
                i = j;
                _drawingManager.Draw(pass);
            }
            _drawingManager.Renderer.Device.ImmediateContext.Flush();

            return ReadPickingTexture(mousePos, pickingPasses);
        }

        private IDrawable ReadPickingTexture(System.Windows.Point point, IEnumerable<Pass> pickingPasses)
        {
            D3D11 renderer = _drawingManager.Renderer;
            Device device = renderer.Device;

            Texture2D resource = RenderTarget.GetResource();
            Texture2DDescription desc = resource.Description;
            desc.BindFlags = BindFlags.None;
            desc.CpuAccessFlags = CpuAccessFlags.Read;
            desc.Usage = ResourceUsage.Staging;
            Texture2D readSurface = new Texture2D(device, desc);
            device.ImmediateContext.CopyResource(resource, readSurface);

            IDrawable selectedObject = null;
            try
            {
                DataStream ds = null;
                DataBox data = device.ImmediateContext.MapSubresource(readSurface, 0, 0, MapMode.Read, MapFlags.None, out ds);
                ds.Seek((data.RowPitch*(long)point.Y) + (4 * (long)point.X), SeekOrigin.Begin);
                var c = ds.Read<uint>();
                byte[] pix = BitConverter.GetBytes(c);
                float B = pix[0] / 255.0f;
                float G = pix[1] / 255.0f;
                float R = pix[2] / 255.0f;
                float A = pix[3] / 255.0f;

                float epsilon = 0.01f;
                IBaseDm selected = null;
                foreach (Pass pass in pickingPasses)
                {
                    foreach (Drawable d in pass.Drawables)
                    {
                        RawColor3 colour = d.Material.Diffuse;
                        if ((Math.Abs(colour.R - R) < epsilon) && (Math.Abs(colour.G - G) < epsilon) && (Math.Abs(colour.B - B) < epsilon))
                        {
                            selected = _dataProvider.FindByGuid(d.ObjectGuid);
                            if (selected == null)
                            {
                                foreach (IDrawingExtension extension in _passGenerator.DrawingExtensions)
                                {
                                    if (extension.FindByGuid(d.ObjectGuid))
                                    {
                                        selected = _dataProvider.SelectedDm;
                                    }
                                }
                            }
                            break;
                            //selectedObject = _dataProvider.PickableColourToObjectMapping[new Point()];
                            //selectedObject.IsSelected = true;
                        }
                    }
                }

                _dataProvider.SelectedDm = selected;

                ds.Dispose();
            }
            finally
            {
                device.ImmediateContext.UnmapSubresource(readSurface, 0);
                readSurface.Dispose();
                RenderTarget.Dispose();
                DepthTarget.Dispose();
            }

            return selectedObject;
        }

        /// <summary>
        /// TODO later on maintain a separate rendertarget class and maintain
        /// flexible updation of targettexture
        /// </summary>
        private void CreatePickableRenderTarget()
        {
            D3D11 renderer = _drawingManager.Renderer;

            RenderTarget = new RenderTarget(renderer.RenderTarget.Width, renderer.RenderTarget.Height, SharpDX.DXGI.Format.B8G8R8A8_UNorm, RenderTargetUsage.RenderTarget);
            RenderTarget.Initialize(renderer.Device);
            DepthTarget = new RenderTarget(renderer.DepthTarget.Width, renderer.DepthTarget.Height, SharpDX.DXGI.Format.D24_UNorm_S8_UInt, RenderTargetUsage.DepthTarget);
            DepthTarget.Initialize(renderer.Device);
        }
    }
}
