﻿float InputGamma;
float OutputGamma;

Texture2D PostProcessMap;
SamplerState PostProcessMap_ss;

float2 TexelSize;

Texture2D MergeMap;
SamplerState MergeMap_ss;

void postProcess(inout float4 color : r_Color,
	float2 tex : OUTTEXCOORD)
{
	float3 col = PostProcessMap.Sample(PostProcessMap_ss, tex).rgb;
	color.xyz = pow(col, InputGamma / OutputGamma) * color.xyz;
}

void selectionEdge(inout float4 color : r_Color,
	float2 tex : OUTTEXCOORD)
{
	color = float4(0, 0, 0, 0);

	float4 baseCol = PostProcessMap.Sample(PostProcessMap_ss, tex);

	float4 col1 = PostProcessMap.Sample(PostProcessMap_ss, tex + float2(TexelSize.x, 0));
	col1 -= PostProcessMap.Sample(PostProcessMap_ss, tex - float2(TexelSize.x, 0));

	float4 col2 = PostProcessMap.Sample(PostProcessMap_ss, tex + float2(0, TexelSize.y));
	col2 -= PostProcessMap.Sample(PostProcessMap_ss, tex - float2(0, TexelSize.y));

	float4 col = abs(col1) + abs(col2) - 2 * baseCol;
	if ((col.x + col.y + col.z + col.w) > 0.1)
	{
		color.r = 1;
		color.a = 1;
	}
}

void mergeTargets(inout float4 color : r_Color,
	float2 tex : OUTTEXCOORD)
{
	color = PostProcessMap.Sample(PostProcessMap_ss, tex);
	float4 col = MergeMap.Sample(MergeMap_ss, tex);
	if ((col.x + col.y + col.z + col.w) > 0.1)
	{
		color = col;
	}
}