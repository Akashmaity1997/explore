﻿Texture2D DiffuseMap;
SamplerState DiffuseMap_ss;

void readDiffuseMap(inout float4 color : r_Color,
	float2 tex : OUTTEXCOORD)
{
	color.xyz = DiffuseMap.Sample(DiffuseMap_ss, tex).xyz * color.xyz;
	//color.xyz = pow(color.xyz, .454);
}