﻿TextureCube ReflectionMap;
SamplerState ReflectionMap_ss;

TextureCube IrradianceMap;
SamplerState IrradianceMap_ss;

TextureCube PrefilterMap;
SamplerState PrefilterMap_ss;

Texture2D BRDFLUTMap;
SamplerState BRDFLUTMap_ss;

void computeIrradiance(inout float4 color : r_Color,
	float4 pos : OUTWORLDPOSITION,
	float3 normal : OUTWORLDNORMAL,
	float3 tangent : OUTWORLDTANGENT)
{
	float3 eyeVec = normalize(EyePos - pos.xyz);

	float3 irradiance = float3(0, 0, 0);
	float3 binormal = normalize(cross(normal, tangent));

	float3x3 ts = float3x3(tangent.x, tangent.y, tangent.z,
		binormal.x, binormal.y, binormal.z,
		normal.x, normal.y, normal.z);

	float sampleDelta = 0.025;
	float nrSamples = 0.0;
	for (float phi = 0.0; phi < 6.28; phi += sampleDelta)
	{
		for (float theta = 0.0; theta < 1.57; theta += sampleDelta)
		{
			// spherical to cartesian (in tangent space)
			float3 tangentSample = float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));

			// tangent space to world
			float3 sampleVec = normalize(mul(tangentSample, ts));
			/*float3 halfVector = normalize(eyeVec + sampleVec);
			float D = DGGX(normal, halfVector, Roughness);
			float pdf = (D * dot(normal, halfVector) / (4.0 * dot(halfVector, eyeVec))) + 0.0001;
			float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);
			float mipLevel = Roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);*/

			float3 col = ReflectionMap.Sample(ReflectionMap_ss, sampleVec).rgb * cos(theta) * sin(theta);
			// TODO - Specular IBL and cook torrance
			//col = ((0.2 * color.xyz / 3.14) + 0.8 * CookTorrance(normal, eyeVec, sampleVec, normalize(eyeVec + sampleVec), Metalness, Roughness, Fresnel, color.xyz, true)) * col;
			irradiance += col;
			nrSamples++;
		}
	}
	irradiance *= 3.14 / nrSamples;
	color.xyz = irradiance;
}

float3 CookTorrance(float3 normal,
	float3 eyeVector,
	float3 lightVector,
	float3 halfVector,
	float metalness,
	float roughness,
	float3 fresnel,
	float3 color,
	bool isIBL)
{
	float D = DGGX(normal, halfVector, roughness);
	float k = 0;
	if (isIBL)
	{
		k = pow(roughness, 2) / 2;
	}
	else
	{
		k = pow(roughness + 1, 2) / 8;
	}
	float G = GeometrySmith(normal, eyeVector, lightVector, k);
	float3 F0 = lerp(fresnel, color, metalness);
	float3 F = fresnelSchlick(dot(halfVector, eyeVector), F0);
	return (D * F * G) / (4 * dot(eyeVector, normal) * dot(lightVector, normal));
}

float DGGX(float3 normal,
	float3 halfVector,
	float roughness)
{
	float a2 = roughness * roughness;
	float NdotH = max(dot(normal, halfVector), 0.0);
	float NdotH2 = pow(NdotH, 2);

	float nom = a2;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	denom = 3.14 * denom * denom;

	return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float k)
{
	float nom = NdotV;
	float denom = NdotV * (1.0 - k) + k;

	return nom / denom;
}

float GeometrySmith(float3 normal, float3 eyeVector, float3 lightVector, float k)
{
	float NdotV = max(dot(normal, eyeVector), 0.0);
	float NdotL = max(dot(normal, lightVector), 0.0);
	float ggx1 = GeometrySchlickGGX(NdotV, k);
	float ggx2 = GeometrySchlickGGX(NdotL, k);

	return ggx1 * ggx2;
}

float3 fresnelSchlick(float cosTheta, float3 F0)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float3 fresnelSchlickRoughness(float cosTheta, float3 F0, float roughness)
{
	return F0 + (max(float3(1.0 - roughness, 1.0 - roughness, 1.0 - roughness), F0) - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

float2 Hammersley(uint i, uint N)
{
	return float2(float(i) / float(N), RadicalInverse_VdC(i));
}

float3 ImportanceSampleGGX(float2 Xi, float3x3 ts, float roughness)
{
	float a = roughness * roughness;

	float phi = 2.0 * 3.14 * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a * a - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	// from spherical coordinates to cartesian coordinates
	float3 halfVector;
	halfVector.x = cos(phi) * sinTheta;
	halfVector.y = sin(phi) * sinTheta;
	halfVector.z = cosTheta;

	// from tangent-space vector to world-space sample vector
	float3 sampleVec = mul(halfVector, ts);
	return normalize(sampleVec);
}

void computePrefilter(inout float4 color : r_Color,
	float3 normal : OUTWORLDNORMAL,
	float3 tangent : OUTWORLDTANGENT)
{
	float3 binormal = normalize(cross(normal, tangent));
	// we consider direct peerpendicular reflection (optimization)
	float3 eyeVec = normal;

	float3x3 ts = float3x3(tangent.x, tangent.y, tangent.z,
		binormal.x, binormal.y, binormal.z,
		normal.x, normal.y, normal.z);

	const uint SAMPLE_COUNT = 1024u;
	float totalWeight = 0.0;
	float3 prefilteredColor = float3(0, 0, 0);
	float resolution = 256.0; // resolution of source cubemap (per face)
	float saTexel = 4.0 * 3.14 / (6.0 * resolution * resolution);
	for (uint i = 0u; i < SAMPLE_COUNT; ++i)
	{
		float2 Xi = Hammersley(i, SAMPLE_COUNT);
		float3 halfVector = ImportanceSampleGGX(Xi, ts, Roughness);
		float3 lightVector = normalize(reflect(-normal, halfVector));

		/*float D = DGGX(normal, halfVector, Roughness);
		float pdf = (D * dot(normal, halfVector) / (4.0 * dot(halfVector, eyeVec))) + 0.0001;
		float saSample = 1.0 / (float(SAMPLE_COUNT) * pdf + 0.0001);
		float mipLevel = Roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);*/

		float NdotL = max(dot(normal, lightVector), 0.0);
		if (NdotL > 0.0)
		{
			prefilteredColor += ReflectionMap.Sample(ReflectionMap_ss, lightVector).rgb * NdotL;
			totalWeight += NdotL;
		}
	}
	prefilteredColor = prefilteredColor / totalWeight;
	color.xyz = prefilteredColor;
}

void computeBRDF(inout float4 color : r_Color,
	float2 tex : OUTTEXCOORD)
{
	float NdotV = tex.x;
	float roughness = tex.y;
	float3 eyeVec;
	eyeVec.x = sqrt(1.0 - NdotV * NdotV);
	eyeVec.y = 0.0;
	eyeVec.z = NdotV;

	float A = 0.0;
	float B = 0.0;

	float3 normal = float3(0.0, 0.0, 1.0);
	float3x3 ts = float3x3(1, 0, 0,
		0, 1, 0,
		normal.x, normal.y, normal.z);

	const uint SAMPLE_COUNT = 1024u;
	for (uint i = 0u; i < SAMPLE_COUNT; ++i)
	{
		float2 Xi = Hammersley(i, SAMPLE_COUNT);
		float3 halfVector = ImportanceSampleGGX(Xi, ts, roughness);
		float3 lightVect = normalize(reflect(-eyeVec, halfVector));

		float NdotL = max(lightVect.z, 0.0);
		float NdotH = max(halfVector.z, 0.0);
		float VdotH = max(dot(eyeVec, halfVector), 0.0);

		if (NdotL > 0.0)
		{
			float k = roughness * roughness / 2;
			float G = GeometrySmith(normal, eyeVec, lightVect, k);
			float G_Vis = (G * VdotH) / (NdotH * NdotV);
			float Fc = pow(1.0 - VdotH, 5.0);

			A += (1.0 - Fc) * G_Vis;
			B += Fc * G_Vis;
		}
	}
	A /= float(SAMPLE_COUNT);
	B /= float(SAMPLE_COUNT);
	color.xyz =  float3(A, B, 0);
}

void lightingIBL(inout float4 color : r_Color,
	float4 pos : OUTWORLDPOSITION,
	float3 normal : OUTWORLDNORMAL)
{
	float3 eyeVec = EyePos - pos.xyz;
	eyeVec = normalize(eyeVec);
	float3 refl = normalize(reflect(-eyeVec, normal));

	float3 irradiance = IrradianceMap.Sample(IrradianceMap_ss, normal).xyz;
	float3 F0 = lerp(Fresnel, color.xyz, Metalness);
	float3 kS = fresnelSchlickRoughness(max(dot(normal, eyeVec), 0.0), F0, Roughness);
	float3 kD = float3(1.0, 1.0, 1.0) - kS;
	kD *= 1.0 - Metalness;
	float3 diffuse = irradiance * color.xyz;
	float3 ambient = kD * diffuse;

	float mipLod = Roughness * 4;
	float3 prefilter = PrefilterMap.SampleLevel(PrefilterMap_ss, refl, mipLod).xyz;
	float2 brdf = BRDFLUTMap.Sample(BRDFLUTMap_ss, float2(max(dot(normal, eyeVec), 0), Roughness)).xy;
	color.xyz = ambient + prefilter * (kS * brdf.x + brdf.y);
}