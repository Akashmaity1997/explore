﻿float4x4 GWorld;
float4x4 GView;
float4x4 GProj;
float3 EyePos;

float4x4 Position;

float3 Diffuse;
float Metalness;
float Roughness;
float3 Fresnel;

void vsOpen(float3 pos : POSITION,
	float3 normal : NORMAL,
	out float4 outPos : SV_POSITION,
	out float3 outNormal : OUTNORMAL)
{
	outPos = float4(pos.xyz, 1);
	outNormal = normal;
}

void calculateTangent(float3 normal : OUTNORMAL,
	float2 outTex : OUTTEXCOORD,
	float2 outTex2 : TEXCOORD2,
	float2 outTex3 : TEXCOORD3,
	float3 pos : SV_POSITION,
	float3 pos2 : POSITION2,
	float3 pos3 : POSITION3,
	out float3 tangent : OUTTANGENT,
	out float3 bitangent : BITANGENT)
{
	float s1 = outTex2.x - outTex.x;
	float s2 = outTex3.x - outTex.x;
	float t1 = outTex2.y - outTex.y;
	float t2 = outTex3.y - outTex.y;

	float r = 1.0f / (s1 * t2 - s2 * t1);

	float3 bNon = float3((s1 * (pos3.x - pos.x) - s2 * (pos2.x - pos.x)) * r, (s1 * (pos3.y - pos.y) - s2 * (pos2.y - pos.y)) * r, (s1 * (pos3.z - pos.z) - s2 * (pos2.z - pos.z)) * r);
	bitangent = normalize(bNon);
	tangent = cross(bitangent, normal);
}

void passTexCoord(float2 tex : TEXCOORD,
                  out float2 outTex : OUTTEXCOORD)
{
	outTex = tex;
}

void passTangent(float3 tangent : TANGENT,
	out float3 outTangent : OUTTANGENT)
{
	outTangent = tangent;
}

void passColor(inout float4 col : COLOR,
	out float4 outCol : OUTCOLOR)
{
	//some hardcoded white ambient light for now
	float4 ambient = float4(0.2, 0.2, 0.2, 0);
	outCol = col + ambient;
}

void toWorld(inout float4 pos : SV_POSITION,
	float3 normal : OUTNORMAL,
	out float4 worldPos : OUTWORLDPOSITION,
	out float3 worldNormal : OUTWORLDNORMAL)
{
	pos = mul(pos, Position);
	//pos = mul(pos, position);
	pos = mul(pos, GWorld); // vertex in world space
	worldPos = pos;
	float4 nor = float4(normal.xyz, 0);
	nor = mul(nor, Position);
	nor = mul(nor, GWorld);
	worldNormal = nor.xyz;
}

void toWorldTangent(float3 tangent : OUTTANGENT,
	out float3 worldTangent : OUTWORLDTANGENT)
{
	float4 tan = float4(tangent.xyz, 0);
	tan = mul(tan, Position);
	tan = mul(tan, GWorld);
	worldTangent = tan.xyz;
}

void toProj(inout float4 pos : SV_POSITION)
{
	pos = mul(pos, mul(GView, GProj)); // perspective divide is done by GPU
}

void vsClose(inout float4 pos : SV_POSITION)
{

}

void psOpen(inout float4 pos : SV_POSITION,
	inout float4 worldPos : OUTWORLDPOSITION,
	inout float3 normal : OUTNORMAL,
	inout float3 worldNormal : OUTWORLDNORMAL)
{
	normal = normalize(normal);
	worldNormal = normalize(worldNormal);
}

void inputTex(inout float2 tex : OUTTEXCOORD)
{

}

void inputTangents(inout float3 tangent : OUTTANGENT,
	inout float3 worldTangent : OUTWORLDTANGENT)
{
	tangent = normalize(tangent);
	worldTangent = normalize(worldTangent);
}

void inputColor(float4 col : OUTCOLOR,
	out float4 pixelCol : r_Color)
{
	pixelCol = col;
}

void readColor(out float4 color : r_Color)
{
	color = float4(Diffuse.xyz, 1);
}

void gammaCorrect(inout float4 color : r_Color)
{
	color = pow(color, 0.454);
}

void psClose(float4 col : r_Color,
	out float4 writeCol : SV_TARGET)
{
	writeCol = col;
}