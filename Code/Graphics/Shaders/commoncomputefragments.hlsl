﻿Texture2D<float4> InputTex;
Texture2DArray<float4> InputTexArray;
RWTexture2D OutputTex;
RWTexture2DArray OutputTexArray;

uint3 Access;

void readFromTex(uint3 threadID : SV_DispatchThreadID,
	out float4 color : r_Color)
{
	color = InputTex[threadID.xy];
}

void readFromTexArray(uint3 threadID : SV_DispatchThreadID,
	out float4 color : r_Color)
{
	color = InputTexArray[threadID];
}

void writeToTex(uint3 threadID : SV_DispatchThreadID,
	float4 color : r_Color)
{
	OutputTex[threadID.xy] = color;
}

void writeToTexExplicit(float4 color : r_Color)
{
	OutputTex[Access.xy] = color;
}

void writeToTexArray(uint3 threadID : SV_DispatchThreadID,
	float4 color : r_Color)
{
	OutputTexArray[threadID] = color;
}

void writeToTexArrayExplicit(float4 color : r_Color)
{
	OutputTexArray[Access] = color;
}