﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;

namespace DataModel
{
    public class TextureDm : BaseDm, ITextureDm
    {
        public Image Asset { get; set; }
    }
}
