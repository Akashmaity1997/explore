﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace DataModel
{
    public class EnvironmentDm : APositionableDm, IEnvironmentDm
    {
        private float _radius = 5;
        public float Radius 
        { 
            get => _radius; 
            set
            {
                _radius = value;
                RaiseThisPropertyChanged();
            } 
        }

        private ITextureDm _texture;
        public ITextureDm Texture 
        {
            get => _texture; 
            set
            {
                _texture = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
