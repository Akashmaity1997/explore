﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;
using Assets;
using Assets.Interfaces;

namespace DataModel
{
    public class CameraDm : APositionableDm, ICameraDm
    {
        private readonly float _nearPlane, _farPlane;

        public CameraDm(Vector position, Vector target, float aspectRatio, float fov, float nearPlane, float farPlane)
        {
            _lookAt = target;
            _aspectRatio = aspectRatio;
            _fov = fov;
            _nearPlane = nearPlane;
            _farPlane = farPlane;

            Vector zFinal = (target - position).Normalize();
            Vector zShadow = new Vector(zFinal.X, 0, zFinal.Z).Normalize();

            Vector axis = zFinal.Cross(zShadow).Normalize();

            Vector xFinal = axis.Flip();
            Vector yFinal = zFinal.Cross(xFinal);

            //float val = zFinal.Dot(zShadow);
            //float theta = (float)(Math.Acos(val) * 180.0f / Math.PI);
            //Vector yFinal = Vector.YAxis.Rotate(-theta, axis);
            //Vector xFinal = yFinal.Cross(zFinal);

            _up = yFinal;
            Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, new Point(position.X, position.Y, position.Z));

            UpdateProjectionMatrix();
        }

        private float _aspectRatio;
        public float AspectRatio 
        { 
            get => _aspectRatio; 
            set
            {
                _aspectRatio = value;
                UpdateProjectionMatrix();
                RaiseThisPropertyChanged();
            }
        }

        private RawMatrix _projectionMatrix;
        public RawMatrix ProjectionMatrix
        {
            get => _projectionMatrix;
            private set
            {
                _projectionMatrix = value;
                RaiseThisPropertyChanged();
            }
        }

        public RawMatrix ViewMatrix => Position.Inverse();

        private Vector _lookAt;
        public Vector LookAt
        {
            get => _lookAt;
            set
            {
                _lookAt = value;
                CalculateCameraAttitude();
                RaiseThisPropertyChanged();
            }
        }

        private Vector _up;
        public Vector Up
        {
            get => _up;
            set
            {
                _up = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _fov;
        public float FOV
        {
            get => _fov;
            set
            {
                _fov = value;
                UpdateProjectionMatrix();
                RaiseThisPropertyChanged();
            }
        }

        private void CalculateCameraAttitude()
        {
            Vector position = new Vector(Position.M41, Position.M42, Position.M43);
            Vector zFinal = (_lookAt - position).Normalize();
            Vector zShadow = new Vector(zFinal.X, 0, zFinal.Z).Normalize();

            Vector axis = zFinal.Cross(zShadow).Normalize();

            Vector xFinal = axis.Flip();
            Vector yFinal = zFinal.Cross(xFinal);

            _up = yFinal;
            //_cameraCube.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, new Point(position.X, position.Y, position.Z));
        }

        private void UpdateProjectionMatrix()
        {
            double rad = _fov * Math.PI / 180.0f;
            ProjectionMatrix = new RawMatrix
            {
                M11 = (float)(1 / (_aspectRatio * Math.Tan(rad / 2))),
                M12 = 0,
                M13 = 0,
                M14 = 0,
                M21 = 0,
                M22 = (float)(1 / Math.Tan(rad / 2)),
                M23 = 0,
                M24 = 0,
                M31 = 0,
                M32 = 0,
                M33 = _farPlane / (_farPlane - _nearPlane),
                M34 = 1,
                M41 = 0,
                M42 = 0,
                M43 = -_nearPlane * _farPlane / (_farPlane - _nearPlane),
                M44 = 0
            };
        }
    }
}
