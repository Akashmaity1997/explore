﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace DataModel
{
    public class MaterialDm : BaseDm, IMaterialDm
    {
        private MaterialDefinition _definition;
        public MaterialDefinition Definition 
        { 
            get => _definition; 
            set
            {
                _definition = value;
                RaiseThisPropertyChanged();
            } 
        }

        private ITextureDm _diffuseTexture;
        public ITextureDm DiffuseTexture 
        { 
            get => _diffuseTexture; 
            set
            {
                _diffuseTexture = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
