﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;

namespace DataModel
{
    public static class DmExtensions
    {
        public static RawMatrix GetWorldPosition(this IPositionableDm positionable)
        {
            if(positionable is IShapeDm shape)
            {
                return shape.PivotOffset.Multiply(shape.Position).Multiply(GetWorldPosition(shape.Parent as IPositionableDm));
            }
            else if(positionable is IModelDm model)
            {
                return model.PivotOffset.Multiply(model.Position);
            }
            else if(positionable is ICameraDm camera)
            {
                return camera.Position;
            }
            else if(positionable is IWorldDm world)
            {
                return world.Position;
            }
            else if(positionable is IEnvironmentDm environement)
            {
                return MatrixHelpers.ScaleMatrix(environement.Radius).Multiply(environement.Position);
            }

            return positionable.Position;
        }

        public static void CameraMouseRotation(this ICameraDm camera, Vector delta)
        {
            Vector targetToPosition = new Vector(camera.Position.M41, camera.Position.M42, camera.Position.M43) - camera.LookAt;
            float xShift = (float)(delta.X / 100.0f);
            float yShift = (float)(delta.Y / 100.0f);
            float thetax = (float)((xShift / targetToPosition.Magnitude()) * (180.0f / Math.PI));
            float thetay = (float)((yShift / targetToPosition.Magnitude()) * (180.0f / Math.PI));
            Vector rotatedTargetToPosition = targetToPosition.Rotate(thetax, camera.Up);

            Point pos = new Point(camera.LookAt.X + rotatedTargetToPosition.X, camera.LookAt.Y + rotatedTargetToPosition.Y, camera.LookAt.Z + rotatedTargetToPosition.Z);

            Vector zFinal = rotatedTargetToPosition.Flip().Normalize();
            Vector yFinal = camera.Up;
            Vector xFinal = yFinal.Cross(zFinal);

            camera.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, pos);

            Vector rotatedTargetToPosition2 = rotatedTargetToPosition.Rotate(thetay, xFinal);
            pos = new Point(camera.LookAt.X + rotatedTargetToPosition2.X, camera.LookAt.Y + rotatedTargetToPosition2.Y, camera.LookAt.Z + rotatedTargetToPosition2.Z);
            zFinal = rotatedTargetToPosition2.Flip().Normalize();
            camera.Up = yFinal = zFinal.Cross(xFinal);

            camera.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, pos);

        }
    }
}
