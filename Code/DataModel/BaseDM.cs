﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;

namespace DataModel
{
    public class BaseDm : PropertyAware, IBaseDm
    {
        
        public BaseDm()
        {
            
        }

        private string _name;
        public virtual string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaiseThisPropertyChanged();
            }
        }

        public Guid Guid { get; set; } = Guid.NewGuid();

        public override string ToString()
        {
            return $"{Name} : {Guid.ToString()}";
        }

        //public override VertexInterface[] Draw()
        //{
        //    throw new NotImplementedException();
        //}

        //protected override void CreateGeometryData()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
