﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using System.IO;
using System.Threading;
using System.Collections.ObjectModel;

namespace DataModel
{
    // TODO : Make MeshData class and consolidate mesh data like tangent space, texture, geomtery data into
    //        neat separate class
    public class ModelDm : APositionableDm, IModelDm
    {
        public ModelDm()
        {
            
        }

        public IParentableDm Parent { get; set; }

        public ObservableCollection<IShapeDm> Shapes { get; } = new ObservableCollection<IShapeDm>();
    }
}
