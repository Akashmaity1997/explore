﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;

namespace DataModel
{
    public abstract class APositionableDm : BaseDm, IPositionableDm
    {
        private RawMatrix _position = MatrixHelpers.Identity;
        public RawMatrix Position 
        { 
            get => _position;
            set 
            { 
                _position = value;
                RaiseThisPropertyChanged();
            }
        }

        private RawMatrix _pivotOffset = MatrixHelpers.Identity;
        public RawMatrix PivotOffset 
        { 
            get => _pivotOffset; 
            set
            {
                _pivotOffset = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
