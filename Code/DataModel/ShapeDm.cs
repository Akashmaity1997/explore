﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using System.Collections.ObjectModel;
using Assets.Interfaces;

namespace DataModel
{
    public class ShapeDm : APositionableDm, IShapeDm
    {
        public ShapeDm(Mesh mesh)
        {
            Mesh = mesh;
        }

        public Mesh Mesh { get; }

        public IParentableDm Parent { get; set; }

        public IEnumerable<IShapeDm> GetSelfAndDescendants()
        {
            yield return this;
            IModelDm modelDm = null;
            IParentableDm parentableDm = this;
            while(modelDm == null)
            {
                modelDm = parentableDm.Parent as IModelDm;
                parentableDm = parentableDm.Parent;
            }

            foreach(IShapeDm shape in modelDm.Shapes)
            {
                if(shape.Parent == this)
                {
                    yield return shape;
                }
            }
        }
    }
}
