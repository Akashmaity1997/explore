﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Assets.Interfaces;

namespace DataModel
{
    public class WorldDm : APositionableDm, IWorldDm
    {
        public ObservableCollection<IModelDm> Models { get; set; } = new ObservableCollection<IModelDm>();
    }
}
