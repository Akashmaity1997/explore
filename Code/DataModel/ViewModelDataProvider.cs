﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using Explore.UI.ViewModels;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Assets.Materials;
using System.IO;
using System.Windows;

namespace DataModel
{
    public interface IViewModelDataProvider
    {
    }

    public class ViewModelDataProvider : PropertyAware, IViewModelDataProvider
    {
        private readonly IDataProvider _dataProvider;
        private readonly MainWindowViewModel _mainWindowViewModel;

        public ViewModelDataProvider(IDataProvider dataProvider, MainWindowViewModel mainWindowViewModel)
        {
            _dataProvider = dataProvider;
            _dataProvider.Objects.CollectionChanged += ObjectsCollectionChanged;
            _dataProvider.Lights.CollectionChanged += LightsCollectionChanged;
            _dataProvider.Cameras.CollectionChanged += CamerasCollectionChanged;
            _dataProvider.Decals.CollectionChanged += MaterialsCollectionChanged;
            _dataProvider.PropertyChanged += DataProviderPropertyChanged;
            _mainWindowViewModel = mainWindowViewModel;
            _mainWindowViewModel.ModelPaletteViewModel.PropertyChanged += PalettePropertyChanged;
            _mainWindowViewModel.EnvironmentPaletteViewModel.PropertyChanged += PalettePropertyChanged;
            _mainWindowViewModel.CameraPaletteViewModel.PropertyChanged += PalettePropertyChanged;
        }

        private void ObjectsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((NotifyCollectionChangedEventHandler)ObjectsCollectionChanged, sender, e);
                return;
            }

            if (e.NewItems != null)
            {
                foreach (APresentable item in e.NewItems)
                {
                    if (item.IsSpecial || item.IsManipulator)
                        return;

                    CreateViewModel(item);
                }
            }

            if (e.OldItems != null)
            {
                foreach (APresentable item in e.OldItems)
                {
                    if (item.IsSpecial || item.IsManipulator)
                        return;

                    DeleteViewModel(item);
                }
            }
        }

        private void LightsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((NotifyCollectionChangedEventHandler)LightsCollectionChanged, sender, e);
                return;
            }

            if (e.NewItems != null)
            {
                foreach (Light item in e.NewItems)
                {

                    CreateViewModel(item);
                }
            }

            if (e.OldItems != null)
            {
                foreach (Light item in e.OldItems)
                {

                    DeleteViewModel(item);
                }
            }
        }

        private void CreateViewModel (APresentable obj)
        {

            IBaseViewModel baseViewModel = null;
            if (obj is Light light)
            {
                baseViewModel = new LightViewModel(_mainWindowViewModel)
                {
                    DataModel = light,
                    Name = "Light"
                };

                int num = 1;
                string name = baseViewModel.Name;
                while (_mainWindowViewModel.EnvironmentPaletteViewModel.Lights.Any(model => model.Name.Equals(name)))
                {
                    name = baseViewModel.Name;
                    name += " ";
                    name += num;
                    ++num;
                }
                baseViewModel.Name = name;

                _mainWindowViewModel.EnvironmentPaletteViewModel.Lights.Add(baseViewModel as LightViewModel);
            }
            else
            {
                baseViewModel = new ModelViewModel(_mainWindowViewModel)
                {
                    DataModel = obj
                };

                switch (obj)
                {
                    case Cube cube:
                        baseViewModel.Name = "Cube";
                        break;
                    case Cylinder cylinder:
                        baseViewModel.Name = "Cylinder";
                        break;
                    case Torus torus:
                        baseViewModel.Name = "Torus";
                        break;
                    //case ImportData data:
                    //    baseViewModel.Name = data.Name;
                    //    break;
                }

                int num = 1;
                string name = baseViewModel.Name;
                while (_mainWindowViewModel.ModelPaletteViewModel.Models.Any(model => model.Name.Equals(name)))
                {
                    name = baseViewModel.Name;
                    name += " ";
                    name += num;
                    ++num;
                }
                baseViewModel.Name = name;
                _mainWindowViewModel.ModelPaletteViewModel.Models.Add(baseViewModel as ModelViewModel);
            }
        }

        private void CreateViewModel(Camera camera)
        {
            CameraViewModel cameraViewModel = new CameraViewModel(_mainWindowViewModel)
            {
                Name = "Camera",
                DataModel = camera
            };

            int num = 1;
            string name = cameraViewModel.Name;
            while (_mainWindowViewModel.CameraPaletteViewModel.Cameras.Any(model => model.Name.Equals(name)))
            {
                name = cameraViewModel.Name;
                name += " ";
                name += num;
                ++num;
            }
            cameraViewModel.Name = name;

            _mainWindowViewModel.CameraPaletteViewModel.Cameras.Add(cameraViewModel);
        }

        private void DeleteViewModel(Camera camera)
        {
            _mainWindowViewModel.CameraPaletteViewModel.Cameras.Remove(_mainWindowViewModel.CameraPaletteViewModel.Cameras.First(c => c.DataModel == camera));
        }

        private void CamerasCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Camera item in e.NewItems)
                {

                    CreateViewModel(item);
                }
            }

            if (e.OldItems != null)
            {
                foreach (Camera item in e.OldItems)
                {

                    DeleteViewModel(item);
                }
            }
        }

        private void DeleteViewModel(APresentable obj)
        {
            if (obj is Light light)
            {
                _mainWindowViewModel.EnvironmentPaletteViewModel.Lights.Remove(_mainWindowViewModel.EnvironmentPaletteViewModel.Lights.First(l => l.DataModel == light));
            }
            else
            {
                _mainWindowViewModel.ModelPaletteViewModel.Models.Remove(_mainWindowViewModel.ModelPaletteViewModel.Models.First(model => model.DataModel == obj));
            }
        }

        private void CreateViewModel(Decal decal)
        {
            DecalViewModel decalViewModel = new DecalViewModel(_mainWindowViewModel)
            {
                Name = Path.GetFileNameWithoutExtension(decal.TextureAsset.AssetPath),
                DataModel = decal
            };

            int num = 1;
            string name = decalViewModel.Name;
            while (_mainWindowViewModel.MaterialsPaletteViewModel.Materials.Any(model => model.Name.Equals(name)))
            {
                name = decalViewModel.Name;
                name += " ";
                name += num;
                ++num;
            }
            decalViewModel.Name = name;

            _mainWindowViewModel.MaterialsPaletteViewModel.Materials.Add(decalViewModel);
        }

        private void DeleteViewModel(Decal decal)
        {
            _mainWindowViewModel.MaterialsPaletteViewModel.Materials.Remove(_mainWindowViewModel.MaterialsPaletteViewModel.Materials.First(mat => ((DecalViewModel)mat).DataModel == decal));
        }

        private void MaterialsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Decal item in e.NewItems)
                {

                    CreateViewModel(item);
                }
            }

            if (e.OldItems != null)
            {
                foreach (Decal item in e.OldItems)
                {

                    DeleteViewModel(item);
                }
            }
        }

        private void PalettePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is ModelPaletteViewModel modelPalette)
            {
                if (e.PropertyName.Equals(nameof(modelPalette.SelectedModel)))
                {
                    _dataProvider.SelectedObject = modelPalette.SelectedModel?.DataModel;
                }
            }
            else if(sender is EnvironmentPaletteViewModel environmentPalette)
            {
                if (e.PropertyName.Equals(nameof(environmentPalette.SelectedLight)))
                {
                    _dataProvider.SelectedObject = environmentPalette.SelectedLight?.DataModel?.Representor;
                }
            }
            else if (sender is CameraPaletteViewModel cameraPalette)
            {
                if (e.PropertyName.Equals(nameof(cameraPalette.SelectedCamera)))
                {
                    if(cameraPalette.SelectedCamera != null && _dataProvider.CurrentCamera.Representor != cameraPalette.SelectedCamera.DataModel.Representor) // HACK - we prevent manipulator to show up for current camera
                        _dataProvider.SelectedObject = cameraPalette.SelectedCamera.DataModel.Representor;
                }
            }

        }

        private void DataProviderPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals(nameof(_dataProvider.SelectedObject)))
            {
                ModelPaletteViewModel palette = _mainWindowViewModel.ModelPaletteViewModel;
                palette.SelectedModel = palette.Models.FirstOrDefault(model => model.DataModel == _dataProvider.SelectedObject);
            }
        }

    }
}
