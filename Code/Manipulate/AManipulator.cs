﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphics;
using SharpDX.Mathematics.Interop;

namespace Manipulate
{
    public abstract class AManipulator
    {
        protected List<AManipulatorHandle> Handles = new List<AManipulatorHandle>();

        public RawMatrix Position { get; set; }

        public virtual void ConstructPass(Pass pass)
        {
            foreach(AManipulatorHandle handle in Handles)
            {
                handle.ConstructPass(pass);
            }
        }

        public bool FindByGuid(Guid guid)
        {
            foreach(AManipulatorHandle handle in  Handles)
            {
                if(handle.Guid == guid)
                {
                    handle.IsSelected = true;
                }
                else
                {
                    handle.IsSelected = false;
                }
            }

            return Handles.Any(h => h.IsSelected);
        }
    }
}
