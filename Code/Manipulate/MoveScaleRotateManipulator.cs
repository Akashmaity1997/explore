﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Interfaces;
using Graphics;
using Assets.Utilities;
using SharpDX.Mathematics.Interop;

namespace Manipulate
{
    public class MoveScaleRotateManipulator : AManipulator
    {
        private readonly IDataProvider _dataProvider;
        private readonly AxialMoveManipulator xAxis;
        private readonly AxialMoveManipulator yAxis;
        private readonly AxialMoveManipulator zAxis;

        public MoveScaleRotateManipulator(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
            xAxis = new AxialMoveManipulator(dataProvider)
            {
                HandleColor = new RawColor3(1, 0, 0)
            };
            yAxis = new AxialMoveManipulator(dataProvider)
            {
                HandleColor = new RawColor3(0, 1, 0)
            };
            zAxis = new AxialMoveManipulator(dataProvider)
            {
                HandleColor = new RawColor3(0, 0, 1)
            };
            Handles.Add(xAxis);
            Handles.Add(yAxis);
            Handles.Add(zAxis);
        }

        public override void ConstructPass(Pass pass)
        {
            xAxis.Position = MatrixHelpers.Create(Vector.NegYAxis, Vector.XAxis, Vector.ZAxis, new Point(Position.M41, Position.M42, Position.M42));
            yAxis.Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(Position.M41, Position.M42, Position.M42));
            zAxis.Position = MatrixHelpers.Create(Vector.XAxis, Vector.NegZAxis, Vector.YAxis, new Point(Position.M41, Position.M42, Position.M42));
            base.ConstructPass(pass);
        }
    }
}
