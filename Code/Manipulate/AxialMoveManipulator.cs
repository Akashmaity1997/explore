﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using Assets;

namespace Manipulate
{
    public class AxialMoveManipulator : AManipulatorHandle
    {
        public AxialMoveManipulator(IDataProvider dataProvider) : base(dataProvider)
        {
            HandleMesh = StandardMeshes.AxialMoveMesh;
        }
    }
}
