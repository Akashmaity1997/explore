﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using Graphics;

namespace Manipulate
{
    public class ManipulatorManager : IDrawingExtension
    {
        private readonly IDataProvider _dataProvider;
        private readonly Dictionary<Type, AManipulator> _dmToManipulator = new Dictionary<Type, AManipulator>();
        
        private AManipulator _currentManipulator;

        public ManipulatorManager(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
            _dataProvider.PropertyChanged += DataProviderPropertyChanged;
            MoveScaleRotateManipulator msr = new MoveScaleRotateManipulator(dataProvider);
            _dmToManipulator.Add(typeof(IPositionableDm), msr);
        }

        private void DataProviderPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(_dataProvider.SelectedDm))
            {
                _currentManipulator = GetManipulator(_dataProvider.SelectedDm);
                if(_currentManipulator != null)
                {
                    _currentManipulator.Position = ((IPositionableDm)_dataProvider.SelectedDm).Position;
                }
            }
        }

        private AManipulator GetManipulator(IBaseDm selection)
        {
            if(selection == null)
            {
                return null;
            }

            foreach(KeyValuePair<Type, AManipulator> pvp in _dmToManipulator)
            {
                if(pvp.Key.IsAssignableFrom(selection.GetType()))
                {
                    return pvp.Value;
                }
            }

            return null;
        }

        public void ConstructPass(Pass pass)
        {
            _currentManipulator?.ConstructPass(pass);
        }

        public bool FindByGuid(Guid guid)
        {
            return _currentManipulator?.FindByGuid(guid) ?? false;
        }
    }
}
