﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphics;
using Assets.Interfaces;
using Assets.Materials;
using SharpDX.Mathematics.Interop;

namespace Manipulate
{
    public abstract class AManipulatorHandle
    {
        private readonly IDataProvider _dataProvider;

        protected AManipulatorHandle(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public Mesh HandleMesh { get; set; }

        public RawColor3 HandleColor { get; set; }

        public RawMatrix Position { get; set; }

        public Guid Guid { get; } = Guid.NewGuid();

        public bool IsSelected { get; set; }

        public void ConstructPass(Pass pass)
        {
            pass.Drawables.Add(new Drawable
            {
                MeshData = HandleMesh,
                Position = Position,
                Camera = _dataProvider.CurrentCameraDm,
                World = _dataProvider.CurrentWorldDm,
                Material = new BasicMaterial { Diffuse = HandleColor },
                ObjectGuid = Guid
            });
        }
    }
}
