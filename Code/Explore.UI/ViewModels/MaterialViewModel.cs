﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Materials;

namespace Explore.UI.ViewModels
{
    public class MaterialViewModel : BaseViewModel
    {
        public MaterialViewModel(MainWindowViewModel mainWindowViewModel):base(mainWindowViewModel)
        {

        }

        public Material DataModel { get; set; }

        public Point Colour
        {
            get => DataModel.Colour;
            set
            {
                DataModel.Colour = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
