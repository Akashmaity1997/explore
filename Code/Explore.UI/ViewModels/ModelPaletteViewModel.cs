﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Assets.Interfaces;
using System.Windows.Controls;
using Assets;

namespace Explore.UI.ViewModels
{
    public class ModelPaletteViewModel : PropertyAware
    {
        public ObservableCollection<ModelViewModel> Models { get; set; } = new ObservableCollection<ModelViewModel>();

        private ModelViewModel _selectedModel;
        public ModelViewModel SelectedModel
        {
            get => _selectedModel;
            set
            {
                if (_selectedModel == value)
                    return;

                _selectedModel = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
