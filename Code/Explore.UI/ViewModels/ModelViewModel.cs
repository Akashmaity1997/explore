﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using Assets;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Specialized;
using Assets.Materials;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using Assets.Utilities;

namespace Explore.UI.ViewModels
{
    public class ModelViewModel : BaseViewModel
    {
        public ModelViewModel(MainWindowViewModel mainWindowViewModel):base(mainWindowViewModel)
        {

        }

        private APresentable _dataModel;
        public APresentable DataModel
        {
            get => _dataModel;
            set
            {
                _dataModel = value;
                _dataModel.PropertyChanged += DataModelPropertyChanged;
                _dataModel.Decals.CollectionChanged += TexturesCollectionChanged;
                _rotationX = DataModel.AngleX;
                _rotationY = DataModel.AngleY;
                _rotationZ = DataModel.AngleZ;
                _pivotRotationX = DataModel.OffsetAngleX;
                _pivotRotationY = DataModel.OffsetAngleY;
                _pivotRotationZ = DataModel.OffsetAngleZ;
                RaiseAllPropertyChanged();
            }
        }

        public ObservableCollection<DecalViewModel> Textures { get; set; } = new ObservableCollection<DecalViewModel>();

        public ObservableCollection<string> TextureMappings { get; set; } = new ObservableCollection<string>
        {
            EnumToString.GetStringValue(TextureMappingType.UV),
            EnumToString.GetStringValue(TextureMappingType.Cubic),
            EnumToString.GetStringValue(TextureMappingType.Cylindrical)
        };

        public string SelectedTextureMapping
        {
            get => EnumToString.GetStringValue(DataModel.TextureMapping);
            set
            {
                DataModel.TextureMapping = value.GetEnum<TextureMappingType>();
                RaiseThisPropertyChanged();
            }
        }

        public RawMatrix Transform
        {
            get => DataModel.PivotOffset.Multiply(DataModel.Position);
            set => DataModel.Position = DataModel.PivotOffset.Inverse().Multiply(value);
        }

        public RawMatrix PivotTransform
        {
            get => DataModel.Position;
            set
            {
                RawMatrix transform = MatrixHelpers.Create(Transform);
                DataModel.Position = value;
                DataModel.PivotOffset = transform.Multiply(DataModel.Position.Inverse());
            }
        }

        public float PositionX
        {
            get => Transform.M41;
            set
            {
                if (Transform.M41 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(Transform);
                transform.M41 = value;
                Transform = transform;
                RaiseThisPropertyChanged();
            }
        }

        public float PositionY
        {
            get => Transform.M42;
            set
            {
                if (Transform.M42 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(Transform);
                transform.M42 = value;
                Transform = transform;
                RaiseThisPropertyChanged();
            }
        }

        public float PositionZ
        {
            get => Transform.M43;
            set
            {
                if (Transform.M43 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(Transform);
                transform.M43 = value;
                Transform = transform;
                RaiseThisPropertyChanged();
            }
        }

        public float PivotPositionX
        {
            get => PivotTransform.M41;
            set
            {
                if (PivotTransform.M41 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(PivotTransform);
                transform.M41 = value;
                PivotTransform = transform;
                RaiseThisPropertyChanged();
            }
        }

        public float PivotPositionY
        {
            get => PivotTransform.M42;
            set
            {
                if (PivotTransform.M42 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(PivotTransform);
                transform.M42 = value;
                PivotTransform = transform;
                RaiseThisPropertyChanged();
            }
        }

        public float PivotPositionZ
        {
            get => PivotTransform.M43;
            set
            {
                if (PivotTransform.M43 == value)
                    return;

                RawMatrix transform = MatrixHelpers.Create(PivotTransform);
                transform.M43 = value;
                PivotTransform = transform;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationX;
        public float RotationX
        {
            get => _rotationX;
            set
            {
                if (_rotationX == value)
                    return;

                DataModel.AngleX = value;
                RecalculateRotationalPosition(value - _rotationX, Vector.XAxis);
                _rotationX = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationY;
        public float RotationY
        {
            get => _rotationY;
            set
            {
                if (_rotationY == value)
                    return;

                DataModel.AngleY = value;
                RecalculateRotationalPosition(value - _rotationY, Vector.YAxis);
                _rotationY = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationZ;
        public float RotationZ
        {
            get => _rotationZ;
            set
            {
                if (_rotationZ == value)
                    return;

                DataModel.AngleZ = value;
                RecalculateRotationalPosition(value - _rotationZ, Vector.ZAxis);
                _rotationZ = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _pivotRotationX;
        public float PivotRotationX
        {
            get => _pivotRotationX;
            set
            {
                if (_pivotRotationX == value)
                    return;

                DataModel.OffsetAngleX = value;
                RecalculateRotationalPosition(value - _pivotRotationX, Vector.XAxis, true);
                _pivotRotationX = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _pivotRotationY;
        public float PivotRotationY
        {
            get => _pivotRotationY;
            set
            {
                if (_pivotRotationY == value)
                    return;

                DataModel.OffsetAngleY = value;
                RecalculateRotationalPosition(value - _pivotRotationY, Vector.YAxis, true);
                _pivotRotationY = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _pivotRotationZ;
        public float PivotRotationZ
        {
            get => _pivotRotationZ;
            set
            {
                if (_pivotRotationZ == value)
                    return;

                DataModel.OffsetAngleZ = value;
                RecalculateRotationalPosition(value - _pivotRotationZ, Vector.ZAxis, true);
                _pivotRotationZ = value;
                RaiseThisPropertyChanged();
            }
        }

        public bool ScaleMode
        {
            get => _dataModel.ScaleMode;
            set
            {
                _dataModel.ScaleMode = value;
                RaiseThisPropertyChanged();
            }
        }

        private void RecalculateRotationalPosition(float deg, Vector axis, bool isPivot = false)
        {
            if(isPivot)
                PivotTransform = MatrixHelpers.RotateAbout(new Point(PivotPositionX, PivotPositionY, PivotPositionZ), PivotTransform, deg, axis);
            else
                Transform = MatrixHelpers.RotateAbout(new Point(PivotPositionX, PivotPositionY, PivotPositionZ), Transform, deg, axis);
        }

        public float ScaleX
        {
            get => new Vector(Transform.M11, Transform.M12, Transform.M13).Magnitude();
            set
            {
                CalculateScaleMatrix(new Vector(value / ScaleX, 1, 1));
                RaiseThisPropertyChanged();
            }
        }

        public float ScaleY
        {
            get => new Vector(Transform.M21, Transform.M22, Transform.M23).Magnitude();
            set
            {
                CalculateScaleMatrix(new Vector(1, value / ScaleY, 1));
                RaiseThisPropertyChanged();
            }
        }

        public float ScaleZ
        {
            get => new Vector(Transform.M31, Transform.M32, Transform.M33).Magnitude();
            set
            {
                CalculateScaleMatrix(new Vector(1, 1, value / ScaleZ));
                RaiseThisPropertyChanged();
            }
        }

        private void CalculateScaleMatrix(Vector scale)
        {
            Transform = MatrixHelpers.ScaleAbout(PivotTransform, Transform, scale);
        }

        private void DataModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(DataModel.Position)) ||
                e.PropertyName.Equals(nameof(DataModel.PivotOffset)))
            {
                _rotationX = DataModel.AngleX;
                _rotationY = DataModel.AngleY;
                _rotationZ = DataModel.AngleZ;
                _pivotRotationX = DataModel.OffsetAngleX;
                _pivotRotationY = DataModel.OffsetAngleY;
                _pivotRotationZ = DataModel.OffsetAngleZ;
                RaiseAllPropertyChanged();
            }
        }

        private void TexturesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.NewItems != null)
            {
                foreach(Decal decal in e.NewItems)
                {
                    DecalViewModel texture =_mainWindowViewModel.MaterialsPaletteViewModel.Materials.First(tex => tex is DecalViewModel dec && dec.DataModel == decal) as DecalViewModel;
                    Textures.Add(texture);
                }
            }

            if(e.OldItems != null)
            {
                foreach(Decal decal in e.OldItems)
                {
                    DecalViewModel texture = _mainWindowViewModel.MaterialsPaletteViewModel.Materials.First(tex => tex is DecalViewModel dec && dec.DataModel == decal) as DecalViewModel;
                    Textures.Remove(texture);
                }
            }
        }

        private ICommand _snapToOriginCommand;
        public ICommand SnapToOriginCommand => _snapToOriginCommand ?? (_snapToOriginCommand = new RelayCommand((param) =>
        {
            CalculateScaleMatrix(new Vector(1 / ScaleX, 1 / ScaleY, 1 / ScaleZ));
        }));

        private ICommand _resetScaleCommand;
        public ICommand ResetScaleCommand => _resetScaleCommand ?? (_resetScaleCommand = new RelayCommand((param) =>
        {
            Transform = Transform.Normalize();
        }));
    }
}
