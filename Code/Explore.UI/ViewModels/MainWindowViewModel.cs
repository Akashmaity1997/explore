﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;

namespace Explore.UI.ViewModels
{
    public class MainWindowViewModel : PropertyAware
    {
        public MainWindowViewModel()
        {
            _modelPaletteViewModel = new ModelPaletteViewModel();
            _materialsPaletteViewModel = new MaterialsPaletteViewModel();
            _environmentPaletteViewModel = new EnvironmentPaletteViewModel();
            _cameraPaletteViewModel = new CameraPaletteViewModel();
        }

        private ModelPaletteViewModel _modelPaletteViewModel;
        public ModelPaletteViewModel ModelPaletteViewModel
        {
            get => _modelPaletteViewModel;
            set
            {
                _modelPaletteViewModel = value;
                RaiseThisPropertyChanged();
            }
        }

        private MaterialsPaletteViewModel _materialsPaletteViewModel;
        public MaterialsPaletteViewModel MaterialsPaletteViewModel
        {
            get => _materialsPaletteViewModel;
            set
            {
                _materialsPaletteViewModel = value;
                RaiseThisPropertyChanged();
            }
        }

        private EnvironmentPaletteViewModel _environmentPaletteViewModel;
        public EnvironmentPaletteViewModel EnvironmentPaletteViewModel
        {
            get => _environmentPaletteViewModel;
            set
            {
                _environmentPaletteViewModel = value;
                RaiseThisPropertyChanged();
            }
        }

        private CameraPaletteViewModel _cameraPaletteViewModel;
        public CameraPaletteViewModel CameraPaletteViewModel
        {
            get => _cameraPaletteViewModel;
            set
            {
                _cameraPaletteViewModel = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
