﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;
using System.Windows.Input;
using Assets.Utilities;
using System.Collections.ObjectModel;

namespace Explore.UI.ViewModels
{
    public class LightViewModel : BaseViewModel
    {
        public LightViewModel(MainWindowViewModel mainWindowViewModel):base(mainWindowViewModel)
        {

        }

        private Light _dataModel;
        public Light DataModel
        {
            get => _dataModel;
            set
            {
                _dataModel = value;
                _dataModel.PropertyChanged += DataModelPropertyChanged;
                _colourR = DataModel.Colour.X * 255.0f;
                _colourG = DataModel.Colour.Y * 255.0f;
                _colourB = DataModel.Colour.Z * 255.0f;
                _rotationX = DataModel.AngleX;
                _rotationY = DataModel.AngleY;
                _rotationZ = DataModel.AngleZ;
                RaiseAllPropertyChanged();
            }
        }

        public ObservableCollection<string> LightTypes { get; set; } = new ObservableCollection<string>
        {
            EnumToString.GetStringValue(LightType.Point),
            EnumToString.GetStringValue(LightType.Directional),
            EnumToString.GetStringValue(LightType.Spot)

        };

        public ObservableCollection<string> AreaTypes { get; set; } = new ObservableCollection<string>
        {
            EnumToString.GetStringValue(AreaType.Circular)

        };

        public string Type
        {
            get => EnumToString.GetStringValue(DataModel.Type);
            set
            {
                DataModel.Type = value.GetEnum<LightType>();
                RaiseThisPropertyChanged();
            }
        }

        public string Area
        {
            get => EnumToString.GetStringValue(DataModel.Area);
            set
            {
                DataModel.Area = value.GetEnum<AreaType>();
                RaiseThisPropertyChanged();
            }
        }

        private float _colourR;
        public float ColourR
        {
            get => _colourR;
            set
            {
                if (_colourR == value)
                    return;

                _colourR = value;
                RecalculateColour();
                RaiseThisPropertyChanged();
            }
        }

        private float _colourG;
        public float ColourG
        {
            get => _colourG;
            set
            {
                if (_colourG == value)
                    return;

                _colourG = value;
                RecalculateColour();
                RaiseThisPropertyChanged();
            }
        }

        private float _colourB;
        public float ColourB
        {
            get => _colourB;
            set
            {
                if (_colourB == value)
                    return;

                _colourB = value;
                RecalculateColour();
                RaiseThisPropertyChanged();
            }
        }

        private void RecalculateColour()
        {
            DataModel.Colour = new RawVector3(ColourR / 255.0f, ColourG / 255.0f, ColourB / 255.0f);
        }

        public float PositionX
        {
            get => DataModel.Position.M41;
            set
            {
                if (DataModel.Position.M41 == value)
                    return;

                RawMatrix pos = MatrixHelpers.Create(DataModel.Position);
                pos.M41 = value;
                DataModel.Position = pos;
                RaiseThisPropertyChanged();
            }
        }

        public float PositionY
        {
            get => DataModel.Position.M42;
            set
            {
                if (DataModel.Position.M42 == value)
                    return;

                RawMatrix pos = MatrixHelpers.Create(DataModel.Position);
                pos.M42 = value;
                DataModel.Position = pos;
                RaiseThisPropertyChanged();
            }
        }

        public float PositionZ
        {
            get => DataModel.Position.M43;
            set
            {
                if (DataModel.Position.M43 == value)
                    return;

                RawMatrix pos = MatrixHelpers.Create(DataModel.Position);
                pos.M43 = value;
                DataModel.Position = pos;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationX;
        public float RotationX
        {
            get => _rotationX;
            set
            {
                if (_rotationX == value)
                    return;

                DataModel.AngleX = value;
                RecalculateRotationalPosition(value - _rotationX, Vector.XAxis);
                _rotationX = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationY;
        public float RotationY
        {
            get => _rotationY;
            set
            {
                if (_rotationY == value)
                    return;

                DataModel.AngleY = value;
                RecalculateRotationalPosition(value - _rotationY, Vector.YAxis);
                _rotationY = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _rotationZ;
        public float RotationZ
        {
            get => _rotationZ;
            set
            {
                if (_rotationZ == value)
                    return;

                DataModel.AngleZ = value;
                RecalculateRotationalPosition(value - _rotationZ, Vector.ZAxis);
                _rotationZ = value;
                RaiseThisPropertyChanged();
            }
        }

        private void RecalculateRotationalPosition(float deg, Vector axis)
        {
            DataModel.Position = MatrixHelpers.RotateAbout(new Point(DataModel.Position.M41, DataModel.Position.M42, DataModel.Position.M43), DataModel.Position, deg, axis);
        }

        private void DataModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals(nameof(DataModel.Position)))
            {
                _rotationX = DataModel.AngleX;
                _rotationY = DataModel.AngleY;
                _rotationZ = DataModel.AngleZ;
                RaisePropertyChanged(nameof(PositionX));
                RaisePropertyChanged(nameof(PositionY));
                RaisePropertyChanged(nameof(PositionZ));
                RaisePropertyChanged(nameof(RotationX));
                RaisePropertyChanged(nameof(RotationY));
                RaisePropertyChanged(nameof(RotationZ));
            }
        }

        private ICommand _snapToOriginCommand;
        public ICommand SnapToOriginCommand => _snapToOriginCommand ?? (_snapToOriginCommand = new RelayCommand((param) =>
        {
            PositionX = PositionY = PositionZ = 0;
        }));
    }
}
