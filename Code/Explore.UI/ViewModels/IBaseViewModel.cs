﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;

namespace Explore.UI.ViewModels
{
    public interface IBaseViewModel
    {
        string Name { get; set; }
    }

    public class BaseViewModel : PropertyAware, IBaseViewModel
    {
        protected readonly MainWindowViewModel _mainWindowViewModel;

        public BaseViewModel(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
