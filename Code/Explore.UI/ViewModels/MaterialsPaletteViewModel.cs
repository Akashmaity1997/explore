﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Assets;

namespace Explore.UI.ViewModels
{
    public class MaterialsPaletteViewModel : PropertyAware
    {

        public ObservableCollection<IBaseViewModel> Materials { get; set; } = new ObservableCollection<IBaseViewModel>();

        private IBaseViewModel _selectedMaterial;
        public IBaseViewModel SelectedMaterial
        {
            get => _selectedMaterial;
            set
            {
                if (_selectedMaterial == value)
                    return;

                _selectedMaterial = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
