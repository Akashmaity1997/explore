﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using Assets.Materials;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Explore.UI.ViewModels
{
    public class DecalViewModel : BaseViewModel
    {

        public DecalViewModel(MainWindowViewModel mainWindowViewModel):base(mainWindowViewModel)
        {

        }

        private Decal _dataModel;
        public Decal DataModel
        {
            get => _dataModel;
            set
            {
                _dataModel = value;
                Thumbnail = _dataModel.Thumbnail;
                RaiseThisPropertyChanged();
            }
        }

        private BitmapImage _thumbnail;
        public BitmapImage Thumbnail
        {
            get => _thumbnail;
            set
            {
                _thumbnail = value;
                RaiseThisPropertyChanged();
            }
        }

        private ICommand _setOnPartCommand;
        public ICommand SetOnPartCommand => _setOnPartCommand ?? (_setOnPartCommand = new RelayCommand((param) =>
        {
            DataModel.SetOnPart();
        }));

        private ICommand _removeFromPartCommand;
        public ICommand RemoveFromPartCommand => _removeFromPartCommand ?? (_removeFromPartCommand = new RelayCommand((param) =>
        {
            DataModel.RemoveFromPart();
        }));
    }
}
