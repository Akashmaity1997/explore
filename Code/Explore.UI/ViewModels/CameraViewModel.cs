﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using System.Windows.Input;
using SharpDX.Mathematics.Interop;

namespace Explore.UI.ViewModels
{
    public class CameraViewModel : BaseViewModel
    {
        public CameraViewModel(MainWindowViewModel mainWindowViewModel):base(mainWindowViewModel)
        {

        }

        private Camera _dataModel;
        public Camera DataModel
        {
            get => _dataModel;
            set
            {
                _dataModel = value;
                RawMatrix pos = _dataModel.Position;
                Vector zVector = new Vector(pos.M31, pos.M32, pos.M33);
                Vector zVectorShadow = new Vector(zVector.X, 0, zVector.Z).Normalize();
                _distance = (_dataModel.LookAt - new Vector(pos.M41, pos.M42, pos.M43)).Magnitude();
                _latitude = (float)(Math.Acos(zVectorShadow.Flip().Dot(Vector.XAxis)) * 180.0f / Math.PI);
                _longitude = (float)(Math.Acos(zVector.Dot(zVectorShadow)) * 180.0f / Math.PI);
            }
        }

        private float _distance;
        public float Distance
        {
            get => _distance;
            set
            {
                _distance = value;
                RawMatrix pos = _dataModel.Position;
                Vector modifiedPos = _dataModel.LookAt - new Vector(pos.M31 * value, pos.M32 * value, pos.M33 * value);
                RawMatrix m = MatrixHelpers.Create(new Vector(pos.M11, pos.M12, pos.M13), new Vector(pos.M21, pos.M22, pos.M23), new Vector(pos.M31, pos.M32, pos.M33), new Point(modifiedPos.X, modifiedPos.Y, modifiedPos.Z));
                _dataModel.Representor.Position = m;
                RaiseThisPropertyChanged();
            }
        }

        private float _latitude;
        public float Latitude
        {
            get => _latitude;
            set
            {
                CalculateCameraAttitude(value, _longitude);
                _latitude = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _longitude;
        public float Longitude
        {
            get => _longitude;
            set
            {
                CalculateCameraAttitude(_latitude, value);
                _longitude = value;
                RaiseThisPropertyChanged();
            }
        }

        private void CalculateCameraAttitude(float latitude, float longitude)
        {
            float deltaLat = latitude - _latitude;

            float deltaLong = longitude;

            RawMatrix pos = _dataModel.Position;
            Vector zVector = new Vector(pos.M31, pos.M32, pos.M33);
            Vector zVectorShadow = new Vector(zVector.X, 0, zVector.Z).Normalize();
            Vector zVectorShadowRotated = zVectorShadow.Rotate(deltaLat, Vector.YAxis).Normalize();
            Vector ortho = new Vector(zVectorShadowRotated.Z, 0, -zVectorShadowRotated.X); // also xNorm
            Vector zNorm = zVectorShadowRotated.Rotate(deltaLong, ortho).Normalize();
            Vector y = ortho.Cross(zNorm).Normalize();
            Vector yNorm = y.Flip();

            Vector modifiedPos = _dataModel.LookAt - new Vector(zNorm.X * _distance, zNorm.Y * _distance, zNorm.Z * _distance);
            RawMatrix m = MatrixHelpers.Create(ortho, yNorm, zNorm, new Point(modifiedPos.X, modifiedPos.Y, modifiedPos.Z));
            _dataModel.Representor.Position = m;
        }

        private ICommand _setCurrentCameraCommand;
        public ICommand SetCurrentCameraCommand => _setCurrentCameraCommand ?? (_setCurrentCameraCommand = new RelayCommand((param) =>
        {
            DataModel.SetCurrent();
        }));

        private ICommand _pickTargetCommand;
        public ICommand PickTargetCommand => _pickTargetCommand ?? (_pickTargetCommand = new RelayCommand(param => 
        {
            DataModel.AssignTarget();
        }));

        private ICommand _followObjectCommand;
        public ICommand FollowObjectCommand => _followObjectCommand ?? (_followObjectCommand = new RelayCommand(param =>
        {
            DataModel.AssignTargetAndFollow();
        }));

        private ICommand _unfollowObjectCommand;
        public ICommand UnfollowObjectCommand => _unfollowObjectCommand ?? (_unfollowObjectCommand = new RelayCommand(param =>
        {
            DataModel.FollowingObject = null;
        }));
    }
}
