﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using System.Collections.ObjectModel;

namespace Explore.UI.ViewModels
{
    public class CameraPaletteViewModel : PropertyAware
    {
        public ObservableCollection<CameraViewModel> Cameras { get; set; } = new ObservableCollection<CameraViewModel>();

        private CameraViewModel _selectedCamera;
        public CameraViewModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (_selectedCamera == value)
                    return;

                _selectedCamera = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
