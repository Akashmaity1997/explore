﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets;
using System.Collections.ObjectModel;

namespace Explore.UI.ViewModels
{
    public class EnvironmentPaletteViewModel : PropertyAware
    {
        public ObservableCollection<LightViewModel> Lights { get; set; } = new ObservableCollection<LightViewModel>();

        private LightViewModel _selectedLight;
        public LightViewModel SelectedLight
        {
            get => _selectedLight;
            set
            {
                if (_selectedLight == value)
                    return;

                _selectedLight = value;
                RaiseThisPropertyChanged();
            }
        }
    }
}
