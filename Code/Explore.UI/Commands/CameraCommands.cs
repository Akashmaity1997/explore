﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Explore.UI.Commands
{
    public static class CameraCommands
    {
        public static RoutedUICommand CreateCameraCommand = new RoutedUICommand("CreateCamera", "CreateCameraCommand", typeof(CameraCommands));
    }
}
