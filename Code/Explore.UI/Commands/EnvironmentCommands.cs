﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Explore.UI.Commands
{
    public static class EnvironmentCommands
    {

        public static RoutedUICommand CreateLightCommand = new RoutedUICommand("CreateLight", "CreateLightCommand", typeof(EnvironmentCommands));
    }
}
