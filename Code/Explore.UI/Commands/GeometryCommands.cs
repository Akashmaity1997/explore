﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Explore.UI.Commands
{
    public static class GeometryCommands
    {
        public static RoutedUICommand CreateCubeCommand = new RoutedUICommand("CreateCube", "CreateCubeCommand", typeof(GeometryCommands));
        public static RoutedUICommand CreateCylinderCommand = new RoutedUICommand("CreateCylinder", "CreateCylinderCommand", typeof(GeometryCommands));
        public static RoutedUICommand CreateSphereCommand = new RoutedUICommand("CreateSphere", "CreateSphereCommand", typeof(GeometryCommands));
        public static RoutedUICommand CreateTorusCommand = new RoutedUICommand("CreateTorus", "CreateTorusCommand", typeof(GeometryCommands));
        public static RoutedUICommand CreateConeCommand = new RoutedUICommand("CreateCone", "CreateConeCommand", typeof(GeometryCommands));
        public static RoutedUICommand ImportModelCommand = new RoutedUICommand("ImportModel", "ImportModelCommand", typeof(GeometryCommands));
    }
}
