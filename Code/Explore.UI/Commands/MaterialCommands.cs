﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Explore.UI.Commands
{
    public static class MaterialCommands
    {
        public static RoutedUICommand CreateDecalCommand = new RoutedUICommand("CreateDecal", "CreateDecalCommand", typeof(MaterialCommands));
    }
}
