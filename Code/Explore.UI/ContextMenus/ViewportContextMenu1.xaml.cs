﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Explore.UI.ContextMenus
{
    /// <summary>
    /// Interaction logic for ViewportContextMenu1.xaml
    /// </summary>
    public partial class ViewportContextMenu1 : ContextMenu
    {
        public ViewportContextMenu1()
        {
            InitializeComponent();
        }
    }
}
