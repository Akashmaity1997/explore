﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D;

namespace Assets
{
    public enum ImageFormat
    {
        BGRA8,
        RGBA8,
        RGBE8,
        RGBA16,
        RGBA32
    }

    public class Image
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public ImageFormat Format { get; set; }

        public byte[] Buffer { get; set; }

        public Image(int width, int height, ImageFormat format)
        {
            int bytesPerPixel = 1;
            if(format == ImageFormat.RGBA16)
            {
                bytesPerPixel = 2;
            }
            else if(format == ImageFormat.RGBA32)
            {
                bytesPerPixel = 4;
            }
            Width = width;
            Height = height;
            Format = format;
            Buffer = new byte[width * height * 4 * bytesPerPixel];
        }
    }
}
