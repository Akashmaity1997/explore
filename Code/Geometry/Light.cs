﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;
using Assets.Utilities;

namespace Assets
{
    public class Light : APresentable
    {
        private readonly Cube _lightCube;

        public Cube Representor => _lightCube;

        public Light(RawMatrix position, Vector target, float brightness, RawVector3 colour, IDataProvider dataProvider) : base(position, dataProvider)
        { 
            _target = target;
            Brightness = brightness;
            _colour = colour;
            _lightCube = new Cube(position, dataProvider, new Point(1,1,0), 0.1f);
            _lightCube.PropertyChanged += RepresentorPropertyChanged;
            RecalculateDirection();
            _dataProvider.RegisterObject(this);
        }

        private RawVector3 _colour;
        public RawVector3 Colour
        {
            get => _colour;
            set
            {
                _colour = value;
                RaiseThisPropertyChanged();
            }
        }

        protected override void CreateGeometryData()
        {
            throw new NotImplementedException();
        }

        public void Unregister()
        {
            _lightCube.PropertyChanged -= RepresentorPropertyChanged;
            _dataProvider.UnRegisterObject(_lightCube);
            //_dataProvider.UnRegisterObject(this);
        }

        private void RepresentorPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(_lightCube.Position)))
            {
                RecalculateDirection();
                RaisePropertyChanged(nameof(Position));
            }
        }

        public override RawMatrix Position
        { 
            get => Representor.Position;
            set
            {
                if (Representor == null)
                    return;

                Representor.Position = value;
                RecalculateDirection();
                RaiseThisPropertyChanged();
            }
        }

        #region pivot orientation overrides

        public override float AngleX 
        {
            get => Representor.AngleX;
            set
            {
                Representor.AngleX = value;
            }
        }

        public override float AngleY
        {
            get => Representor.AngleY;
            set
            {
                Representor.AngleY = value;
            }
        }

        public override float AngleZ
        {
            get => Representor.AngleZ;
            set
            {
                Representor.AngleZ = value;
            }
        }

        #endregion

        public float Brightness { get; set; }

        private Vector _target;
        public Vector Target 
        { 
            get => _target;
            set
            {
                _target = value;
                RecalculateDirection();
            } 
        }

        private LightType _type;
        public LightType Type
        {
            get => _type;
            set
            {
                if (_type == value)
                    return;

                _type = value;
                RaiseThisPropertyChanged();
            }
        }

        private AreaType _area;
        public AreaType Area
        {
            get => _area;
            set
            {
                if (_area == value)
                    return;

                _area = value;
                RaiseThisPropertyChanged();
            }
        }

        public Vector Directional => new Vector(Representor.Position.M31, Representor.Position.M32, Representor.Position.M33).Flip();

        private float _coneAngle;
        public float ConeAngle
        {
            get => _coneAngle;
            set
            {
                _coneAngle = value;
                RaiseThisPropertyChanged();
            }
        }

        private Vector _direction;
        public Vector Direction
        { 
            get => _direction;
            set
            {
                _direction = value;
            } 
        }

        private void RecalculateDirection()
        {
            if (Target == null)
                return;

            Vector pos = new Vector(Position.M41, Position.M42, Position.M43);
            Direction = Target - pos;
        }

        public override VertexInterface[] Draw()
        {
            return new VertexInterface[] { };
        }

        public override VertexInterface[] DrawPickable()
        {
            return new VertexInterface[] { };
        }
    }
}
