﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Assets
{
    public class AssetManager
    {
        private static readonly Dictionary<string, string> _directoyMapping = new Dictionary<string, string>();

        public AssetManager()
        {
            _directoyMapping.Add("Decal", "DecalThumbnails");

            if (Directory.Exists(DecalThumbnailDir))
            {
                Directory.Delete(DecalThumbnailDir, true);
            }

            Directory.CreateDirectory(DecalThumbnailDir);
        }


        public static string TempDir => Path.GetTempPath();

        public static string DecalThumbnailDir => Path.Combine(TempDir, _directoyMapping["Decal"]);

        public static string ShaderPath => $@"{AppDomain.CurrentDomain.BaseDirectory}\Shader\\Shader.fx";
    }
}
