﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;

namespace Assets
{
    public class Torus : APresentable, IPositionable
    {
        private float SECTION_ANGLE = 9.0f, WIDTH_RES_ANGLE = 15.0f;
        private readonly float _rad, _width;

        public Point Colour { get; set; } = new Point(0.0f, 1.0f, 0.0f);

        public float Radius => _rad;

        public Torus(float radius, float width, IDataProvider dataProvider, Point colour, bool isManipulator = false) : base(MatrixHelpers.Identity, dataProvider)
        {
            _rad = radius;
            _width = width;
            IsManipulator = isManipulator;
            Colour = colour;
            CreateGeometryData();
            _dataProvider.RegisterObject(this);

        }

        public Torus(RawMatrix position, float radius, float width, IDataProvider dataProvider, Point colour, bool isManipulator = false) : base(position, dataProvider)
        {
            _rad = radius;
            _width = width;
            IsManipulator = isManipulator;
            Colour = colour;
            CreateGeometryData();
            _dataProvider.RegisterObject(this);
        }

        protected override void CreateGeometryData()
        {
            if (IsManipulator || IsSpecial)
            {
                SECTION_ANGLE = 15.0f;
                WIDTH_RES_ANGLE = 45.0f;
            }

            List<Point> basePts = new List<Point>();
            for (float theta2 = 0; theta2 <= 360.0f - WIDTH_RES_ANGLE; theta2 += WIDTH_RES_ANGLE)
            {
                Point basePointShift = new Point(0.0f, 0, -_width / 2.0f).Rotate(-theta2, Vector.XAxis);
                basePts.Add(basePointShift);
            }

            List<Point> actual = new List<Point>();
            for (float theta = 0; theta <= 360.0f - SECTION_ANGLE; theta += SECTION_ANGLE)
            {
                foreach (Point p in basePts)
                {
                    Point shiftedBasePointShift = p.Rotate(-theta, Vector.YAxis);
                    Point bias = new Point(0, 0, -_rad).Rotate(-theta, Vector.YAxis);
                    actual.Add((bias + shiftedBasePointShift));
                }
            }

            _vertexInfo = new VertexInfo(actual);
        }

        public override VertexInterface[] Draw()
        {
            if (_cachedVertexInterface == null)
            {
                Point[] geometryData = _vertexInfo.GeometryData.ToArray();
                int resWidth = (int)(360.0f / WIDTH_RES_ANGLE);
                int sectionWidth = (int)(360.0f / SECTION_ANGLE);
                List<Point> pts = new List<Point>();
                int y;
                for (y = 0; y <= (sectionWidth - 2) * resWidth; y += resWidth)
                {
                    int i;
                    for (i = y; i <= y + resWidth - 2; ++i)
                    {
                        pts.Add(geometryData[i]);
                        pts.Add(Colour);
                        pts.Add(geometryData[i + resWidth]);
                        pts.Add(Colour);
                        pts.Add(geometryData[i + resWidth + 1]);
                        pts.Add(Colour);
                        pts.Add(geometryData[i + resWidth + 1]);
                        pts.Add(Colour);
                        pts.Add(geometryData[i + resWidth - (resWidth - 1)]);
                        pts.Add(Colour);
                        pts.Add(geometryData[i]);
                        pts.Add(Colour);
                    }
                    pts.Add(geometryData[i]);
                    pts.Add(Colour);
                    pts.Add(geometryData[i + resWidth]);
                    pts.Add(Colour);
                    pts.Add(geometryData[i + resWidth - (resWidth - 1)]);
                    pts.Add(Colour);
                    pts.Add(geometryData[i + resWidth - (resWidth - 1)]);
                    pts.Add(Colour);
                    pts.Add(geometryData[i + resWidth - (resWidth - 1) - resWidth]);
                    pts.Add(Colour);
                    pts.Add(geometryData[i]);
                    pts.Add(Colour);
                }
                int j;
                for (j = y; j <= y + resWidth - 2; ++j)
                {
                    pts.Add(geometryData[j]);
                    pts.Add(Colour);
                    pts.Add(geometryData[j - resWidth * (sectionWidth - 1)]);
                    pts.Add(Colour);
                    pts.Add(geometryData[j - resWidth * (sectionWidth - 1) + 1]);
                    pts.Add(Colour);
                    pts.Add(geometryData[j - resWidth * (sectionWidth - 1) + 1]);
                    pts.Add(Colour);
                    pts.Add(geometryData[j + 1]);
                    pts.Add(Colour);
                    pts.Add(geometryData[j]);
                    pts.Add(Colour);
                }
                pts.Add(geometryData[j]);
                pts.Add(Colour);
                pts.Add(geometryData[j - resWidth * (sectionWidth - 1)]);
                pts.Add(Colour);
                pts.Add(geometryData[j - resWidth * (sectionWidth - 1) - (resWidth - 1)]);
                pts.Add(Colour);
                pts.Add(geometryData[j - resWidth * (sectionWidth - 1) - (resWidth - 1)]);
                pts.Add(Colour);
                pts.Add(geometryData[j - resWidth * (sectionWidth - 1) - (resWidth - 1) + (sectionWidth - 1) * resWidth]);
                pts.Add(Colour);
                pts.Add(geometryData[j]);
                pts.Add(Colour);

                CalculateTangentSpace(pts.ToArray());

                List<VertexInterface> vertss = new List<VertexInterface>();
                List<VertexInterface> vtsp = new List<VertexInterface>();
                for (int i = 0; i < pts.Count; i += 2)
                {
                    Point p = pts[i];
                    vertss.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    int index = _vertexInfo.GeometryData.IndexOf(p);
                    //Vector v = _vertexInfo.VertexNormals[index];
                    //vertss.Add(new VertexInterface { X = v.X, Y = v.Y, Z = v.Z, W = v.W });
                    p = pts[i + 1];
                    vertss.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = Pickable.PickColour.X, Y = Pickable.PickColour.Y, Z = Pickable.PickColour.Z, W = Pickable.PickColour.W });
                    RawVector2 tex = _vertexInfo.TexCoords[i/2];
                    vertss.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    vtsp.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    RawMatrix ts = _vertexInfo.TangentSpace[index];
                    vertss.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vertss.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vertss.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });
                    vtsp.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vtsp.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vtsp.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });
                }
                _cachedVertexInterface = vertss.ToArray();
                _cachedVertexInterfaceForPickable = vtsp.ToArray();
            }

            return _cachedVertexInterface;
        }

        public override VertexInterface[] DrawPickable()
        {
            if (_cachedVertexInterface == null)
                Draw();

            return _cachedVertexInterfaceForPickable;
        }
    }
}
