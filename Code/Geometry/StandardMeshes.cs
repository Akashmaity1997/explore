﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace Assets
{
    public static class StandardMeshes
    {
        private static Mesh _axisCylinder;
        public static Mesh AxisCylinder => _axisCylinder ?? (_axisCylinder = ProceduralMeshGenerator.CreateCylinder(2.5f, 0.01f));

        private static Mesh _axialMoveCylinder;
        public static Mesh AxialMoveCylinder => _axialMoveCylinder ?? (_axialMoveCylinder = ProceduralMeshGenerator.CreateCylinder(MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 1.25f, 0)), 2.5f, 0.05f));

        private static Mesh _axialMoveCone;
        public static Mesh AxialMoveCone => _axialMoveCone ?? (_axialMoveCone = ProceduralMeshGenerator.CreateCone(MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(0, 2.5f, 0)), 0.05f, 0.1f));

        private static Mesh _axialMoveMesh;
        public static Mesh AxialMoveMesh => _axialMoveMesh ?? (_axialMoveMesh = AxialMoveCylinder.Combine(AxialMoveCone));

        private static Mesh _unitSphere;
        public static Mesh UnitSphere => _unitSphere ?? (_unitSphere = ProceduralMeshGenerator.CreateSphere());

        private static Mesh _unitPlane;
        public static Mesh UnitPlane => _unitPlane ?? (_unitPlane = ProceduralMeshGenerator.CreatePlane());
    }
}
