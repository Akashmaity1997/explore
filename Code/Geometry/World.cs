﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;

namespace Assets
{
    public class World : IDrawable
    {
        public bool IsSpecial { get; private set; } = true;
        public bool IsManipulator { get; private set; } = false;

        public World()
        {
            Position = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point());
        }

        public World(RawMatrix position)
        {
            Position = position;
        }

        public RawMatrix Position { get; set; }

        public RawMatrix PivotOffset { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// Contains polygon vertex count for drawing
        /// </summary>
        public int VertexCount { get; set; }

        public List<IDrawable> Objects { get; private set; } = new List<IDrawable>();
        public bool IsSelected { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float AngleX { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float AngleY { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float AngleZ { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleX { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleY { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleZ { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool ScaleMode { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IPickable Pickable { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void RegisterObject(IDrawable obj)
        {
            Objects.Add(obj);
            UpdateVertexCount();
        }

        public void UnregisterObject(IDrawable obj)
        {
            Objects.Remove(obj);
            UpdateVertexCount();
        }

        public VertexInterface[] Draw()
        {
            List<VertexInterface> vertices = new List<VertexInterface>();
            foreach(IDrawable obj in Objects)
            {
                vertices.AddRange(obj.Draw());
            }

            return vertices.ToArray();
        }

        public VertexInterface[] DrawPickable()
        {
            List<VertexInterface> vertices = new List<VertexInterface>();
            foreach (IDrawable obj in Objects)
            {
                vertices.AddRange(obj.DrawPickable());
            }

            return vertices.ToArray();
        }

        private void UpdateVertexCount()
        {
            VertexCount = Draw().Length / 6;
        }
    }

    public class Axis : APresentable
    {
        private readonly float _size, _rad;
        private readonly bool _flipped;
        private Cylinder cx, cy, cz;
        private Torus tx, ty, tz;

        public Axis(RawMatrix position, IDataProvider dataProvider, float size, float rad, bool flipped = false, bool nonAbsoulute = false) : base(position, dataProvider)
        {
            _size = size;
            _flipped = flipped;
            _rad = rad;
            Absoulute = !nonAbsoulute;
            CreateGeometryData();
        }

        protected override void CreateGeometryData()
        {
            if (Absoulute)
            {
                cx = new Cylinder(_size, _rad, MatrixHelpers.Create(Vector.YAxis.Flip(), Vector.XAxis, Vector.ZAxis, new Point(_size / 2.0f + Position.M41, Position.M42, Position.M43)), _dataProvider, new Point(1.0f, 0.0f, 0.0f), !_flipped, _flipped)
                {
                };
                cy = new Cylinder(_size, _rad, MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(Position.M41, _size / 2.0f + Position.M42, Position.M43)), _dataProvider, new Point(0.0f, 1.0f, 0.0f), !_flipped, _flipped)
                {
                };
                if (_flipped)
                    cz = new Cylinder(_size, _rad, MatrixHelpers.Create(Vector.XAxis, Vector.ZAxis.Flip(), Vector.YAxis, new Point(Position.M41, Position.M42, Position.M43 - _size / 2.0f)), _dataProvider, new Point(0.0f, 0.0f, 1.0f), false, true)
                    {
                    };
                else
                    cz = new Cylinder(_size, _rad, MatrixHelpers.Create(Vector.XAxis, Vector.ZAxis, Vector.YAxis.Flip(), new Point(Position.M41, Position.M42, _size / 2.0f + Position.M43)), _dataProvider, new Point(0.0f, 0.0f, 1.0f), true, false)
                    {

                    };

                if (_flipped)
                {
                    tx = new Torus(MatrixHelpers.Create(Vector.YAxis.Flip(), Vector.XAxis, Vector.ZAxis, new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(1.0f, 0.0f, 0.0f), true)
                    {
                    };

                    ty = new Torus(MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(0.0f, 1.0f, 0.0f), true)
                    {
                    };

                    tz = new Torus(MatrixHelpers.Create(Vector.XAxis, Vector.ZAxis.Flip(), Vector.YAxis, new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(0.0f, 0.0f, 1.0f), true)
                    {
                    };

                }
            }
            else
            {
                RawMatrix pivot = _dataProvider.SelectedObject.Position.Normalize();
                Vector xAxis = new Vector(pivot.M11, pivot.M12, pivot.M13);
                Vector yAxis = new Vector(pivot.M21, pivot.M22, pivot.M23);
                Vector zAxis = new Vector(pivot.M31, pivot.M32, pivot.M33);
                RawMatrix orthonormalX = MatrixHelpers.BuildOrthonormal(xAxis, false, true, false);
                RawMatrix orthonormalY = MatrixHelpers.BuildOrthonormal(yAxis, false, true, false);
                RawMatrix orthonormalZ = MatrixHelpers.BuildOrthonormal(zAxis, false, true, false);
                Point cxPos = new Point(Position.M41, Position.M42, Position.M43) + new Point(orthonormalX.M21 * (_size / 2.0f), orthonormalX.M22 * (_size / 2.0f), orthonormalX.M23 * (_size / 2.0f));
                Point cyPos = new Point(Position.M41, Position.M42, Position.M43) + new Point(orthonormalY.M21 * (_size / 2.0f), orthonormalY.M22 * (_size / 2.0f), orthonormalY.M23 * (_size / 2.0f));
                Point czPos = new Point(Position.M41, Position.M42, Position.M43) - new Point(orthonormalZ.M21 * (_size / 2.0f), orthonormalZ.M22 * (_size / 2.0f), orthonormalZ.M23 * (_size / 2.0f));
                cx = new Cylinder(_size, _rad, MatrixHelpers.Create(new Vector(orthonormalX.M11, orthonormalX.M12, orthonormalX.M13), new Vector(orthonormalX.M21, orthonormalX.M22, orthonormalX.M23), new Vector(orthonormalX.M31, orthonormalX.M32, orthonormalX.M33), cxPos), _dataProvider, new Point(1.0f, 0.0f, 0.0f), !_flipped, _flipped)
                {
                };
                cy = new Cylinder(_size, _rad, MatrixHelpers.Create(new Vector(orthonormalY.M11, orthonormalY.M12, orthonormalY.M13), new Vector(orthonormalY.M21, orthonormalY.M22, orthonormalY.M23), new Vector(orthonormalY.M31, orthonormalY.M32, orthonormalY.M33), cyPos), _dataProvider, new Point(0.0f, 1.0f, 0.0f), !_flipped, _flipped)
                {
                };
                cz = new Cylinder(_size, _rad, MatrixHelpers.Create(new Vector(orthonormalZ.M11, orthonormalZ.M12, orthonormalZ.M13).Flip(), new Vector(orthonormalZ.M21, orthonormalZ.M22, orthonormalZ.M23), new Vector(orthonormalZ.M31, orthonormalZ.M32, orthonormalZ.M33).Flip(), czPos), _dataProvider, new Point(0.0f, 0.0f, 1.0f), false, true)
                {
                };

                tx = new Torus(MatrixHelpers.Create(new Vector(orthonormalX.M11, orthonormalX.M12, orthonormalX.M13), new Vector(orthonormalX.M21, orthonormalX.M22, orthonormalX.M23), new Vector(orthonormalX.M31, orthonormalX.M32, orthonormalX.M33), new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(1.0f, 0.0f, 0.0f), true)
                {
                };
                ty = new Torus(MatrixHelpers.Create(new Vector(orthonormalY.M11, orthonormalY.M12, orthonormalY.M13), new Vector(orthonormalY.M21, orthonormalY.M22, orthonormalY.M23), new Vector(orthonormalY.M31, orthonormalY.M32, orthonormalY.M33), new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(0.0f, 1.0f, 0.0f), true)
                {
                };
                tz = new Torus(MatrixHelpers.Create(new Vector(orthonormalZ.M11, orthonormalZ.M12, orthonormalZ.M13).Flip(), new Vector(orthonormalZ.M21, orthonormalZ.M22, orthonormalZ.M23), new Vector(orthonormalZ.M31, orthonormalZ.M32, orthonormalZ.M33).Flip(), new Point(Position.M41, Position.M42, Position.M43)), _size / 2, _rad * 5, _dataProvider, new Point(0.0f, 0.0f, 1.0f), true)
                {
                };
            }
        }

        public void UnRegister()
        {
            _dataProvider.UnRegisterObject(cx);
            _dataProvider.UnRegisterObject(cy);
            _dataProvider.UnRegisterObject(cz);
            _dataProvider.UnRegisterObject(tx);
            _dataProvider.UnRegisterObject(ty);
            _dataProvider.UnRegisterObject(tz);
        }

        public override bool IsSelected => cx.IsSelected || cy.IsSelected || cz.IsSelected || tx.IsSelected || ty.IsSelected || tz.IsSelected;

        /// <summary>
        /// True if it shows manipulator oriented towards world's X, Y and Z
        /// </summary>
        public bool Absoulute { get; set; } = true;

        public void Manipulate(Vector delta)
        {
            RawMatrix m = new RawMatrix();
            float deg=0.0f;
            Vector axis = new Vector();
            if (cx.IsSelected)
            {
                RawMatrix pos = cx.Position;
                Vector shift = new Vector(pos.M21, pos.M22, pos.M23); // local Y axis
                float translate = delta.Dot(shift) / 100.0f;
                Vector trans = new Vector(shift.X * translate, shift.Y * translate, shift.Z * translate);

                if (!_dataProvider.SelectedObject.ScaleMode)
                    m = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(trans.X, trans.Y, trans.Z));
                else
                {
                    IDrawable obj = _dataProvider.SelectedObject;
                    float scaleX = new Vector(obj.Position.M11, obj.Position.M12, obj.Position.M13).Magnitude();
                    float diff = (scaleX + translate) / scaleX;
                    obj.Position = MatrixHelpers.ScaleAbout(obj.Position, obj.Position, new Vector(diff, 1, 1));
                    //Vector xScale = new Vector(obj.ScaleMatrix.M11 + translate, obj.ScaleMatrix.M12, obj.ScaleMatrix.M13);
                    //Vector yScale = new Vector(obj.ScaleMatrix.M21, obj.ScaleMatrix.M22, obj.ScaleMatrix.M23);
                    //Vector zScale = new Vector(obj.ScaleMatrix.M31, obj.ScaleMatrix.M32, obj.ScaleMatrix.M33);
                    //obj.ScaleMatrix = MatrixHelpers.Create(xScale, yScale, zScale, new Point());

                    //Vector xScale1 = new Vector(cx.ScaleMatrix.M11, cx.ScaleMatrix.M12, cx.ScaleMatrix.M13);
                    //Vector yScale1 = new Vector(cx.ScaleMatrix.M21, cx.ScaleMatrix.M22 + translate, cx.ScaleMatrix.M23);
                    //Vector zScale1 = new Vector(cx.ScaleMatrix.M31, cx.ScaleMatrix.M32, cx.ScaleMatrix.M33);
                    //cx.ScaleMatrix = MatrixHelpers.Create(xScale1, yScale1, zScale1, new Point());

                    //Vector xScale2 = new Vector(ty.ScaleMatrix.M11 + translate, ty.ScaleMatrix.M12, ty.ScaleMatrix.M13);
                    //Vector yScale2 = new Vector(ty.ScaleMatrix.M21, ty.ScaleMatrix.M22, ty.ScaleMatrix.M23);
                    //Vector zScale2 = new Vector(ty.ScaleMatrix.M31, ty.ScaleMatrix.M32, ty.ScaleMatrix.M33 + translate);
                    //ty.ScaleMatrix = MatrixHelpers.Create(xScale2, yScale2, zScale2, new Point());

                    //Vector xScale3 = new Vector(tz.ScaleMatrix.M11 + translate, tz.ScaleMatrix.M12, tz.ScaleMatrix.M13);
                    //Vector yScale3 = new Vector(tz.ScaleMatrix.M21, tz.ScaleMatrix.M22, tz.ScaleMatrix.M23);
                    //Vector zScale3 = new Vector(tz.ScaleMatrix.M31, tz.ScaleMatrix.M32, tz.ScaleMatrix.M33 + translate);
                    //tz.ScaleMatrix = MatrixHelpers.Create(xScale3, yScale3, zScale3, new Point());
                }
            }
            else if(cy.IsSelected)
            {
                RawMatrix pos = cy.Position;
                Vector shift = new Vector(pos.M21, pos.M22, pos.M23); // local Y axis
                float translate = delta.Dot(shift) / 100f;
                Vector trans = new Vector(shift.X * translate, shift.Y * translate, shift.Z * translate);

                if(!_dataProvider.SelectedObject.ScaleMode)
                    m = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(trans.X, trans.Y, trans.Z));
                else
                {
                    IDrawable obj = _dataProvider.SelectedObject;
                    float scaleY = new Vector(obj.Position.M21, obj.Position.M22, obj.Position.M23).Magnitude();
                    float diff = (scaleY + translate) / scaleY;
                    obj.Position = MatrixHelpers.ScaleAbout(obj.Position, obj.Position, new Vector(1, diff, 1));
                    //Vector xScale = new Vector(obj.ScaleMatrix.M11, obj.ScaleMatrix.M12, obj.ScaleMatrix.M13);
                    //Vector yScale = new Vector(obj.ScaleMatrix.M21, obj.ScaleMatrix.M22 + translate, obj.ScaleMatrix.M23);
                    //Vector zScale = new Vector(obj.ScaleMatrix.M31, obj.ScaleMatrix.M32, obj.ScaleMatrix.M33);
                    //obj.ScaleMatrix = MatrixHelpers.Create(xScale, yScale, zScale, new Point());

                    //Vector xScale1 = new Vector(cy.ScaleMatrix.M11, cy.ScaleMatrix.M12, cy.ScaleMatrix.M13);
                    //Vector yScale1 = new Vector(cy.ScaleMatrix.M21, cy.ScaleMatrix.M22 + translate, cy.ScaleMatrix.M23);
                    //Vector zScale1 = new Vector(cy.ScaleMatrix.M31, cy.ScaleMatrix.M32, cy.ScaleMatrix.M33);
                    //cy.ScaleMatrix = MatrixHelpers.Create(xScale1, yScale1, zScale1, new Point());

                    //Vector xScale2 = new Vector(tx.ScaleMatrix.M11 + translate, tx.ScaleMatrix.M12, tx.ScaleMatrix.M13);
                    //Vector yScale2 = new Vector(tx.ScaleMatrix.M21, tx.ScaleMatrix.M22, tx.ScaleMatrix.M23);
                    //Vector zScale2 = new Vector(tx.ScaleMatrix.M31, tx.ScaleMatrix.M32, tx.ScaleMatrix.M33 + translate);
                    //tx.ScaleMatrix = MatrixHelpers.Create(xScale2, yScale2, zScale2, new Point());

                    //Vector xScale3 = new Vector(tz.ScaleMatrix.M11 + translate, tz.ScaleMatrix.M12, tz.ScaleMatrix.M13);
                    //Vector yScale3 = new Vector(tz.ScaleMatrix.M21, tz.ScaleMatrix.M22, tz.ScaleMatrix.M23);
                    //Vector zScale3 = new Vector(tz.ScaleMatrix.M31, tz.ScaleMatrix.M32, tz.ScaleMatrix.M33 + translate);
                    //tz.ScaleMatrix = MatrixHelpers.Create(xScale3, yScale3, zScale3, new Point());
                }
            }
            else if(cz.IsSelected)
            {
                RawMatrix pos = cz.Position;
                Vector shift = new Vector(pos.M21, pos.M22, pos.M23); // local Y axis
                float del = 0.0f;
                if (delta.X < 0 && delta.Y < 0)
                {
                    del = delta.Magnitude() / 100.0f;
                }
                else if (delta.X < 0 && delta.Y > 0)
                {
                    del = -(delta.Magnitude() / 200.0f);
                }
                else if(delta.X > 0 && delta.Y < 0)
                {
                    del = delta.Magnitude() / 200.0f;
                }
                else if (delta.X > 0 && delta.Y > 0)
                {
                    del = -(delta.Magnitude() / 100.0f);
                }
                float translate = del;
                Vector trans = new Vector(shift.X * translate, shift.Y * translate, shift.Z * translate);

                if(!_dataProvider.SelectedObject.ScaleMode)
                    m = MatrixHelpers.Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point(trans.X, trans.Y, trans.Z));
                else
                {
                    IDrawable obj = _dataProvider.SelectedObject;
                    float scaleZ = new Vector(obj.Position.M31, obj.Position.M32, obj.Position.M33).Magnitude();
                    float diff = (scaleZ + translate) / scaleZ;
                    obj.Position = MatrixHelpers.ScaleAbout(obj.Position, obj.Position, new Vector(1, 1, diff));
                    //Vector xScale = new Vector(obj.ScaleMatrix.M11, obj.ScaleMatrix.M12, obj.ScaleMatrix.M13);
                    //Vector yScale = new Vector(obj.ScaleMatrix.M21, obj.ScaleMatrix.M22, obj.ScaleMatrix.M23);
                    //Vector zScale = new Vector(obj.ScaleMatrix.M31, obj.ScaleMatrix.M32, obj.ScaleMatrix.M33 + translate);
                    //obj.ScaleMatrix = MatrixHelpers.Create(xScale, yScale, zScale, new Point());

                    //Vector xScale1 = new Vector(cz.ScaleMatrix.M11, cz.ScaleMatrix.M12, cz.ScaleMatrix.M13);
                    //Vector yScale1 = new Vector(cz.ScaleMatrix.M21, cz.ScaleMatrix.M22 + translate, cz.ScaleMatrix.M23);
                    //Vector zScale1 = new Vector(cz.ScaleMatrix.M31, cz.ScaleMatrix.M32, cz.ScaleMatrix.M33);
                    //cz.ScaleMatrix = MatrixHelpers.Create(xScale1, yScale1, zScale1, new Point());

                    //Vector xScale2 = new Vector(tx.ScaleMatrix.M11 + translate, tx.ScaleMatrix.M12, tx.ScaleMatrix.M13);
                    //Vector yScale2 = new Vector(tx.ScaleMatrix.M21, tx.ScaleMatrix.M22, tx.ScaleMatrix.M23);
                    //Vector zScale2 = new Vector(tx.ScaleMatrix.M31, tx.ScaleMatrix.M32, tx.ScaleMatrix.M33 + translate);
                    //tx.ScaleMatrix = MatrixHelpers.Create(xScale2, yScale2, zScale2, new Point());

                    //Vector xScale3 = new Vector(ty.ScaleMatrix.M11 + translate, ty.ScaleMatrix.M12, ty.ScaleMatrix.M13);
                    //Vector yScale3 = new Vector(ty.ScaleMatrix.M21, ty.ScaleMatrix.M22, ty.ScaleMatrix.M23);
                    //Vector zScale3 = new Vector(ty.ScaleMatrix.M31, ty.ScaleMatrix.M32, ty.ScaleMatrix.M33 + translate);
                    //ty.ScaleMatrix = MatrixHelpers.Create(xScale3, yScale3, zScale3, new Point());
                }
            }
            if(tx.IsSelected)
            {
                axis = new Vector(tx.Position.M21, tx.Position.M22, tx.Position.M23); 
                Vector vect = delta.Cross(axis);
                if (vect.Z == 0.0f)
                    return;
                float sign = vect.Z / Math.Abs(vect.Z);
                deg = (delta.Magnitude() / tx.Radius) * -sign / 10.0f;
                _dataProvider.SelectedObject.AngleX += deg;
                //m = MatrixHelpers.CreateRotationMatrix(deg, axis);
            }
            else if(ty.IsSelected)
            {
                axis = new Vector(ty.Position.M21, ty.Position.M22, ty.Position.M23);
                Vector vect = delta.Cross(axis);
                if (vect.Z == 0.0f)
                    return;
                float sign = vect.Z / Math.Abs(vect.Z);
                deg = (delta.Magnitude() / ty.Radius) * -sign / 10.0f;
                _dataProvider.SelectedObject.AngleY += deg;
                //m = MatrixHelpers.CreateRotationMatrix(deg, axis);

            }
            else if(tz.IsSelected)
            {
                axis = new Vector(tz.Position.M21, tz.Position.M22, tz.Position.M23);
                float del = 0.0f;
                if (delta.X < 0 && delta.Y < 0)
                {
                    del = delta.Magnitude() / 100.0f;
                }
                else if (delta.X < 0 && delta.Y > 0)
                {
                    del = -(delta.Magnitude() / 200.0f);
                }
                else if (delta.X > 0 && delta.Y < 0)
                {
                    del = delta.Magnitude() / 200.0f;
                }
                else if (delta.X > 0 && delta.Y > 0)
                {
                    del = -(delta.Magnitude() / 100.0f);
                }
                deg = (del / tz.Radius) * 10.0f;
                _dataProvider.SelectedObject.AngleZ += deg;
                //m = MatrixHelpers.CreateRotationMatrix(deg, axis);
            }

            if (!_dataProvider.SelectedObject.ScaleMode)
            {
                if (cx.IsSelected || cy.IsSelected || cz.IsSelected)
                {
                    cx.Position = cx.Position.Multiply(m);
                    cy.Position = cy.Position.Multiply(m);
                    cz.Position = cz.Position.Multiply(m);
                    tx.Position = tx.Position.Multiply(m);
                    ty.Position = ty.Position.Multiply(m);
                    tz.Position = tz.Position.Multiply(m);
                    _dataProvider.SelectedObject.Position = _dataProvider.SelectedObject.Position.Multiply(m);
                }
                else
                {
                    Point pos = new Point(_dataProvider.SelectedObject.Position.M41, _dataProvider.SelectedObject.Position.M42, _dataProvider.SelectedObject.Position.M43);
                    _dataProvider.SelectedObject.Position = MatrixHelpers.RotateAbout(pos, _dataProvider.SelectedObject.Position, deg, axis);
                }
            }
        }

        public override VertexInterface[] Draw()
        {
            throw new NotImplementedException();
        }

        public override VertexInterface[] DrawPickable()
        {
            throw new NotImplementedException();
        }
    }
}
