﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using System.Windows.Threading;
using System.Timers;
using Assets.Materials;

namespace Assets
{
    public class Cube : APresentable, IPositionable
    {

        private readonly float _sizeMult;
        public Point Colour { get; set; }

        public Cube(IDataProvider dataProvider, Point colour, float sizeMultiplier = 1.0f) : base(MatrixHelpers.Identity, dataProvider)
        {
            Colour = colour;
            _sizeMult = sizeMultiplier;
            CreateGeometryData();

            _dataProvider.RegisterObject(this);
        }

        public Cube(RawMatrix position, IDataProvider dataProvider, Point colour, float sizeMultiplier = 1.0f) : base(position, dataProvider)
        {
            Colour = colour;
            _sizeMult = sizeMultiplier;
            CreateGeometryData();

            _dataProvider.RegisterObject(this);
        }

        protected override void CreateGeometryData()
        {
            Point[] geometryData = new[]
            {
                new Point(-0.5f * _sizeMult, 0.5f * _sizeMult, -0.5f* _sizeMult),
                new Point(0.5f * _sizeMult, 0.5f * _sizeMult, -0.5f * _sizeMult),
                new Point(0.5f* _sizeMult, -0.5f* _sizeMult, -0.5f * _sizeMult),
                new Point(-0.5f * _sizeMult, -0.5f * _sizeMult, -0.5f* _sizeMult),
                new Point(-0.5f * _sizeMult, 0.5f* _sizeMult, 0.5f * _sizeMult),
                new Point(0.5f * _sizeMult, 0.5f * _sizeMult, 0.5f * _sizeMult),
                new Point(0.5f * _sizeMult, -0.5f * _sizeMult, 0.5f * _sizeMult),
                new Point(-0.5f * _sizeMult, -0.5f * _sizeMult, 0.5f * _sizeMult)
            };

            _vertexInfo = new VertexInfo(geometryData.ToList());
        }

        public override VertexInterface[] Draw()
        {
            if (_cachedVertexInterface == null)
            {
                Point[] geometryData = _vertexInfo.GeometryData.ToArray();
                //WorldTransform();
                Point[] pts = new[]
                {
                                      geometryData[0], Colour,
                                      geometryData[1], Colour,
                                      geometryData[2], Colour,
                                      geometryData[2], Colour,
                                      geometryData[3], Colour,
                                      geometryData[0], Colour,
                                      geometryData[1], Colour,
                                      geometryData[5], Colour,
                                      geometryData[6], Colour,
                                      geometryData[6], Colour,
                                      geometryData[2], Colour,
                                      geometryData[1], Colour,
                                      geometryData[5], Colour,
                                      geometryData[4], Colour,
                                      geometryData[7], Colour,
                                      geometryData[7], Colour,
                                      geometryData[6], Colour,
                                      geometryData[5], Colour,
                                      geometryData[4], Colour,
                                      geometryData[0], Colour,
                                      geometryData[3], Colour,
                                      geometryData[3], Colour,
                                      geometryData[7], Colour,
                                      geometryData[4], Colour,
                                      geometryData[4], Colour,
                                      geometryData[5], Colour,
                                      geometryData[1], Colour,
                                      geometryData[1], Colour,
                                      geometryData[0], Colour,
                                      geometryData[4], Colour,
                                      geometryData[3], Colour,
                                      geometryData[2], Colour,
                                      geometryData[6], Colour,
                                      geometryData[6], Colour,
                                      geometryData[7], Colour,
                                      geometryData[3], Colour
           };

                CalculateTangentSpace(pts);

                List<VertexInterface> vts = new List<VertexInterface>();
                List<VertexInterface> vtsp = new List<VertexInterface>();
                for (int i = 0; i < pts.Length; i += 2)
                {
                    Point p = pts[i];
                    vts.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    int index = _vertexInfo.GeometryData.IndexOf(p);
                    //Vector v = _vertexInfo.VertexNormals[index];
                    //vts.Add(new VertexInterface { X = v.X, Y = v.Y, Z = v.Z, W = v.W });
                    p = pts[i + 1];
                    vts.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = Pickable.PickColour.X, Y = Pickable.PickColour.Y, Z = Pickable.PickColour.Z, W = Pickable.PickColour.W });
                    RawVector2 tex = _vertexInfo.TexCoords[i/2];
                    vts.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    vtsp.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    RawMatrix ts = _vertexInfo.TangentSpace[index];
                    vts.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vts.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vts.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });
                    vtsp.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vtsp.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vtsp.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });

                }

                _cachedVertexInterface = vts.ToArray();
                _cachedVertexInterfaceForPickable = vtsp.ToArray();
            }
            return _cachedVertexInterface;
        }

        public override VertexInterface[] DrawPickable()
        {
            if (_cachedVertexInterface == null)
                Draw();

            return _cachedVertexInterfaceForPickable;
        }
    }
}
