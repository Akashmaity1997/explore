﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Assets.Utilities
{
    public class EnumStringAttribute : Attribute
    {
        public EnumStringAttribute(string value)
        {
            Value = value;
        }
        public string Value { get; set; }
    }

    public static class EnumToString
    {
        public static string GetStringValue(Enum value)
        {
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            EnumStringAttribute[] attrs = fi.GetCustomAttributes(typeof(EnumStringAttribute), false) as EnumStringAttribute[];
            if (attrs.Length > 0)
            {
                return attrs[0].Value;
            }
            return null;
        }

        public static T GetEnum<T>(this string enumName)
        {
            return (T)Enum.Parse(typeof(T), enumName, true);
        }
    }
}
