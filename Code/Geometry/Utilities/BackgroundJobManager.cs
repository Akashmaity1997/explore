﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using System.Threading;
using System.Windows;
using System.Reflection;
using System.Windows.Controls;
using AvalonDock;

namespace Assets.Utilities
{
    public delegate void VoidDelegate();

    public class BackgroundJobManager : IBackgroundJobManager
    {
        public bool JobActive {get; private set;}

        public void RunBackgroundJob<T>(DoJobDelegate<T> doIt, SucceededJobDelegate<T> succeeded, FailedJobDelegate failed, CancelledJobDelegate cancelled)
        {
            if (JobActive)
                throw new Exception("A background job is already running");

            JobActive = true;
            LockUI();

            Thread jobThread = new Thread(() =>
            {
                T result = default(T);
                try
                {
                    result = doIt();
                }
                catch (ThreadAbortException)
                {
                    if (cancelled != null)
                        Application.Current.Dispatcher.BeginInvoke(cancelled);
                }
                catch (Exception ex)
                {
                    if (failed == null)
                        throw new Exception($"Exception at Thread {Thread.CurrentThread.Name} from {ex.TargetSite.Name} call");

                    else
                        Application.Current.Dispatcher.BeginInvoke(failed, ex);
                }
                finally
                {
                    if (succeeded != null)
                        Application.Current.Dispatcher.BeginInvoke(succeeded, result);

                    JobActive = false;
                    Application.Current.Dispatcher.BeginInvoke((VoidDelegate)UnlockUI);
                }
            });
            jobThread.Name = "BackgroundJobThread";
            jobThread.IsBackground = true;
            jobThread.SetApartmentState(ApartmentState.MTA);
            jobThread.Start();
        }

        public void RunBackgroundJob(DoJobDelegate doIt, SucceededJobDelegate succeeded, FailedJobDelegate failed, CancelledJobDelegate cancelled)
        {
            if (JobActive)
                throw new Exception("A background job is already running");

            JobActive = true;
            LockUI();

            Thread jobThread = new Thread(() => 
            {
                try
                {
                    doIt();
                }
                catch(ThreadAbortException)
                {
                    if(cancelled != null)
                        Application.Current.Dispatcher.BeginInvoke(cancelled);
                }
                catch (Exception ex)
                {
                    if (failed == null)
                        throw new Exception($"Exception at Thread {Thread.CurrentThread.Name} from {ex.TargetSite.Name} call");

                    else
                        Application.Current.Dispatcher.BeginInvoke(failed, ex);
                }
                finally
                {
                    if(succeeded != null)
                        Application.Current.Dispatcher.BeginInvoke(succeeded);

                    JobActive = false;
                    Application.Current.Dispatcher.BeginInvoke((VoidDelegate)UnlockUI);
                }
            });
            jobThread.Name = "BackgroundJobThread";
            jobThread.IsBackground = true;
            jobThread.SetApartmentState(ApartmentState.MTA);
            jobThread.Start();
        }

        private void LockUI()
        {
            Window window = Application.Current.MainWindow;
            DocumentPane mainDocument = DependencyObjectHelper.FindChild<DocumentPane>(window, "MainDocument");
            DockablePane tabPane = DependencyObjectHelper.FindChild<DockablePane>(window, "TabPane");
            mainDocument.IsEnabled = false;
            tabPane.IsEnabled = false;
        }

        private void UnlockUI()
        {
            Window window = Application.Current.MainWindow;
            DocumentPane mainDocument = DependencyObjectHelper.FindChild<DocumentPane>(window, "MainDocument");
            DockablePane tabPane = DependencyObjectHelper.FindChild<DockablePane>(window, "TabPane");
            mainDocument.IsEnabled = true;
            tabPane.IsEnabled = true;
        }
    }
}
