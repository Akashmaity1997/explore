﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Utilities
{
    public enum LightType
    {
        [EnumString("Point")]
        Point,
        [EnumString("Directional")]
        Directional,
        [EnumString("Spot")]
        Spot
    }

    public enum AreaType
    {
        [EnumString("Circular")]
        Circular
    }

    public enum TextureMappingType
    {
        [EnumString("UV")]
        UV,
        [EnumString("Cubic")]
        Cubic,
        [EnumString("Cylindrical")]
        Cylindrical
    }
}
