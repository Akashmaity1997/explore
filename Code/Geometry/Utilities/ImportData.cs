﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using System.IO;
using IxMilia.Stl;
using System.Diagnostics;

namespace Assets.Utilities
{
    public class ImportData
    {
        private readonly string _filePath;
        private readonly IBackgroundJobManager _backgroundJobManager;

        public string Name => Path.GetFileNameWithoutExtension(_filePath);

        public ImportData(string filePath, IDataProvider dataProvider, IBackgroundJobManager backgroundJobManager)
        {
            _filePath = filePath;
            _backgroundJobManager = backgroundJobManager;

            //Thread bThread = new Thread(() =>
            //{
            //    CreateGeometryData();
            //    _dataProvider.RegisterObject(this);
            //});
            //bThread.Start();

            //_backgroundJobManager.RunBackgroundJob(() =>
            //{
            //    CreateGeometryData();
            //    Draw();
            //},
            //() =>
            //{
            //    _dataProvider.RegisterObject(this);
            //});
        }

        public Mesh Import()
        {
            Mesh importMesh = new Mesh();
            if (Path.GetExtension(_filePath).ToLowerInvariant().Equals(".stl"))
            {
                _backgroundJobManager.RunBackgroundJob(() =>
                {
                    using (FileStream fs = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
                    {
                        StlFile stl = StlFile.Load(fs);
                        int count = -1;
                        
                        foreach (StlTriangle facet in stl.Triangles)
                        {
                            importMesh.Vertices.Add(new RawVector3(facet.Vertex1.X, facet.Vertex1.Y, facet.Vertex1.Z));
                            importMesh.Vertices.Add(new RawVector3(facet.Vertex2.X, facet.Vertex2.Y, facet.Vertex2.Z));
                            importMesh.Vertices.Add(new RawVector3(facet.Vertex3.X, facet.Vertex3.Y, facet.Vertex3.Z));
                            importMesh.Faces.Add((++count, ++count, ++count));
                        }

                        importMesh.GenerateUVs();
                        importMesh.GenerateNormals();
                        importMesh.GenerateTangents();
                        return importMesh;
                    }
                }, (mesh) =>
                {
                    Debug.Assert(mesh.Vertices.Any());
                    Debug.Assert(mesh.Faces.Any());
                });
            }

            return importMesh;
        }
    }
}
