﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Assets.Interfaces
{
    public interface IWorldDm : IPositionableDm
    {
        ObservableCollection<IModelDm> Models { get; }
    }
}
