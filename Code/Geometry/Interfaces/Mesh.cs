﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;
using System.Diagnostics;

namespace Assets.Interfaces
{
    public class Mesh
    {
        private BoundingBox _axisAlignedBoundingBox = new BoundingBox();

        public List<RawVector3> Vertices { get; } = new List<RawVector3>();

        public List<(int, int, int)> Faces { get; } = new List<(int, int, int)>();

        public List<RawVector2> UVs { get; private set; } = new List<RawVector2>();

        public List<RawVector3> Normals { get; private set; } = new List<RawVector3>();

        public List<RawVector3> Tangents { get; private set; } = new List<RawVector3>();

        public void UpdateBoundingBox()
        {
            Debug.Assert(Vertices.Any(), "Vertices are empty!");

            _axisAlignedBoundingBox.Assimilate(Vertices);
        }

        public void GenerateNormals()
        {
            Normals = new List<RawVector3>(new RawVector3[Vertices.Count]);
            List<Vector> normals = new List<Vector>(new Vector[Vertices.Count]);
            for (int i = 0; i < Faces.Count; i++)
            {
                int index1 = Faces[i].Item1;
                int index2 = Faces[i].Item2;
                int index3 = Faces[i].Item3;
                Point p1 = Vertices[index1].ToPoint();
                Point p2 = Vertices[index2].ToPoint();
                Point p3 = Vertices[index3].ToPoint();

                Vector v1 = new Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
                Vector v2 = new Vector(p3.X - p1.X, p3.Y - p1.Y, p3.Z - p1.Z);
                Vector triNormal = v1.Cross(v2).Normalize();
                Vector n1 = normals[index1] ?? new Vector();
                Vector n2 = normals[index2] ?? new Vector();
                Vector n3 = normals[index3] ?? new Vector();
                normals[index1] = n1 + triNormal;
                normals[index2] = n2 + triNormal;
                normals[index3] = n3 + triNormal;
            }

            for (int i = 0; i < Normals.Count; ++i)
            {
                Normals[i] = normals[i].Normalize().ToVector3();
            }
        }

        public void GenerateUVs()
        {
            UVs = new List<RawVector2>(new RawVector2[Vertices.Count]);
            for (int i = 0; i < Faces.Count ; i++)
            {
                int index1 = Faces[i].Item1;
                int index2 = Faces[i].Item2;
                int index3 = Faces[i].Item3;
                Point p1 = Vertices[index1].ToPoint();
                Point p2 = Vertices[index2].ToPoint();
                Point p3 = Vertices[index3].ToPoint();

                Vector v1 = new Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
                Vector v2 = new Vector(p3.X - p1.X, p3.Y - p1.Y, p3.Z - p1.Z);
                Vector triNormal = v1.Cross(v2).Normalize();

                //if (TextureMapping == TextureMappingType.UV)
                {
                    float xWeight = triNormal.X / (triNormal.X + triNormal.Y + triNormal.Z);
                    float yWeight = triNormal.Y / (triNormal.X + triNormal.Y + triNormal.Z);
                    float zWeight = triNormal.Z / (triNormal.X + triNormal.Y + triNormal.Z);

                    float p1u = xWeight * p1.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p1.X / _axisAlignedBoundingBox.Size.X + zWeight * p1.X / _axisAlignedBoundingBox.Size.X;
                    float p1v = xWeight * p1.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p1.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p1.Y / _axisAlignedBoundingBox.Size.Y;
                    float p2u = xWeight * p2.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p2.X / _axisAlignedBoundingBox.Size.X + zWeight * p2.X / _axisAlignedBoundingBox.Size.X;
                    float p2v = xWeight * p2.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p2.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p2.Y / _axisAlignedBoundingBox.Size.Y;
                    float p3u = xWeight * p3.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p3.X / _axisAlignedBoundingBox.Size.X + zWeight * p3.X / _axisAlignedBoundingBox.Size.X;
                    float p3v = xWeight * p3.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p3.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p3.Y / _axisAlignedBoundingBox.Size.Y;

                    //if (i % 2 == 0)
                    {
                        UVs[index1] = new RawVector2(p1u, p1v);
                        UVs[index2] = new RawVector2(p2u, p2v);
                        UVs[index3] = new RawVector2(p3u, p3v);
                    }
                    //else
                    //{
                    //    _vertexInfo.TexCoords[(i * 3)] = new RawVector2(1, 1);
                    //    _vertexInfo.TexCoords[(i * 3 + 1)] = new RawVector2(0, 1);
                    //    _vertexInfo.TexCoords[(i * 3 + 2)] = new RawVector2(0, 0);
                    //}
                }
                //else if (TextureMapping == TextureMappingType.Cylindrical)
                //{
                //    float v = 1.0f - (p1.Y - yMin) / yRange;
                //    float u = (float)((1 / (2 * Math.PI)) * Math.Acos(p1.X / xMax));
                //    _vertexInfo.TexCoords[(i * 3)] = new RawVector2(u, v);
                //    v = 1.0f - (p2.Y - yMin) / yRange;
                //    u = (float)((1 / (2 * Math.PI)) * Math.Acos(p2.X / xMax));
                //    _vertexInfo.TexCoords[(i * 3 + 1)] = new RawVector2(u, v);
                //    v = 1.0f - (p3.Y - yMin) / yRange;
                //    u = (float)((1 / (2 * Math.PI)) * Math.Acos(p3.X / xMax));
                //    _vertexInfo.TexCoords[(i * 3 + 2)] = new RawVector2(u, v);
                //}
            }
        }

        public void GenerateTangents()
        {
            List<RawMatrix> tangentSpaces = new List<RawMatrix>(new RawMatrix[Vertices.Count]);
            Tangents = new List<RawVector3>(new RawVector3[Vertices.Count]);
            for (int i = 0; i < Faces.Count; i++)
            {
                int index1 = Faces[i].Item1;
                int index2 = Faces[i].Item2;
                int index3 = Faces[i].Item3;
                Point p1 = Vertices[index1].ToPoint();
                Point p2 = Vertices[index2].ToPoint();
                Point p3 = Vertices[index3].ToPoint();

                Vector v1 = new Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
                Vector v2 = new Vector(p3.X - p1.X, p3.Y - p1.Y, p3.Z - p1.Z);
                Vector triNormal = v1.Cross(v2).Normalize();
                float s1 = UVs[index2].X - UVs[index1].X;
                float s2 = UVs[index3].X - UVs[index1].X;
                float t1 = UVs[index2].Y - UVs[index1].Y;
                float t2 = UVs[index3].Y - UVs[index1].Y;

                float r = 1.0f / (s1 * t2 - s2 * t1);

                //_vertexInfo.VertexNormals[index1] += triNormal;
                //_vertexInfo.VertexNormals[index2] += triNormal;
                //_vertexInfo.VertexNormals[index3] += triNormal;
                Vector Bnon = new Vector((s1 * (p3.X - p1.X) - s2 * (p2.X - p1.X)) * r, (s1 * (p3.Y - p1.Y) - s2 * (p2.Y - p1.Y)) * r, (s1 * (p3.Z - p1.Z) - s2 * (p2.Z - p1.Z)) * r);
                Vector N = triNormal; // normal
                Vector B = Bnon.Normalize(); // Bi-tangent
                Vector T = B.Cross(N).Normalize();// Tangent
                RawMatrix tangentSpace = MatrixHelpers.Create(T, B, N, new Point());
                tangentSpaces[index1] = tangentSpaces[index1].Add(tangentSpace);
                tangentSpaces[index2] = tangentSpaces[index2].Add(tangentSpace);
                tangentSpaces[index3] = tangentSpaces[index3].Add(tangentSpace);
            }

            for (int i = 0; i < tangentSpaces.Count; ++i)
            {
                //Vector nnormal = _vertexInfo.VertexNormals[i].Normalize();
                //_vertexInfo.VertexNormals[i] = nnormal;

                RawMatrix ts = tangentSpaces[i];
                Vector T = new Vector(ts.M11, ts.M12, ts.M13).Normalize();
                //Vector B = new Vector(ts.M21, ts.M22, ts.M23).Normalize();
                //Vector N = new Vector(ts.M31, ts.M32, ts.M33).Normalize();

                //Normals[i] = N.ToVector3();
                Tangents[i] = T.ToVector3();
            }
        }

        public Mesh Combine(Mesh mesh2)
        {
            Mesh mesh = new Mesh();
            mesh.Vertices.AddRange(Vertices);
            mesh.Faces.AddRange(Faces);
            mesh.UVs.AddRange(UVs);
            mesh.Normals.AddRange(Normals);
            mesh.Tangents.AddRange(Tangents);

            mesh.Vertices.AddRange(mesh2.Vertices);
            mesh.UVs.AddRange(mesh2.UVs);
            mesh.Normals.AddRange(mesh2.Normals);
            mesh.Tangents.AddRange(mesh2.Tangents);

            foreach((int, int, int) face in mesh2.Faces)
            {
                mesh.Faces.Add((face.Item1 + Vertices.Count, face.Item2 + Vertices.Count, face.Item3 + Vertices.Count));
            }

            return mesh;
        }

        public void Transform(RawMatrix position)
        {
            List<RawVector3> vertices = new List<RawVector3>();
            List<RawVector3> normals = new List<RawVector3>();
            List<RawVector3> tangents = new List<RawVector3>();
            foreach (RawVector3 vertex in Vertices)
            {
                vertices.Add((new Point(vertex) * position).ToVector3());
            }
            Vertices.Clear();
            Vertices.AddRange(vertices);
            foreach (RawVector3 normal in Normals)
            {
                normals.Add((new Vector(normal) * position).ToVector3());
            }
            Normals.Clear();
            Normals.AddRange(normals);
            foreach (RawVector3 tangent in Tangents)
            {
                tangents.Add((new Vector(tangent) * position).ToVector3());
            }
            Tangents.Clear();
            Tangents.AddRange(tangents);
        }
    }
}
