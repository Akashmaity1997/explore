﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;

namespace Assets.Interfaces
{
    public interface IPositionableDm : IBaseDm
    {
        RawMatrix Position { get; set; }

        /// <summary>
        /// This defines the centre of mesh relative to <see cref="Position">
        /// </summary>
        RawMatrix PivotOffset { get; set; }
    }
}
