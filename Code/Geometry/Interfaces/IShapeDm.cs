﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Interfaces
{
    public interface IShapeDm : IParentableDm, IPositionableDm
    {
        Mesh Mesh { get; }

        IEnumerable<IShapeDm> GetSelfAndDescendants();
    }
}
