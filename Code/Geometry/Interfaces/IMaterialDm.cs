﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;

namespace Assets.Interfaces
{
    public class MaterialDefinition : IDisposable
    {
        public RawColor3 Diffuse { get; set; } = new RawColor3(1, 1, 1);

        public float Specular { get; set; }

        public RawColor3 SpecularTint { get; set; }

        public float Metalness { get; set; } = 0.8f;

        public float Roughness { get; set; } = 0.2f;

        public RawColor3 Fresnel { get; set; } = new RawColor3(0.24f, 0.24f, 0.24f);

        public virtual void Dispose()
        {

        }
    }

    public interface IMaterialDm : IBaseDm
    {
        MaterialDefinition Definition { get; set; }

        ITextureDm DiffuseTexture { get; set; }
    }
}
