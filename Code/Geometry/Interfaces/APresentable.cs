﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Assets.Materials;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Assets.Utilities;

namespace Assets.Interfaces
{
    public abstract class APresentable : IDrawable, INotifyPropertyChanged
    {
        protected readonly IDataProvider _dataProvider;

        protected VertexInfo _vertexInfo;
        protected BoundingBox _axisAlignedBoundingBox;
        protected VertexInterface[] _cachedVertexInterface;
        protected VertexInterface[] _cachedVertexInterfaceForPickable;
        public int VertexCount => _vertexInfo?.GeometryData?.Count ?? 0;

        public bool IsSpecial { get; set; }

        public bool IsManipulator { get; set; }

        public ObservableCollection<Decal> Decals { get; set; } = new ObservableCollection<Decal>();

        private TextureMappingType _textureMapping;
        public TextureMappingType TextureMapping
        {
            get => _textureMapping;
            set
            {
                if (_textureMapping == value)
                    return;

                _textureMapping = value;
                _cachedVertexInterface = null;
                RaiseThisPropertyChanged();
            }
        }

        private bool _isSelected;
        public virtual bool IsSelected 
        { 
            get => _isSelected; 
            set 
            {
                if (_isSelected == value)
                    return;

                _isSelected = value;
                RaiseThisPropertyChanged();
            } 
        }
        
        public APresentable(RawMatrix position, IDataProvider dataProvider)
        {
            Position = position;
            _dataProvider = dataProvider;
            BuildUniquePickableColour();
            Decals.CollectionChanged += DecalsCollectionChanged;
        }

        private void BuildUniquePickableColour()
        {
            Pickable = new Pickable();
            Random random = new Random();
            Pickable.PickColour = new Point((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
            while(_dataProvider.PickableColourToObjectMapping.Keys.Any(colour => colour.Matches(Pickable.PickColour)))
                Pickable.PickColour = new Point((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());

            _dataProvider.PickableColourToObjectMapping.Add(Pickable.PickColour, this);
        }

        private void DecalsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(Decals));
        }

        private RawMatrix _position = MatrixHelpers.Identity;
        public virtual RawMatrix Position
        {
            get => _position;
            set
            {
                _position = value;         
                RaiseThisPropertyChanged();
            }
        }

        private RawMatrix _pivotOffset = MatrixHelpers.Identity;
        public virtual RawMatrix PivotOffset
        {
            get => _pivotOffset;
            set
            {
                _pivotOffset = value;
                RaiseThisPropertyChanged();
            }
        }

        public virtual float AngleX { get; set; }
        public virtual float AngleY { get; set; }
        public virtual float AngleZ { get; set; }
        public virtual float OffsetAngleX { get; set; }
        public virtual float OffsetAngleY { get; set; }
        public virtual float OffsetAngleZ { get; set; }

        private bool _scaleMode;
        public bool ScaleMode
        {
            get => _scaleMode;
            set
            {
                _scaleMode = value;
                RaiseThisPropertyChanged();
            }
        }

        #region pivot orientation

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaiseThisPropertyChanged([CallerMemberName] string propertyName="")
        {
            RaisePropertyChanged(propertyName);
        }

        protected abstract void CreateGeometryData();

        public abstract VertexInterface[] Draw();

        public abstract VertexInterface[] DrawPickable();

        private IPickable _pickable;
        public IPickable Pickable
        {
            get => _pickable;
            set
            {
                _pickable = value;
            }
        }

        protected virtual void CalculateTangentSpace(Point[] points)
        {
            _axisAlignedBoundingBox = new BoundingBox { Position = PivotOffset.Multiply(Position) };
            _axisAlignedBoundingBox.Assimilate(_vertexInfo.GeometryData);

            _vertexInfo.TexCoords.Clear();
            for(int i =0; i < _vertexInfo.TangentSpace.Count; ++i)
            {
                _vertexInfo.TangentSpace[i] = new RawMatrix(); // refresh tangent space
            }
            //determine max min x y
            float yMax = _vertexInfo.GeometryData[0].Y;
            float xMax = _vertexInfo.GeometryData[0].X;
            float yMin = _vertexInfo.GeometryData[0].Y;
            float xMin = _vertexInfo.GeometryData[0].X;
            foreach (Point data in _vertexInfo.GeometryData)
            {
                if (yMax < data.Y)
                    yMax = data.Y;
                if (xMax < data.X)
                    xMax = data.X;
                if (yMin > data.Y)
                    yMin = data.Y;
                if (xMin > data.X)
                    xMin = data.X;
            }
            float yRange = yMax - yMin;

            for (int i = 0; i < points.Count() / 2; ++i)
            {
                _vertexInfo.TexCoords.Add(new RawVector2()); // dummy up texcoords
            }

            int numTriangles = points.Length / 6;
            for (int i = 0; i < numTriangles; i++)
            {
                Point p1 = points[(i * 3) * 2];
                Point p2 = points[(i * 3 + 1) * 2];
                Point p3 = points[(i * 3 + 2) * 2];

                Vector v1 = new Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
                Vector v2 = new Vector(p3.X - p1.X, p3.Y - p1.Y, p3.Z - p1.Z);
                Vector triNormal = v1.Cross(v2).Normalize();

                int index1 = _vertexInfo.GeometryData.IndexOf(p1);
                int index2 = _vertexInfo.GeometryData.IndexOf(p2);
                int index3 = _vertexInfo.GeometryData.IndexOf(p3);

                if (TextureMapping == TextureMappingType.UV)
                {
                    float xWeight = triNormal.X / (triNormal.X + triNormal.Y + triNormal.Z);
                    float yWeight = triNormal.Y / (triNormal.X + triNormal.Y + triNormal.Z);
                    float zWeight = triNormal.Z / (triNormal.X + triNormal.Y + triNormal.Z);

                    float p1u = xWeight * p1.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p1.X / _axisAlignedBoundingBox.Size.X + zWeight * p1.X / _axisAlignedBoundingBox.Size.X;
                    float p1v = xWeight * p1.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p1.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p1.Y / _axisAlignedBoundingBox.Size.Y;
                    float p2u = xWeight * p2.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p2.X / _axisAlignedBoundingBox.Size.X + zWeight * p2.X / _axisAlignedBoundingBox.Size.X;
                    float p2v = xWeight * p2.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p2.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p2.Y / _axisAlignedBoundingBox.Size.Y;
                    float p3u = xWeight * p3.Z / _axisAlignedBoundingBox.Size.Z + yWeight * p3.X / _axisAlignedBoundingBox.Size.X + zWeight * p3.X / _axisAlignedBoundingBox.Size.X;
                    float p3v = xWeight * p3.Y / _axisAlignedBoundingBox.Size.Y + yWeight * p3.Z / _axisAlignedBoundingBox.Size.Z + zWeight * p3.Y / _axisAlignedBoundingBox.Size.Y;

                    //if (i % 2 == 0)
                    {
                        _vertexInfo.TexCoords[(i * 3)] = new RawVector2(p1u, p1v);
                        _vertexInfo.TexCoords[(i * 3 + 1)] = new RawVector2(p2u, p2v);
                        _vertexInfo.TexCoords[(i * 3 + 2)] = new RawVector2(p3u, p3v);
                    }
                    //else
                    //{
                    //    _vertexInfo.TexCoords[(i * 3)] = new RawVector2(1, 1);
                    //    _vertexInfo.TexCoords[(i * 3 + 1)] = new RawVector2(0, 1);
                    //    _vertexInfo.TexCoords[(i * 3 + 2)] = new RawVector2(0, 0);
                    //}
                }
                else if(TextureMapping == TextureMappingType.Cylindrical)
                {
                    float v = 1.0f - (p1.Y - yMin) / yRange;
                    float u = (float)((1 / (2 * Math.PI)) * Math.Acos(p1.X / xMax));
                    _vertexInfo.TexCoords[(i * 3)] = new RawVector2(u, v);
                    v = 1.0f - (p2.Y - yMin) / yRange;
                    u = (float)((1 / (2 * Math.PI)) * Math.Acos(p2.X / xMax));
                    _vertexInfo.TexCoords[(i * 3 + 1)] = new RawVector2(u, v);
                    v = 1.0f - (p3.Y - yMin) / yRange;
                    u = (float)((1 / (2 * Math.PI)) * Math.Acos(p3.X / xMax));
                    _vertexInfo.TexCoords[(i * 3 + 2)] = new RawVector2(u, v);
                }

                float s1 = _vertexInfo.TexCoords[(i * 3 + 1)].X - _vertexInfo.TexCoords[i * 3].X;
                float s2 = _vertexInfo.TexCoords[(i * 3 + 2)].X - _vertexInfo.TexCoords[i * 3].X;
                float t1 = _vertexInfo.TexCoords[(i * 3 + 1)].Y - _vertexInfo.TexCoords[i * 3].Y;
                float t2 = _vertexInfo.TexCoords[(i * 3 + 2)].Y - _vertexInfo.TexCoords[i * 3].Y;

                float r = 1.0f / (s1 * t2 - s2 * t1);

                //_vertexInfo.VertexNormals[index1] += triNormal;
                //_vertexInfo.VertexNormals[index2] += triNormal;
                //_vertexInfo.VertexNormals[index3] += triNormal;
                Vector Bnon = new Vector((s1 * (p3.X - p1.X) - s2 * (p2.X - p1.X)) * r, (s1 * (p3.Y - p1.Y) - s2 * (p2.Y - p1.Y)) * r, (s1 * (p3.Z - p1.Z) - s2 * (p2.Z - p1.Z)) * r);
                Vector N = triNormal; // normal
                Vector B = Bnon.Normalize(); // Bi-tangent
                Vector T = B.Cross(N).Normalize();// Tangent
                RawMatrix tangentSpace = MatrixHelpers.Create(T, B, N, new Point());
                _vertexInfo.TangentSpace[index1] = _vertexInfo.TangentSpace[index1].Add(tangentSpace);
                _vertexInfo.TangentSpace[index2] = _vertexInfo.TangentSpace[index2].Add(tangentSpace);
                _vertexInfo.TangentSpace[index3] = _vertexInfo.TangentSpace[index3].Add(tangentSpace);
            }

            for (int i=0;i<_vertexInfo.GeometryData.Count;++i)
            {
                //Vector nnormal = _vertexInfo.VertexNormals[i].Normalize();
                //_vertexInfo.VertexNormals[i] = nnormal;

                RawMatrix ts = _vertexInfo.TangentSpace[i];
                Vector T = new Vector(ts.M11, ts.M12, ts.M13).Normalize();
                Vector B = new Vector(ts.M21, ts.M22, ts.M23).Normalize();
                Vector N = new Vector(ts.M31, ts.M32, ts.M33).Normalize();

                _vertexInfo.TangentSpace[i] = MatrixHelpers.Create(T, B, N, new Point()); // normalized tangent space
            }

        }

        /// <summary>
        /// Transforms geomtry into View Space (NDC Space)
        /// </summary>
        //private void Project(Point[] data)
        //{
        //    List<Point> pts = new List<Point>();
        //    foreach (Point p in data)
        //    {
        //        pts.Add(p.Project(_dataProvider.Camera.ProjectionMatrix));
        //    }

        //    _vertexData = pts.ToArray();
        //}

        /// <summary>
        /// Transforms geomtry into World Space
        /// </summary>
        //protected void WorldTransform()
        //{
        //    List<Point> pts = new List<Point>();
        //    foreach (Point p in _geometryData)
        //        pts.Add(p * _dataProvider.World.Position);

        //    Project(pts.ToArray());
        //}
        public class VertexInfo
        {
            public List<Point> GeometryData { get; set; }
            //public List<Vector> VertexNormals { get; set; } = new List<Vector>();

            public List<RawVector2> TexCoords { get; set; } = new List<RawVector2>();

            public List<RawMatrix> TangentSpace { get; set; } = new List<RawMatrix>();

            public VertexInfo(List<Point> geomteryData)
            {
                GeometryData = geomteryData;

                for (int i =0; i< GeometryData.Count; ++i)
                {
                    //VertexNormals.Add(new Vector());
                    TexCoords.Add(new RawVector2());
                    TangentSpace.Add(new RawMatrix());
                }
            }
        }
    }

    public class BoundingBox
    {
        public void Assimilate(List<Point> vertices)
        {
            //determine max min x y
            float yMax = vertices[0].Y;
            float xMax = vertices[0].X;
            float zMax = vertices[0].Z;
            float yMin = vertices[0].Y;
            float xMin = vertices[0].X;
            float zMin = vertices[0].Z;
            foreach (Point data in vertices)
            {
                if (zMax < data.Z)
                    zMax = data.Z;
                if (yMax < data.Y)
                    yMax = data.Y;
                if (xMax < data.X)
                    xMax = data.X;
                if (zMin > data.Z)
                    zMin = data.Z;
                if (yMin > data.Y)
                    yMin = data.Y;
                if (xMin > data.X)
                    xMin = data.X;
            }

            Size = new RawVector3(Math.Abs(xMax - xMin), Math.Abs(yMax - yMin), Math.Abs(zMax - zMin));
        }

        public void Assimilate(IEnumerable<RawVector3> vertices)
        {
            //determine max min x y
            RawVector3 element = vertices.ElementAt(0);
            float yMax = element.Y;
            float xMax = element.X;
            float zMax = element.Z;
            float yMin = element.Y;
            float xMin = element.X;
            float zMin = element.Z;
            foreach (RawVector3 data in vertices)
            {
                if (zMax < data.Z)
                    zMax = data.Z;
                if (yMax < data.Y)
                    yMax = data.Y;
                if (xMax < data.X)
                    xMax = data.X;
                if (zMin > data.Z)
                    zMin = data.Z;
                if (yMin > data.Y)
                    yMin = data.Y;
                if (xMin > data.X)
                    xMin = data.X;
            }

            Size = new RawVector3(Math.Abs(xMax - xMin), Math.Abs(yMax - yMin), Math.Abs(zMax - zMin));
        }

        public RawVector3 Size { get; private set; } = new RawVector3();

        public RawMatrix Position { get; set; } = MatrixHelpers.Identity;
    }

    public class Pickable : IPickable
    {
        public Point PickColour { get; set; } = new Point(0, 0, 0);
    }

}
