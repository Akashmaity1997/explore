﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Assets.Materials;
using SharpDX.Direct3D11;

namespace Assets.Interfaces
{
    public interface IPickable
    {
        Point PickColour { get; set; }
    }

    public interface IPositionable
    {
        RawMatrix Position { get; set; }

        /// <summary>
        /// This defines the centre of mesh relative to <see cref="Position">
        /// </summary>
        RawMatrix PivotOffset { get; set; }

        float AngleX { get; set; }

        float AngleY { get; set; }

        float AngleZ { get; set; }

        float OffsetAngleX { get; set; }

        float OffsetAngleY { get; set; }

        float OffsetAngleZ { get; set; }
    }

    public interface IDrawable : IPositionable
    {
        bool IsSelected { get; set; }

        IPickable Pickable { get; set; }

        VertexInterface[] Draw();

        VertexInterface[] DrawPickable();

        /// <summary>
        /// True for unpickable
        /// </summary>
        bool IsSpecial { get; }

        /// <summary>
        /// True for not showing manipulator for itself, but is Pickable
        /// </summary>
        bool IsManipulator { get; }

        /// <summary>
        /// true for making active manipulator as a scaler
        /// </summary>
        bool ScaleMode { get; set; }
    }

    public interface IDataProvider
    {
        event PropertyChangedEventHandler PropertyChanged;
        ObservableCollection<IDrawable> Objects { get; }

        Dictionary<Point, IDrawable> PickableColourToObjectMapping { get; }
        ObservableCollection<Light> Lights { get; }

        ObservableCollection<Camera> Cameras { get; }

        ObservableCollection<Decal> Decals { get; }

        IWorldDm CurrentWorldDm { get; set; }

        ICameraDm CurrentCameraDm { get; set; }

        IEnvironmentDm CurrentEnvironmentDm { get; set; }

        World CurrentWorld { get; set; }

        Camera CurrentCamera { get; set; }

        event EventHandler CrucialPropertyChanged;

        IBaseDm FindByGuid(Guid guid);

        void RegisterObject(APresentable obj);

        void RegisterObject(Camera camera);

        void RegisterObject(Decal decal);

        void UnRegisterObject(APresentable obj);

        void UnRegisterObject(Decal decal);

        void UnRegisterObject();

        void Manipulate(Vector delta);

        /// <summary>
        /// Helps in changing LookAt of current camera
        /// </summary>
        /// <param name="follow"></param>
        void PickCameraTarget(bool follow = false);

        /// <summary>
        /// Geometry vertex count
        /// </summary>
        int VertexCount { get; }

        IBaseDm SelectedDm { get; set; }

        IDrawable SelectedObject { get; set; }

        void DisposeAssets();
    }
}
