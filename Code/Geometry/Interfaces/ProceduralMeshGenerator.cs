﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;

namespace Assets.Interfaces
{
    public enum ProceduralMeshType
    {
        Cube,
        Sphere,
        Cylinder,
        Torus,
        Plane,
        Cone
    }

    public static class ProceduralMeshGenerator
    {
        private const float _sliceHeight = 0.1f;
        private const float _sliceAngle = 5f;

        public static Mesh Create(ProceduralMeshType type)
        {
            switch(type)
            {
                case ProceduralMeshType.Cube:
                    return CreateCube();
                case ProceduralMeshType.Cylinder:
                    return CreateCylinder();
                case ProceduralMeshType.Plane:
                    return CreatePlane();
                case ProceduralMeshType.Sphere:
                    return CreateSphere();
                case ProceduralMeshType.Torus:
                    return CreateTorus();
                case ProceduralMeshType.Cone:
                    return CreateCone();
                default:
                    throw new NotImplementedException();
            }
        }

        public static Mesh CreateCube(float size = 1f)
        {
            Mesh mesh = new Mesh();

            List<RawVector3> geometryData = new List<RawVector3>()
            {
                new RawVector3(-0.5f * size, 0.5f * size, -0.5f* size),
                new RawVector3(0.5f * size, 0.5f * size, -0.5f * size),
                new RawVector3(0.5f* size, -0.5f* size, -0.5f * size),
                new RawVector3(-0.5f * size, -0.5f * size, -0.5f* size),
                new RawVector3(0.5f * size, 0.5f* size, -0.5f * size),
                new RawVector3(0.5f * size, 0.5f * size, 0.5f * size),
                new RawVector3(0.5f * size, -0.5f * size, 0.5f * size),
                new RawVector3(0.5f * size, -0.5f * size, -0.5f * size),
                new RawVector3(0.5f * size, 0.5f * size, 0.5f* size),
                new RawVector3(-0.5f * size, 0.5f * size, 0.5f * size),
                new RawVector3(-0.5f* size, -0.5f* size, 0.5f * size),
                new RawVector3(0.5f * size, -0.5f * size, 0.5f* size),
                new RawVector3(-0.5f * size, 0.5f* size, 0.5f * size),
                new RawVector3(-0.5f * size, 0.5f * size, -0.5f * size),
                new RawVector3(-0.5f * size, -0.5f * size, -0.5f * size),
                new RawVector3(-0.5f * size, -0.5f * size, 0.5f * size),
                new RawVector3(-0.5f * size, 0.5f * size, 0.5f* size),
                new RawVector3(0.5f * size, 0.5f * size, 0.5f * size),
                new RawVector3(0.5f* size, 0.5f* size, -0.5f * size),
                new RawVector3(-0.5f * size, 0.5f * size, -0.5f* size),
                new RawVector3(-0.5f * size, -0.5f* size, -0.5f * size),
                new RawVector3(0.5f * size, -0.5f * size, -0.5f * size),
                new RawVector3(0.5f * size, -0.5f * size, 0.5f * size),
                new RawVector3(-0.5f * size, -0.5f * size, 0.5f * size)
            };
            mesh.Vertices.AddRange(geometryData);
            mesh.UpdateBoundingBox();

            List<(int, int, int)> faces = new List<(int, int, int)>()
            {
                (0, 1, 2),
                (0, 2, 3),
                (4, 5, 6),
                (4, 6, 7),
                (8, 9, 10),
                (8, 10, 11),
                (12, 13, 14),
                (12, 14, 15),
                (16, 17, 18),
                (16, 18, 19),
                (20, 21, 22),
                (20, 22, 23)
            };
            mesh.Faces.AddRange(faces);

            mesh.GenerateNormals();
            mesh.GenerateUVs();
            mesh.GenerateTangents();

            return mesh;
        }

        public static Mesh CreatePlane(float size = 1)
        {
            Mesh mesh = new Mesh();
            List<RawVector3> geometryData = new List<RawVector3>()
            {
                new RawVector3(-1f * size, 1f * size, 0),
                new RawVector3(1f * size, 1f * size, 0),
                new RawVector3(1f * size, -1f * size, 0),
                new RawVector3(-1f * size, -1f * size, 0)
            };
            mesh.Vertices.AddRange(geometryData);
            mesh.UpdateBoundingBox();

            List<(int, int, int)> faces = new List<(int, int, int)>()
            {
                (0, 1, 2),
                (0, 2, 3)
            };
            mesh.Faces.AddRange(faces);

            List<RawVector2> uvs = new List<RawVector2>()
            {
                new RawVector2(0f, 0f),
                new RawVector2(1f, 0f),
                new RawVector2(1f, 1f),
                new RawVector2(0f, 1f)
            };
            mesh.UVs.AddRange(uvs);

            RawVector3 normal = new RawVector3(0, 0, -1);
            List<RawVector3> normals = new List<RawVector3>()
            {
                normal,
                normal,
                normal,
                normal
            };
            mesh.Normals.AddRange(normals);

            RawVector3 tangent = new RawVector3(1, 0, 0);
            List<RawVector3> tangents = new List<RawVector3>()
            {
                tangent,
                tangent,
                tangent,
                tangent
            };
            mesh.Tangents.AddRange(tangents);

            return mesh;
        }

        public static Mesh CreateCylinder(float height = 1f, float radius = 1f)
        {
            return CreateCylinder(MatrixHelpers.Identity, height, radius);
        }

        public static Mesh CreateCylinder(RawMatrix position, float height, float radius)
        {
            Mesh mesh = new Mesh();
            mesh.Vertices.Add(new RawVector3(0, height / 2, 0));
            mesh.UVs.Add(new RawVector2());
            mesh.Tangents.Add(Vector.XAxis.ToVector3());
            mesh.Normals.Add(Vector.YAxis.ToVector3());
            for (float i = height; i >= 0; i -= _sliceHeight)
            {
                Point basePoint = new Point(0, i, -radius);
                Vector baseNormal = Vector.NegZAxis;
                Vector baseTangent = Vector.XAxis;
                for (float theta = 0.0f; theta <= 360.0f - _sliceAngle; theta += _sliceAngle)
                {
                    RawMatrix m = MatrixHelpers.CreateRotationMatrix(-theta, Vector.YAxis);
                    Point p = basePoint * m;
                    Vector rotatedNormal = baseNormal * m;
                    Vector rotatedTangent = baseTangent * m;
                    Point tp = new Point(p.X, p.Y - height / 2, p.Z);
                    mesh.Vertices.Add(tp.ToVector3());
                    mesh.UVs.Add(new RawVector2(theta / 360f, 1f - i / height));
                    mesh.Normals.Add(rotatedNormal.ToVector3());
                    mesh.Tangents.Add(rotatedTangent.ToVector3());
                }

                mesh.Vertices.Add(new RawVector3(0, i - height / 2, -radius));
                mesh.UVs.Add(new RawVector2(1, 1f - i / height));
                mesh.Normals.Add(baseNormal.ToVector3());
                mesh.Tangents.Add(baseTangent.ToVector3());
            }
            mesh.Vertices.Add(new RawVector3(0, -height / 2, 0));
            mesh.UVs.Add(new RawVector2(0, 1));
            mesh.Tangents.Add(Vector.XAxis.ToVector3());
            mesh.Normals.Add(Vector.NegYAxis.ToVector3());

            int heightPoints = (int)(height / _sliceHeight) + 1;
            int slicePoints = (int)(360f / _sliceAngle) + 1;
            for (int i = 0; i < heightPoints - 1; ++i)
            {
                for (int j = 0; j < slicePoints - 1; ++j)
                {
                    mesh.Faces.Add((slicePoints * i + j + 1, slicePoints * i + j + 1 + 1, slicePoints * (i + 1) + j + 1 + 1));
                    mesh.Faces.Add((slicePoints * i + j + 1, slicePoints * (i + 1) + j + 1 + 1, slicePoints * (i + 1) + j + 1));
                }
            }

            int stride = (heightPoints - 1) * slicePoints;
            for (int j = 0; j < slicePoints - 1; ++j)
            {
                mesh.Faces.Add((j + 1, 0, j + 1 + 1));
                mesh.Faces.Add((stride + j + 1, stride + j + 1 + 1, mesh.Vertices.Count - 1));
            }

            mesh.Transform(position);
            return mesh;
        }

        public static Mesh CreateSphere(float radius = 1f)
        {
            Mesh mesh = new Mesh();
            for (float phi = 0; phi <= 180f; phi += _sliceAngle)
            {
                RawMatrix mp = MatrixHelpers.CreateRotationMatrix(-phi, Vector.XAxis);
                for (float theta = 0.0f; theta <= 360.0f; theta += _sliceAngle)
                {
                    RawMatrix m = MatrixHelpers.CreateRotationMatrix(-theta, Vector.YAxis);
                    RawMatrix rotM = mp.Multiply(m);
                    Vector v = Vector.YAxis * rotM;
                    v = v.Normalize().Scale(radius);
                    RawVector3 normal = new RawVector3(rotM.M21, rotM.M22, rotM.M23);
                    RawVector3 tangent = new RawVector3(rotM.M11, rotM.M12, rotM.M13);
                    mesh.Vertices.Add(v.ToVector3());
                    mesh.UVs.Add(new RawVector2(theta / 360f, phi / 180f));
                    mesh.Normals.Add(normal);
                    mesh.Tangents.Add(tangent);
                }
            }

            int slicePointsPhi = (int)(180f / _sliceAngle) + 1;
            int slicePoints = (int)(360f / _sliceAngle) + 1;
            for (int i = 0; i < slicePointsPhi - 1; ++i)
            {
                for (int j = 0; j < slicePoints - 1; ++j)
                {
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * i + j + 1, slicePoints * (i + 1) + j + 1));
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * (i + 1) + j + 1, slicePoints * i + j + 1));
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * (i + 1) + j + 1, slicePoints * (i + 1) + j));
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * (i + 1) + j, slicePoints * (i + 1) + j + 1));
                }
            }

            //int stride = (slicePointsPhi - 3) * slicePoints;
            //for (int j = 0; j < slicePoints - 1; ++j)
            //{
            //    mesh.Faces.Add((j + 1, 0, j + 1 + 1));
            //    mesh.Faces.Add((stride + j + 1, stride + j + 1 + 1, mesh.Vertices.Count - 1));
            //}

            return mesh;
        }

        public static Mesh CreateTorus(float radius = 1, float width = 0.5f)
        {
            Mesh mesh = new Mesh();
            for(float phi = 0; phi <= 360f; phi += _sliceAngle)
            {
                for(float theta = 0; theta <= 360; theta += _sliceAngle)
                {
                    RawMatrix rotMtx = MatrixHelpers.CreateRotationMatrix(-theta, Vector.YAxis);
                    Vector rotAxis = new Vector(rotMtx.M11, rotMtx.M12, rotMtx.M13);
                    Vector baseVector = new Vector(rotMtx.M31, rotMtx.M32, rotMtx.M33).Flip().Scale(radius);
                    Vector rv = Vector.YAxis * MatrixHelpers.CreateRotationMatrix(-phi, rotAxis);
                    rv = rv.Normalize();
                    RawVector3 normal = rv.ToVector3();
                    rv = rv.Scale(width);
                    Vector v = baseVector + rv;
                    mesh.Vertices.Add(v.ToVector3());
                    mesh.UVs.Add(new RawVector2(theta / 360f, phi / 360f));
                    mesh.Tangents.Add(rotAxis.ToVector3());
                    mesh.Normals.Add(normal);
                }
            }

            int slicePoints = (int)(360f / _sliceAngle) + 1;
            for(int i = 0; i < slicePoints - 1; ++i)
            {
                for (int j = 0; j < slicePoints - 1; ++j)
                {
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * i + j + 1, slicePoints * (i + 1) + j + 1));
                    mesh.Faces.Add((slicePoints * i + j, slicePoints * (i + 1) + j + 1, slicePoints * (i + 1) + j));
                }
            }

            return mesh;
        }

        public static Mesh CreateCone(float height = 1, float radius = 0.5f)
        {
            return CreateCone(MatrixHelpers.Identity, height, radius);
        }

        public static Mesh CreateCone(RawMatrix position, float height, float radius)
        {
            Mesh mesh = new Mesh();
            for (float theta = 0; theta <= 360; theta += _sliceAngle)
            {
                RawMatrix rotMtx = MatrixHelpers.CreateRotationMatrix(-theta, Vector.YAxis);
                Vector v = Vector.NegZAxis;
                v = v * rotMtx;
                v = v.Scale(radius);
                RawVector3 normal = new RawVector3(-rotMtx.M31, -rotMtx.M32, -rotMtx.M33);
                RawVector3 tangent = new RawVector3(rotMtx.M11, rotMtx.M12, rotMtx.M13);
                mesh.Vertices.Add(new RawVector3(0, 1, 0));
                mesh.UVs.Add(new RawVector2(theta / 360f, 0));
                mesh.Tangents.Add(tangent);
                mesh.Normals.Add(Vector.YAxis.ToVector3());
                mesh.Vertices.Add(v.ToVector3());
                mesh.UVs.Add(new RawVector2(theta / 360f, 1));
                mesh.Tangents.Add(tangent);
                mesh.Normals.Add(normal);
                mesh.Vertices.Add(new RawVector3());
                mesh.UVs.Add(new RawVector2(theta / 360f, 1));
                mesh.Tangents.Add(tangent);
                mesh.Normals.Add(Vector.NegYAxis.ToVector3());
            }

            int slicePoints = (int)(360f / _sliceAngle) + 1;
            for (int i = 0; i < slicePoints - 1; ++i)
            {
                mesh.Faces.Add((i * 3 + 1, i * 3, (i + 1) * 3 + 1));
                mesh.Faces.Add((i * 3 + 1, (i + 1) * 3 + 1, i * 3 + 2));
            }

            mesh.Transform(position);
            return mesh;
        }
    }
}
