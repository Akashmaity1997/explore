﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D11;

namespace Assets.Interfaces
{
    public interface ITextureResource : IDisposable
    {
        int Width { get; } 

        int Height { get; }

        ShaderResourceView GetShaderResourceView(Device device);

        SamplerState GetSamplerState(Device device);
    }
}
