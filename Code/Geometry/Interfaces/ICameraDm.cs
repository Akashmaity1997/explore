﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;

namespace Assets.Interfaces
{
    public interface ICameraDm : IPositionableDm
    {
        RawMatrix ProjectionMatrix { get; }

        RawMatrix ViewMatrix { get; }

        float FOV { get; set; }

        Vector LookAt { get; set; }

        Vector Up { get; set; }

        float AspectRatio { get; set; }
    }
}
