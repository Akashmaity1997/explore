﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Assets.Interfaces
{
    public interface IBaseDm : INotifyPropertyChanged
    {
        string Name { get; }

        Guid Guid { get; }
    }
}
