﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Interfaces
{
    public delegate void DoJobDelegate();
    public delegate void SucceededJobDelegate();
    public delegate void FailedJobDelegate(Exception ex);
    public delegate void CancelledJobDelegate();
    public delegate T DoJobDelegate<T>();
    public delegate void SucceededJobDelegate<T>(T param);

    public interface IBackgroundJobManager
    {
        bool JobActive { get; }
        void RunBackgroundJob(DoJobDelegate doIt, SucceededJobDelegate succeeded = null, FailedJobDelegate failed = null, CancelledJobDelegate cancelled = null);
        void RunBackgroundJob<T>(DoJobDelegate<T> doIt, SucceededJobDelegate<T> succeeded = null, FailedJobDelegate failed = null, CancelledJobDelegate cancelled = null);
    }
}
