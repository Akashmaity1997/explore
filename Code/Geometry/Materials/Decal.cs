﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Direct3D11;
using System.Windows.Media.Imaging;
using Assets.Interfaces;
using System.IO;

namespace Assets.Materials
{
    public class TextureAsset : IDisposable
    {
        public Texture2D Texture { get; set; }
        public string AssetPath { get; set; }

        public WriteableBitmap Thumbnail { get; set; }

        public void Dispose()
        {
            Texture.Dispose();
        }
    }

    public class Decal
    {
        private readonly TextureAsset _textureAsset;
        private readonly IDataProvider _dataProvider;

        public TextureAsset TextureAsset => _textureAsset;

        public Decal(string filepath, Texture2D texture, WriteableBitmap image, IDataProvider dataProvider)
        {
            _textureAsset = new TextureAsset
            {
                AssetPath = filepath,
                Texture = texture,
                Thumbnail = image
            };

            CreateThumbnail();
            _dataProvider = dataProvider;
            _dataProvider.RegisterObject(this);
        }

        public void Unregister()
        {
            TextureAsset.Dispose();
            _dataProvider.UnRegisterObject(this);
        }

        private void CreateThumbnail()
        {
            string path = Path.Combine(AssetManager.DecalThumbnailDir, Path.GetFileNameWithoutExtension(_textureAsset.AssetPath));
            path += ".PNG";
            using(FileStream fs = new FileStream(path, FileMode.Create))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(_textureAsset.Thumbnail.Clone()));
                encoder.Save(fs);
            }

            Thumbnail = new BitmapImage(new Uri(path));
        }

        public Texture2D Texture => _textureAsset.Texture;

        public BitmapImage Thumbnail { get; set; }

        public void SetOnPart()
        {
            if (_dataProvider.SelectedObject is APresentable model)
            {
                model.Decals.Add(this);
            }
        }

        public void RemoveFromPart()
        {
            if (_dataProvider.SelectedObject is APresentable model)
            {
                model.Decals.Remove(this);
            }
        }
    }
}
