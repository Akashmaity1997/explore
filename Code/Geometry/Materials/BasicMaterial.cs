﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace Assets.Materials
{
    public class BasicMaterial : MaterialDefinition
    {
        public BasicMaterial()
        {
            Metalness = 0;
            Roughness = 1;
        }
    }
}
