﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;

namespace Assets.Materials
{
    public class PostProcessMaterial : MaterialDefinition
    {
        public float InputGamma { get; set; } = 1;

        public float OutputGamma { get; set; } = 1;

        public ITextureResource PostProcessMap { get; set; }

        public ITextureResource MergeMap { get; set; }

        public RawVector2 TexelSize => new RawVector2(1f / PostProcessMap.Width, 1f / PostProcessMap.Height);

        public override void Dispose()
        {
            PostProcessMap?.Dispose();
            MergeMap?.Dispose();
        }
    }
}
