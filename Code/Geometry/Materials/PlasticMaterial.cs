﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;

namespace Assets.Materials
{
    public class PlasticMaterial : MaterialDefinition
    {
        public PlasticMaterial()
        {
            Metalness = 0.2f;
            Roughness = 0.6f;
        }
    }
}
