﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Interfaces;
using SharpDX.Mathematics.Interop;
using Assets.Utilities;

namespace Assets
{
    public class Cylinder : APresentable, IPositionable
    {
        private const float SLICE_HEIGHT = 0.1f;
        private float SECTION_ANGLE = 15.0f;

        private readonly float _height, _radius;

        public Point Colour { get; set; } = new Point(0.0f, 1.0f, 0.0f);

        public Cylinder(float height, float radius, IDataProvider dataProvider, Point colour, bool isSpecial = false, bool isManipulator = false) : base(MatrixHelpers.Identity, dataProvider)
        {
            _height = height;
            _radius = radius;
            IsSpecial = isSpecial;
            IsManipulator = isManipulator;
            Colour = colour;
            CreateGeometryData();
            _dataProvider.RegisterObject(this);
        }

        public Cylinder(float height, float radius, RawMatrix position, IDataProvider dataProvider, Point colour, bool isSpecial = false, bool isManipulator = false) : base(position, dataProvider)
        {
            _height = height;
            _radius = radius;
            IsSpecial = isSpecial;
            IsManipulator = isManipulator;
            Colour = colour;
            CreateGeometryData();
            _dataProvider.RegisterObject(this);
        }

        protected override void CreateGeometryData()
        {
            if (IsManipulator || IsSpecial)
                SECTION_ANGLE = 45.0f;

            Point up = new Point(0, _height / 2, 0);
            List<Point> pts = new List<Point>();
            pts.Add(up);
            for (float i = _height; i >= 0; i -= SLICE_HEIGHT)
            {
                Point basePoint = new Point(0, i, -_radius);
                for (float theta = 0.0f; theta <= 360.0f - SECTION_ANGLE; theta += SECTION_ANGLE)
                {
                    Point p = basePoint.Rotate(-theta, Vector.YAxis);
                    Point tp = new Point(p.X, p.Y - _height / 2, p.Z);
                    pts.Add(tp);
                }
            }
            Point down = new Point(0, -_height / 2, 0);
            pts.Add(down);

            _vertexInfo = new VertexInfo(pts);
        }

        public override VertexInterface[] Draw()
        {
            if (_cachedVertexInterface == null)
            {
                Point[] geometryData = _vertexInfo.GeometryData.ToArray();
                int sectionRes = (int)(360.0f / SECTION_ANGLE);
                //WorldTransform();
                List<Point> verts = new List<Point>();
                int lastIndex = geometryData.Length - 2; // without the bottom cap vertex

                for (int y = 1; y <= lastIndex - ((sectionRes * 2) - 1); y += sectionRes) // without the top cap vertex
                {
                    int i = y;

                    for (; i < y + (sectionRes - 1); ++i)
                    {
                        verts.Add(geometryData[i]);
                        verts.Add(Colour);
                        verts.Add(geometryData[i + 1]);
                        verts.Add(Colour);
                        verts.Add(geometryData[i + sectionRes + 1]);
                        verts.Add(Colour);
                        verts.Add(geometryData[i + sectionRes + 1]);
                        verts.Add(Colour);
                        verts.Add(geometryData[i + sectionRes]);
                        verts.Add(Colour);
                        verts.Add(geometryData[i]);
                        verts.Add(Colour);
                    }

                    verts.Add(geometryData[i]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i - (sectionRes - 1)]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i + 1]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i + 1]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i + sectionRes]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i]);
                    verts.Add(Colour);
                }

                // caps
                for (int i = 1; i <= (sectionRes - 1); i++)
                {
                    verts.Add(geometryData[0]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i + 1]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i]);
                    verts.Add(Colour);
                }

                verts.Add(geometryData[0]);
                verts.Add(Colour);
                verts.Add(geometryData[1]);
                verts.Add(Colour);
                verts.Add(geometryData[sectionRes]);
                verts.Add(Colour);

                for (int i = lastIndex - (sectionRes - 1); i < lastIndex; i++)
                {
                    verts.Add(geometryData[lastIndex + 1]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i]);
                    verts.Add(Colour);
                    verts.Add(geometryData[i + 1]);
                    verts.Add(Colour);
                }
                verts.Add(geometryData[lastIndex + 1]);
                verts.Add(Colour);
                verts.Add(geometryData[lastIndex]);
                verts.Add(Colour);
                verts.Add(geometryData[lastIndex - (sectionRes - 1)]);
                verts.Add(Colour);

                CalculateTangentSpace(verts.ToArray());

                List<VertexInterface> vertss = new List<VertexInterface>();
                List<VertexInterface> vtsp = new List<VertexInterface>();
                for (int i = 0; i < verts.Count; i += 2)
                {
                    Point p = verts[i];
                    vertss.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    int index = _vertexInfo.GeometryData.IndexOf(p);
                    //Vector v = _vertexInfo.VertexNormals[index];
                    //vertss.Add(new VertexInterface { X = v.X, Y = v.Y, Z = v.Z, W = v.W });
                    p = verts[i + 1];
                    vertss.Add(new VertexInterface { X = p.X, Y = p.Y, Z = p.Z, W = p.W });
                    vtsp.Add(new VertexInterface { X = Pickable.PickColour.X, Y = Pickable.PickColour.Y, Z = Pickable.PickColour.Z, W = Pickable.PickColour.W });
                    RawVector2 tex = _vertexInfo.TexCoords[i/2];
                    vertss.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    vtsp.Add(new VertexInterface { X = tex.X, Y = tex.Y, Z = 0, W = 0 });
                    RawMatrix ts = _vertexInfo.TangentSpace[index];
                    vertss.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vertss.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vertss.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });
                    vtsp.Add(new VertexInterface { X = ts.M11, Y = ts.M12, Z = ts.M13, W = ts.M14 });
                    vtsp.Add(new VertexInterface { X = ts.M21, Y = ts.M22, Z = ts.M23, W = ts.M24 });
                    vtsp.Add(new VertexInterface { X = ts.M31, Y = ts.M32, Z = ts.M33, W = ts.M34 });
                }
                _cachedVertexInterface = vertss.ToArray();
                _cachedVertexInterfaceForPickable = vtsp.ToArray();
            }

            return _cachedVertexInterface;
        }

        public override VertexInterface[] DrawPickable()
        {
            if (_cachedVertexInterface == null)
                Draw();

            return _cachedVertexInterfaceForPickable;
        }
    }
}
