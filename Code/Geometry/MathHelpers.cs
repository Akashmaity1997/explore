﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;

namespace Assets
{
    public class Point
    {
        private readonly float x, y, z, w = 1.0f;

        public float X => x;
        public float Y => y;
        public float Z => z;

        public float W => w;

        public Point()
        {
            this.x = 0.0f;
            this.y = 0.0f;
            this.z = 0.0f;
        }

        public Point (float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Point(RawVector3 vector)
        {
            x = vector.X;
            y = vector.Y;
            z = vector.Z;
        }

        public static bool operator== (Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z;
        }

        public static bool operator!= (Point p1, Point p2)
        {
            return p1.X != p2.X || p1.Y != p2.Y || p1.Z != p2.Z;
        }

        public static Point operator* (Point p, RawMatrix m)
        {
            float x = p.x * m.M11 + p.y*m.M21 + p.z*m.M31 + p.w*m.M41;
            float y = p.x * m.M12 + p.y * m.M22 + p.z * m.M32 + p.w * m.M42;
            float z = p.x * m.M13 + p.y * m.M23 + p.z * m.M33 + p.w * m.M43;

            return new Point(x, y, z);
        }

        public static Point operator +(Point v1, Point v2)
        {
            return new Point(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public static Point operator -(Point v1, Point v2)
        {
            return new Point(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }
    }

    public static class PointExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="angle">angle to rotate</param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static Point Rotate(this Point v, float angle, Vector axis)
        {
            RawMatrix m = MatrixHelpers.CreateRotationMatrix(angle, axis);

            return v * m;
        }

        public static Point Project(this Point p, RawMatrix projMatrix)
        {
            Point pr = p * projMatrix;
            return new Point(pr.X / p.Z, pr.Y / p.Z, pr.Z / p.Z);
        }

        public static RawVector3 ToVector3(this Point p)
        {
            return new RawVector3 { X = p.X, Y = p.Y, Z = p.Z };
        }

        public static RawVector4 ToVector4(this Point p)
        {
            return new RawVector4 { X = p.X, Y = p.Y, Z = p.Z, W = p.W };
        }

        public static bool Matches(this Point p1, Point p2)
        {
            float epsilon = 0.01f;
            return (Math.Abs(p1.X - p2.X) < epsilon) && (Math.Abs(p1.Y - p2.Y) < epsilon) && (Math.Abs(p1.Z - p2.Z) < epsilon);
        }

    }

    public class Vector
    {
        private readonly float x, y, z, w = 0.0f;

        public float X => x;
        public float Y => y;
        public float Z => z;
        public float W => w;

        public static Vector XAxis => new Vector(1.0f, 0.0f, 0.0f);
        public static Vector YAxis => new Vector(0.0f, 1.0f, 0.0f);
        public static Vector ZAxis => new Vector(0.0f, 0.0f, 1.0f);
        public static Vector NegXAxis => new Vector(-1.0f, 0.0f, 0.0f);
        public static Vector NegYAxis => new Vector(0.0f, -1.0f, 0.0f);
        public static Vector NegZAxis => new Vector(0.0f, 0.0f, -1.0f);

        public Vector()
        {
            this.x = 0.0f;
            this.y = 0.0f;
            this.z = 0.0f;
        }

        public Vector(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector(RawVector3 vector)
        {
            x = vector.X;
            y = vector.Y;
            z = vector.Z;
        }

        public static Vector operator *(Vector v, RawMatrix m)
        {
            float x = v.x * m.M11 + v.y * m.M21 + v.z * m.M31 + v.w * m.M41;
            float y = v.x * m.M12 + v.y * m.M22 + v.z * m.M32 + v.w * m.M42;
            float z = v.x * m.M13 + v.y * m.M23 + v.z * m.M33 + v.w * m.M43;

            return new Vector(x, y, z);
        }

        public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }
    }

    public static class VectorExtensions
    {
        public static float Magnitude(this Vector v)
        {
            return (float)Math.Pow(v.X * v.X + v.Y * v.Y + v.Z * v.Z, 0.5);
        }

        public static Vector Normalize(this Vector v)
        {
            float mag = v.Magnitude();
            return new Vector(v.X / mag, v.Y / mag, v.Z / mag);
        }

        public static Vector Scale(this Vector v, float scale)
        {
            return new Vector(v.X * scale, v.Y * scale, v.Z * scale);
        }

        public static Vector Flip(this Vector v)
        {
            return new Vector(-v.X, -v.Y, -v.Z);
        }

        public static RawVector3 ToVector3(this Vector v)
        {
            return new RawVector3 { X = v.X, Y = v.Y, Z = v.Z };
        }

        public static RawVector4 ToVector4(this Vector v)
        {
            return new RawVector4 { X= v.X, Y = v.Y, Z = v.Z, W = v.W};
        }

        public static RawColor3 PickingColor(int i)
        {
            ++i;
            byte r = (byte)((((i & (1 << 0)) >> 0) << 7) | (((i & (1 << 3)) >> 3) << 6) |
                (((i & (1 << 6)) >> 6) << 5) | (((i & (1 << 9)) >> 9) << 4) |
                (((i & (1 << 12)) >> 12) << 3) | (((i & (1 << 15)) >> 15) << 2) |
                (((i & (1 << 18)) >> 18) << 1) | (((i & (1 << 21)) >> 21) << 0));
            byte g = (byte)((((i & (1 << 1)) >> 1) << 7) | (((i & (1 << 4)) >> 4) << 6) |
                (((i & (1 << 7)) >> 7) << 5) | (((i & (1 << 10)) >> 10) << 4) |
                (((i & (1 << 13)) >> 13) << 3) | (((i & (1 << 16)) >> 16) << 2) |
                (((i & (1 << 19)) >> 19) << 1) | (((i & (1 << 22)) >> 22) << 0));
            byte b = (byte)((((i & (1 << 2)) >> 2) << 7) | (((i & (1 << 5)) >> 5) << 6) |
                (((i & (1 << 8)) >> 8) << 5) | (((i & (1 << 11)) >> 11) << 4) |
                (((i & (1 << 14)) >> 14) << 3) | (((i & (1 << 17)) >> 17) << 2) |
                (((i & (1 << 20)) >> 20) << 1) | (((i & (1 << 23)) >> 23) << 0));

            return new RawColor3(r / 255f, g / 255f, b / 255f);
        }

        public static Vector Rotate(this Vector v, float angle, Vector axis)
        {
            RawMatrix m = MatrixHelpers.CreateRotationMatrix(angle, axis);

            return v * m;
        }

        public static Vector Cross(this Vector v1, Vector v2)
        {
            return new Vector(v1.Y*v2.Z - v1.Z*v2.Y, -(v1.X*v2.Z - v1.Z*v2.X), v1.X*v2.Y - v1.Y*v2.X);
        }

        public static float Dot(this Vector v1, Vector v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }
    }

    public static class MatrixHelpers
    {

        public static RawMatrix Identity => Create(Vector.XAxis, Vector.YAxis, Vector.ZAxis, new Point());

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle">angle in degrees</param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static RawMatrix CreateRotationMatrix(float angle, Vector axis)
        {
            double rad = angle * Math.PI / 180.0f;
            Vector naxis = axis.Normalize();

            // construct rotation matrix
            return new RawMatrix
            {
                M11 = (float)(Math.Cos(rad) + (1 - Math.Cos(rad)) * Math.Pow(naxis.X, 2)),
                M12 = (float)((1 - Math.Cos(rad)) * naxis.X * naxis.Y + Math.Sin(rad) * naxis.Z),
                M13 = (float)((1 - Math.Cos(rad)) * naxis.X * naxis.Z - Math.Sin(rad) * naxis.Y),
                M14 = 0,
                M21 = (float)((1 - Math.Cos(rad)) * naxis.X * naxis.Y - Math.Sin(rad) * naxis.Z),
                M22 = (float)(Math.Cos(rad) + (1 - Math.Cos(rad)) * Math.Pow(naxis.Y, 2)),
                M23 = (float)((1 - Math.Cos(rad)) * naxis.Y * naxis.Z + Math.Sin(rad) * naxis.X),
                M24 = 0,
                M31 = (float)((1 - Math.Cos(rad)) * naxis.X * naxis.Z + Math.Sin(rad) * naxis.Y),
                M32 = (float)((1 - Math.Cos(rad)) * naxis.Y * naxis.Z - Math.Sin(rad) * naxis.X),
                M33 = (float)(Math.Cos(rad) + (1 - Math.Cos(rad)) * Math.Pow(naxis.Z, 2)),
                M34 = 0,
                M41 = 0,
                M42 = 0,
                M43 = 0,
                M44 = 1
            };
        }

        public static RawMatrix Create(RawMatrix m)
        {
            return new RawMatrix
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M13,
                M14 = m.M14,
                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M23,
                M24 = m.M24,
                M31 = m.M31,
                M32 = m.M32,
                M33 = m.M33,
                M34 = m.M34,
                M41 = m.M41,
                M42 = m.M42,
                M43 = m.M43,
                M44 = m.M44
            };
        }

        public static RawMatrix Create(Vector x, Vector y, Vector z, Point p)
        {
            return new RawMatrix
            {
                M11 = x.X,
                M12 = x.Y,
                M13 = x.Z,
                M14 = x.W,
                M21 = y.X,
                M22 = y.Y,
                M23 = y.Z,
                M24 = y.W,
                M31 = z.X,
                M32 = z.Y,
                M33 = z.Z,
                M34 = z.W,
                M41 = p.X,
                M42 = p.Y,
                M43 = p.Z,
                M44 = p.W
            };
        }

        public static RawMatrix Multiply(this RawMatrix m1, RawMatrix m2)
        {
            // m1 x m2
            return new RawMatrix
            {
                M11 = m1.M11 * m2.M11 + m1.M12 * m2.M21 + m1.M13 * m2.M31 + m1.M14 * m2.M41,
                M12 = m1.M11 * m2.M12 + m1.M12 * m2.M22 + m1.M13 * m2.M32 + m1.M14 * m2.M42,
                M13 = m1.M11 * m2.M13 + m1.M12 * m2.M23 + m1.M13 * m2.M33 + m1.M14 * m2.M43,
                M14 = m1.M11 * m2.M14 + m1.M12 * m2.M24 + m1.M13 * m2.M34 + m1.M14 * m2.M44,
                M21 = m1.M21 * m2.M11 + m1.M22 * m2.M21 + m1.M23 * m2.M31 + m1.M24 * m2.M41,
                M22 = m1.M21 * m2.M12 + m1.M22 * m2.M22 + m1.M23 * m2.M32 + m1.M24 * m2.M42,
                M23 = m1.M21 * m2.M13 + m1.M22 * m2.M23 + m1.M23 * m2.M33 + m1.M24 * m2.M43,
                M24 = m1.M21 * m2.M14 + m1.M22 * m2.M24 + m1.M23 * m2.M34 + m1.M24 * m2.M44,
                M31 = m1.M31 * m2.M11 + m1.M32 * m2.M21 + m1.M33 * m2.M31 + m1.M34 * m2.M41,
                M32 = m1.M31 * m2.M12 + m1.M32 * m2.M22 + m1.M33 * m2.M32 + m1.M34 * m2.M42,
                M33 = m1.M31 * m2.M13 + m1.M32 * m2.M23 + m1.M33 * m2.M33 + m1.M34 * m2.M43,
                M34 = m1.M31 * m2.M14 + m1.M32 * m2.M24 + m1.M33 * m2.M34 + m1.M34 * m2.M44,
                M41 = m1.M41 * m2.M11 + m1.M42 * m2.M21 + m1.M43 * m2.M31 + m1.M44 * m2.M41,
                M42 = m1.M41 * m2.M12 + m1.M42 * m2.M22 + m1.M43 * m2.M32 + m1.M44 * m2.M42,
                M43 = m1.M41 * m2.M13 + m1.M42 * m2.M23 + m1.M43 * m2.M33 + m1.M44 * m2.M43,
                M44 = m1.M41 * m2.M14 + m1.M42 * m2.M24 + m1.M43 * m2.M34 + m1.M44 * m2.M44,
            };
        }

        public static RawMatrix Add(this RawMatrix m1, RawMatrix m2)
        {
            return new RawMatrix
            {
                M11 = m1.M11 + m2.M11,
                M12 = m1.M12 + m2.M12,
                M13 = m1.M13 + m2.M13,
                M14 = m1.M14 + m2.M14,
                M21 = m1.M21 + m2.M21,
                M22 = m1.M22 + m2.M22,
                M23 = m1.M23 + m2.M23,
                M24 = m1.M24 + m2.M24,
                M31 = m1.M31 + m2.M31,
                M32 = m1.M32 + m2.M32,
                M33 = m1.M33 + m2.M33,
                M34 = m1.M34 + m2.M34,
                M41 = m1.M41 + m2.M41,
                M42 = m1.M42 + m2.M42,
                M43 = m1.M43 + m2.M43,
                M44 = m1.M44 + m2.M44
            };
        }

        public static RawMatrix Transpose(this RawMatrix m)
        {
            return new RawMatrix
            {
                M11 = m.M11,
                M12 = m.M21,
                M13 = m.M31,
                M14 = m.M41,
                M21 = m.M12,
                M22 = m.M22,
                M23 = m.M32,
                M24 = m.M42,
                M31 = m.M13,
                M32 = m.M23,
                M33 = m.M33,
                M34 = m.M43,
                M41 = m.M14,
                M42 = m.M24,
                M43 = m.M34,
                M44 = m.M44
            };
        }

        public static RawMatrix Inverse(this RawMatrix m)
        {
            float det = m.Det();
            if (det == 0.0f)
                throw new Exception("Determinant cannot be zero here");

            float A11 = new Matrix3x3
            {
                M11 = m.M22,
                M12 = m.M23,
                M13 = m.M24,
                M21 = m.M32,
                M22 = m.M33,
                M23 = m.M34,
                M31 = m.M42,
                M32 = m.M43,
                M33 = m.M44
            }.Det();

            float A12= new Matrix3x3
            {
                M11 = m.M21,
                M12 = m.M23,
                M13 = m.M24,
                M21 = m.M31,
                M22 = m.M33,
                M23 = m.M34,
                M31 = m.M41,
                M32 = m.M43,
                M33 = m.M44
            }.Det()*-1.0f;

            float A13 = new Matrix3x3
            {
                M11 = m.M21,
                M12 = m.M22,
                M13 = m.M24,
                M21 = m.M31,
                M22 = m.M32,
                M23 = m.M34,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M44
            }.Det();

            float A14 = new Matrix3x3
            {
                M11 = m.M21,
                M12 = m.M22,
                M13 = m.M23,
                M21 = m.M31,
                M22 = m.M32,
                M23 = m.M33,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M43
            }.Det() * -1.0f;

            float A21 = new Matrix3x3
            {
                M11 = m.M12,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M32,
                M22 = m.M33,
                M23 = m.M34,
                M31 = m.M42,
                M32 = m.M43,
                M33 = m.M44
            }.Det() * -1.0f;

            float A22 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M31,
                M22 = m.M33,
                M23 = m.M34,
                M31 = m.M41,
                M32 = m.M43,
                M33 = m.M44
            }.Det();

            float A23 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M14,
                M21 = m.M31,
                M22 = m.M32,
                M23 = m.M34,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M44
            }.Det() * -1.0f;

            float A24 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M13,
                M21 = m.M31,
                M22 = m.M32,
                M23 = m.M33,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M43
            }.Det();

            float A31 = new Matrix3x3
            {
                M11 = m.M12,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M22,
                M22 = m.M23,
                M23 = m.M24,
                M31 = m.M42,
                M32 = m.M43,
                M33 = m.M44
            }.Det();

            float A32 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M21,
                M22 = m.M23,
                M23 = m.M24,
                M31 = m.M41,
                M32 = m.M43,
                M33 = m.M44
            }.Det() * -1.0f;

            float A33 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M14,
                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M24,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M44
            }.Det();

            float A34 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M13,
                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M23,
                M31 = m.M41,
                M32 = m.M42,
                M33 = m.M43
            }.Det() * -1.0f;

            float A41 = new Matrix3x3
            {
                M11 = m.M12,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M22,
                M22 = m.M23,
                M23 = m.M24,
                M31 = m.M32,
                M32 = m.M33,
                M33 = m.M34
            }.Det() * -1.0f;

            float A42 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M13,
                M13 = m.M14,
                M21 = m.M21,
                M22 = m.M23,
                M23 = m.M24,
                M31 = m.M31,
                M32 = m.M33,
                M33 = m.M34
            }.Det();

            float A43 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M14,
                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M24,
                M31 = m.M31,
                M32 = m.M32,
                M33 = m.M34
            }.Det() * -1.0f;

            float A44 = new Matrix3x3
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M13,
                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M23,
                M31 = m.M31,
                M32 = m.M32,
                M33 = m.M33
            }.Det();

            return new RawMatrix
            {
                M11 = A11 / det,
                M12 = A21 / det,
                M13 = A31 / det,
                M14 = A41 / det,
                M21 = A12 / det,
                M22 = A22 / det,
                M23 = A32 / det,
                M24 = A42 / det,
                M31 = A13 / det,
                M32 = A23 / det,
                M33 = A33 / det,
                M34 = A43 / det,
                M41 = A14 / det,
                M42 = A24 / det,
                M43 = A34 / det,
                M44 = A44 / det
            };
        }

        public static float Det(this RawMatrix m)
        {
            return m.M11 * m.M22 * m.M33 * m.M44 + m.M11 * m.M23 * m.M34 * m.M42 + m.M11 * m.M24 * m.M32 * m.M43 - m.M11 * m.M24 * m.M33 * m.M42 - m.M11 * m.M23 * m.M32 * m.M44 - m.M11 * m.M22 * m.M34 * m.M43 - m.M12 * m.M21 * m.M33 * m.M44 - m.M13 * m.M21 * m.M34 * m.M42 - m.M14 * m.M21 * m.M32 * m.M43 + m.M14 * m.M21 * m.M33 * m.M42 + m.M13 * m.M21 * m.M32 * m.M44 + m.M12 * m.M21 * m.M34 * m.M43
                      + m.M12 * m.M23 * m.M31 * m.M44 + m.M13 * m.M24 * m.M31 * m.M42 + m.M14 * m.M22 * m.M31 * m.M43 - m.M14 * m.M23 * m.M31 * m.M42 - m.M13 * m.M22 * m.M31 * m.M44 - m.M12 * m.M24 * m.M31 * m.M43 - m.M12 * m.M23 * m.M34 * m.M41 - m.M13 * m.M24 * m.M32 * m.M41 - m.M14 * m.M22 * m.M33 * m.M41 + m.M14 * m.M23 * m.M32 * m.M41 + m.M13 * m.M22 * m.M34 * m.M41 + m.M12 * m.M24 * m.M33 * m.M41;
        }

        public static float Det(this Matrix3x3 m)
        {
            return m.M11 * (m.M22 * m.M33 - m.M23 * m.M32) - m.M12 * (m.M21 * m.M33 - m.M23 * m.M31) + m.M13 * (m.M21 * m.M32 - m.M22 * m.M31);
        }

        public static RawMatrix Normalize(this RawMatrix m)
        {
            return Create(new Vector(m.M11, m.M12, m.M13).Normalize(), new Vector(m.M21, m.M22, m.M23).Normalize(), new Vector(m.M31, m.M32, m.M33).Normalize(), new Point(m.M41, m.M42, m.M43));
        }

        public static RawMatrix ScaleMatrix(float f)
        {
            return ScaleMatrix(new Vector(f, f, f));
        }

        public static RawMatrix ScaleMatrix(Vector v)
        {
            return new RawMatrix
            {
                M11 = v.X,
                M12 = 0,
                M13 = 0,
                M14 = 0,
                M21 = 0,
                M22 = v.Y,
                M23 = 0,
                M24 = 0,
                M31 = 0,
                M32 = 0,
                M33 = v.Z,
                M34 = 0,
                M41 = 0,
                M42 = 0,
                M43 = 0,
                M44 = 1
            };
        }

        /// <summary>
        /// Rotates start about point
        /// </summary>
        /// <param name="point"></param>
        /// <param name="start"></param>
        /// <param name="degrees"></param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static RawMatrix RotateAbout(Point point, RawMatrix start, float degrees, Vector axis)
        {
            RawMatrix trans = MatrixHelpers.Identity;
            trans.M41 = point.X;
            trans.M42 = point.Y;
            trans.M43 = point.Z;

            return start.Multiply(trans.Inverse().Multiply(CreateRotationMatrix(degrees, axis).Multiply(trans)));
        }

        /// <summary>
        /// Scales start about scaleBasis
        /// </summary>
        /// <param name="scaleBasis"></param>
        /// <param name="start"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static RawMatrix ScaleAbout(RawMatrix scaleBasis, RawMatrix start, float scale)
        {
            return ScaleAbout(scaleBasis, start, new Vector(scale, scale, scale));
        }

        /// <summary>
        /// Scales start about scaleBasis
        /// </summary>
        /// <param name="scaleBasis"></param>
        /// <param name="start"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static RawMatrix ScaleAbout(RawMatrix scaleBasis, RawMatrix start, Vector scale)
        {
            RawMatrix scaleBasisNormalized = scaleBasis.Normalize();

            return start.Multiply(scaleBasisNormalized.Inverse().Multiply(ScaleMatrix(scale).Multiply(scaleBasisNormalized)));
        }

        public static RawMatrix BuildOrthonormal(Vector baseAxis, bool slotInX = false, bool slotInY = false, bool slotInZ = true)
        {
            Vector b1 = new Vector((float)(1 - (Math.Pow(baseAxis.X, 2) / (1 + baseAxis.Z))), -(baseAxis.X * baseAxis.Y) / (1 + baseAxis.Z), -baseAxis.X);
            Vector b2 = new Vector(-(baseAxis.X * baseAxis.Y) / (1 + baseAxis.Z), (float)(1 - (Math.Pow(baseAxis.Y, 2) / (1 + baseAxis.Z))), -baseAxis.Y);
            Vector n = baseAxis;
            RawMatrix m = Create(b1, b2, n, new Point());
            if (slotInX)
            {
                m = Create(n, b2, b1.Flip(), new Point());
            }
            else if (slotInY)
            {
                m = Create(b1, n, b2.Flip(), new Point());
            }

            return m;
        }
    }

    public static class RawVectorExtensions
    {
        public static Point ToPoint(this RawVector2 v)
        {
            return new Point(v.X, v.Y, 0);
        }

        public static Point ToPoint(this RawVector3 v)
        {
            return new Point(v.X, v.Y, v.Z);
        }

        public static Vector ToVector(this RawVector2 v)
        {
            return new Vector(v.X, v.Y, 0);
        }

        public static Vector ToVector(this RawVector3 v)
        {
            return new Vector(v.X, v.Y, v.Z);
        }

        public static bool Matches(this RawVector3 v1, RawVector3 v2)
        {
            float epsilon = 0.01f;
            return (Math.Abs(v1.X - v2.X) < epsilon) && (Math.Abs(v1.Y - v2.Y) < epsilon) && (Math.Abs(v1.Z - v2.Z) < epsilon);
        }

        public static bool Matches(this RawVector4 v1, RawVector4 v2)
        {
            float epsilon = 0.01f;
            return (Math.Abs(v1.X - v2.X) < epsilon) && (Math.Abs(v1.Y - v2.Y) < epsilon) && (Math.Abs(v1.Z - v2.Z) < epsilon) && (Math.Abs(v1.W - v2.W) < epsilon);
        }
    }

    public static class RawColorExtensions
    {
        public static bool Matches(this RawColor3 v1, RawColor3 v2)
        {
            float epsilon = 0.01f;
            return (Math.Abs(v1.R - v2.R) < epsilon) && (Math.Abs(v1.G - v2.G) < epsilon) && (Math.Abs(v1.B - v2.B) < epsilon);
        }

        public static bool Matches(this RawColor4 v1, RawColor4 v2)
        {
            float epsilon = 0.01f;
            return (Math.Abs(v1.R - v2.R) < epsilon) && (Math.Abs(v1.G - v2.G) < epsilon) && (Math.Abs(v1.B - v2.B) < epsilon) && (Math.Abs(v1.A - v2.A) < epsilon);
        }
    }

    public struct VertexInterface
    {
        public float X { get; set;}
        public float Y { get; set; }
        public float Z { get; set; }
        public float W { get; set; }
    }

    public struct Matrix3x3
    {
        public float M11;
        public float M12;
        public float M13;
        public float M21;
        public float M22;
        public float M23;
        public float M31;
        public float M32;
        public float M33;
    }
}
