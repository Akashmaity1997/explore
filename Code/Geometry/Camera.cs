﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Mathematics.Interop;
using Assets.Interfaces;
using System.ComponentModel;

namespace Assets
{
    public class Camera : PropertyAware, IPositionable
    {
        private readonly float _aspectRatio, _nearPlane, _farPlane;

        private readonly IDataProvider _dataProvider;

        private readonly Cube _cameraCube;

        public Cube Representor => _cameraCube;

        private RawMatrix _projectionMatrix;
        public RawMatrix ProjectionMatrix { 
            get => _projectionMatrix; 
            private set 
            {
                _projectionMatrix = value;
                RaiseThisPropertyChanged();
            } 
        }

        private RawMatrix _position;
        public RawMatrix Position { 
            get => _position;
            set
            {
                _position = value;
                RaiseThisPropertyChanged();
                RaisePropertyChanged(nameof(ViewMatrix));
            }
        }

        public RawMatrix PivotOffset { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public RawMatrix ViewMatrix => Position.Inverse();

        private Vector _lookAt;
        public Vector LookAt
        {
            get => _lookAt;
            set
            {
                _lookAt = value;
                CalculateCameraAttitude();
                RaiseThisPropertyChanged();
            }
        }

        private Vector _up;
        public Vector Up
        {
            get => _up;
            set
            {
                _up = value;
                RaiseThisPropertyChanged();
            }
        }

        private float _fov;
        public float FOV
        {
            get => _fov;
            set
            {
                _fov = value;
                UpdateProjectionMatrix();
                RaiseThisPropertyChanged();
            }
        }

        private APresentable _followingObject;
        public APresentable FollowingObject
        {
            get => _followingObject;
            set
            {
                if (_followingObject != null)
                    _followingObject.PropertyChanged -= FollowingObjectPropertyChanged;

                _followingObject = value;

                if (_followingObject != null)
                    _followingObject.PropertyChanged += FollowingObjectPropertyChanged;
            }
        }

        public float AngleX { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float AngleY { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float AngleZ { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleX { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleY { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float OffsetAngleZ { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Camera(float aspectRatio, float fov, float nearPlane, float farPlane, IDataProvider dataProvider)
        {
            _up = Vector.YAxis;
            _lookAt = new Vector();
            _aspectRatio = aspectRatio;
            _fov = fov;
            _nearPlane = nearPlane;
            _farPlane = farPlane;
            _dataProvider = dataProvider;

            Point p = new Point(2, 0, -2);
            Vector z = (_lookAt - new Vector(p.X, p.Y, p.Z)).Normalize();
            Vector x = _up.Cross(z).Normalize();
            _position = MatrixHelpers.Create(x, _up, z, p);

            UpdateProjectionMatrix();

            _cameraCube = new Cube(Position, _dataProvider, new Point(1.0f, 1.0f, 1.0f), 0.5f);
            _cameraCube.PropertyChanged += RepresentorPropertyChanged;
            _dataProvider.RegisterObject(this);
        }

        public Camera(Vector position, Vector target, float aspectRatio, float fov, float nearPlane, float farPlane, IDataProvider dataProvider)
        {
            _lookAt = target;
            _aspectRatio = aspectRatio;
            _fov = fov;
            _nearPlane = nearPlane;
            _farPlane = farPlane;
            _dataProvider = dataProvider;

            Vector zFinal = (target - position).Normalize();
            Vector zShadow = new Vector(zFinal.X, 0, zFinal.Z).Normalize();

            Vector axis = zFinal.Cross(zShadow).Normalize();

            Vector xFinal = axis.Flip();
            Vector yFinal = zFinal.Cross(xFinal);

            //float val = zFinal.Dot(zShadow);
            //float theta = (float)(Math.Acos(val) * 180.0f / Math.PI);
            //Vector yFinal = Vector.YAxis.Rotate(-theta, axis);
            //Vector xFinal = yFinal.Cross(zFinal);

            _up = yFinal;
            _position = MatrixHelpers.Create(xFinal, yFinal, zFinal, new Point(position.X, position.Y, position.Z));

            UpdateProjectionMatrix();

            _cameraCube = new Cube(Position, _dataProvider, new Point(1.0f, 1.0f, 1.0f), 0.5f);
            _cameraCube.PropertyChanged += RepresentorPropertyChanged;
            _dataProvider.RegisterObject(this);
        }

        public void Unregister()
        {
            _cameraCube.PropertyChanged -= RepresentorPropertyChanged;
            _dataProvider.UnRegisterObject(_cameraCube);
        }

        private void RepresentorPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(_cameraCube.Position)))
            {
                Position = _cameraCube.Position;
            }
        }

        private void CalculateCameraAttitude()
        {
            Vector position = new Vector(Position.M41, Position.M42, Position.M43);
            Vector zFinal = (_lookAt - position).Normalize();
            Vector zShadow = new Vector(zFinal.X, 0, zFinal.Z).Normalize();

            Vector axis = zFinal.Cross(zShadow).Normalize();

            Vector xFinal = axis.Flip();
            Vector yFinal = zFinal.Cross(xFinal);

            _up = yFinal;
            _cameraCube.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, new Point(position.X, position.Y, position.Z));
        }

        private void UpdateProjectionMatrix()
        {
            double rad = _fov * Math.PI / 180.0f;
            ProjectionMatrix = new RawMatrix
            {
                M11 = (float)(1 / (_aspectRatio * Math.Tan(rad / 2))),
                M12 = 0,
                M13 = 0,
                M14 = 0,
                M21 = 0,
                M22 = (float)(1 / Math.Tan(rad / 2)),
                M23 = 0,
                M24 = 0,
                M31 = 0,
                M32 = 0,
                M33 = _farPlane / (_farPlane - _nearPlane),
                M34 = 1,
                M41 = 0,
                M42 = 0,
                M43 = -_nearPlane * _farPlane / (_farPlane - _nearPlane),
                M44 = 0
            };
        }

        public void Zoom(float zoom)
        {
            if(FOV + zoom >=10 && FOV + zoom <=180)
                FOV += zoom;
        }

        public void CameraMouseRotation(Vector delta)
        {
            Vector targetToPosition = new Vector(Position.M41, Position.M42, Position.M43) - LookAt;
            float xShift = (float)(delta.X / 100.0f);
            float yShift = (float)(delta.Y / 100.0f);
            float thetax = (float)((xShift / targetToPosition.Magnitude()) * (180.0f / Math.PI));
            float thetay = (float)((yShift / targetToPosition.Magnitude()) * (180.0f / Math.PI));
            Vector rotatedTargetToPosition = targetToPosition.Rotate(thetax, _up);

            Point pos = new Point(LookAt.X + rotatedTargetToPosition.X, LookAt.Y + rotatedTargetToPosition.Y, LookAt.Z + rotatedTargetToPosition.Z);

            Vector zFinal = rotatedTargetToPosition.Flip().Normalize();
            Vector yFinal = _up;
            Vector xFinal = yFinal.Cross(zFinal);

            _cameraCube.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, pos);

            Vector rotatedTargetToPosition2 = rotatedTargetToPosition.Rotate(thetay, xFinal);
            pos = new Point(LookAt.X + rotatedTargetToPosition2.X, LookAt.Y + rotatedTargetToPosition2.Y, LookAt.Z + rotatedTargetToPosition2.Z);
            zFinal = rotatedTargetToPosition2.Flip().Normalize();
            Up = yFinal = zFinal.Cross(xFinal);

            _cameraCube.Position = MatrixHelpers.Create(xFinal, yFinal, zFinal, pos);

        }

        public void SetCurrent()
        {
            _dataProvider.CurrentCamera = this;
            _dataProvider.SelectedObject = null;
        }

        public void AssignTarget()
        {      
            _dataProvider.PickCameraTarget();
            FollowingObject = null;
        }

        public void AssignTargetAndFollow()
        {
            _dataProvider.PickCameraTarget(true);
        }

        private void FollowingObjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals(nameof(FollowingObject.Position)))
            {
                LookAt = new Vector(FollowingObject.Position.M41, FollowingObject.Position.M42, FollowingObject.Position.M43);
            }
        }
    }
}
