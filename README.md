# README #

This a project to develop a physics based rendering software, may possibly include ray tracing later on. We primarily have
multi-lighting system, multi-camera system. Currently texture support is at a very preliminary stage. The software has
a main viewport within which the objects are rendered and the viewport serves as the view or the current camera. Three 
coloured axis represent current world x, y and z axis. To the right side of the window is the main control panel, which
is divided into 4 tabs: Geometry, Materials, Environment, Cameras. All objects spawned go in geometry tab, cameras in
cameras tab, lights in environment tab and decals in materials tab. Extensive materials support is also a thing we want
to do in future as it is indispensible when it comes to support photo realistic rendering.


### What is this repository for? ###

This repo hosts the source code of this software. The repo is divided into two submodules. The main submodule contains
the main source code of the software. Another submodule named 'Third Party' contains dependencies and third party libraries
which the softare depends on. This is done to make sure we have clear separation between our code and thord party libraries. 
When the time comes to update third party we do it separately in the third party submodule. This way things remain neat!

### How do I get set up? ###

Well these are the things that are needed:

* VS2019 or whatever VS IDE
* Pull repo
* And we are done!

### Contribution guidelines ###

* Writing tests
* Code review
* Suggestions/ Ideas/ Improvements

### Who do I talk to? ###

* Repo owner or admin

### Code Architecture Overview ###
The code is written in C#. UI is developed in WPF (Windows Presentation Foundation). For graphics API we are using DirectX 11
which is wrapped indise a library known as SharpDX. This exposes COM objects in a managed environment. We have to be aware that
all these D3D11 objects are unmanaged code, which requires us to explicitly dispose the objects when we are done using them,
or else they will start filling up memory resources! Frames are updated only when it is required, ie, if an action is performed,
such as, changing the object position, or adding a light or adding a picture, changing the camera position or anything! Frames are
updated via events which are raised by changing objects. In response to those events what we do is clear the existing render state,
update the state of the scene via shader communication and redraw the updated scene.

For example, when we change the position of the object, it raises an event, which leads to clearing out the existing render state,
the updated position of the object is transferred to shader constants and the calculations are performed there and the updated state
is redrawn on the viewport.Its the same for any change in camera, lights, decals or anything. We always update the shader constants.

Changes like position change calculations, camera manipulation calulations etc are done in CPU(inside our classes). For that I have
written a math helper class which helps us to multiply two matrices, or cross product two vectors or inverse a matrix etc. So any
code we change here we have to be ultra careful, cause it will affect everywhere! Once the updated matrices or vectors are calculated,
they are updated to shader in the form of constant buffers registers. The shaders then use these updated buffers to perform 
vetex calculations, texture calculations or lighting calculations etc. So all these calculations are done in GPU. The shaders I have
written are modular and largely works in metallic-roughness PBR workflow and surely we can work and improve on it. Given a particular 
object to be drawn, a 'shader requirement' object is instantiated, which contains information about what shader function sequence is required
to draw the object. Accordingly, the HLSL functions and variables are taken from the shader parser, and the text constructor constructs a shader 
text containing the vertex and pixel shader for us. This is fully automatic. All mathematics are done in left hand vector notations. So keep that in mind.

We have a drawing manager which sets up render target and depth target for the current immediate rendering destination. This class manages
different render passes (currently we have a main render pipeline and a pixel picking render pipeline). The pixel picking render pass is used
for viewport picking logic. For more info in this I urge you to go through the code.

We have a Pass class, which holds info on what obejcts to draw, on which render taregt to draw on and info about other draw states. Several passes are
concatenated and drawn at the end. E.g., the very first collection of passes are mostly to render the reflection map, where the drawable is the environment 
sphere with the mapped IBL, and in each of the 6 passes we position the camera or frustum along the 6 faces of a cube, so that we generate a cube map.
Later on this cube map is used to compute other maps and draw objects in the scene.